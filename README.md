<h1>AdminiX</h1>

[![Total Downloads](https://poser.pugx.org/alex-kudrya/adminix/downloads)](//packagist.org/packages/alex-kudrya/adminix)
[![Version](https://poser.pugx.org/alex-kudrya/adminix/version)](//packagist.org/packages/alex-kudrya/adminix)
[![License](https://poser.pugx.org/alex-kudrya/adminix/license)](//packagist.org/packages/alex-kudrya/adminix)

<h4>Fast deployment, easy configurable admin panel for Laravel</h4>

<h3>All instruction in <a href="https://adminix-alexkudrya91-c66a1a88e52d3552094f7ec5ce3d7f0831d16e5039.gitlab.io/">AdminiX documentation<a/>.</h3>

- <h3><a href="https://adminix-alexkudrya91-c66a1a88e52d3552094f7ec5ce3d7f0831d16e5039.gitlab.io/installation.html">Installation<a/></h3></h3>
- <h3><a href="https://adminix-alexkudrya91-c66a1a88e52d3552094f7ec5ce3d7f0831d16e5039.gitlab.io/configurartion.html">Configuration<a/></h3></h3>
- <h3><a href="https://adminix-alexkudrya91-c66a1a88e52d3552094f7ec5ce3d7f0831d16e5039.gitlab.io/modules.html">Modules<a/></h3></h3>

Author - Oleksandr Kudria <a href="mailto:alexkudrya91@gmail.com">alexkudrya91@gmail.com<a/>

<img src="https://gitlab.com/alexkudrya91/adminix/-/wikis/uploads/4465cec111982fd7b5a3563af28bddfc/image.png">
