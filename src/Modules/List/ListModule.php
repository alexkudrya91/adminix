<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Link\AdminixLinkModule;
use AlexKudrya\Adminix\Modules\Link\LinkModule;
use AlexKudrya\Adminix\Modules\Sorting;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null title(?string $title = null)
 * @method self|string name(?string $name = null)
 * @method self|string dataSource(?string $data_source = null, ?array $relations = null)
 * @method self|array|null criteria(?array $criteria = null)
 * @method self|array|null data(?array $static = null)
 * @method self|Sorting|null sorting(?Sorting $sorting = null)
 * @method self|int|null pagination(?int $pagination = null)
 * @method self|string|null resource(?string $resource = null)
 * @method self|string|null primaryKey(?string $primary_key = null)
 * @method self|ListSearch|null search(?ListSearch $search = null)
 * @method self|array|null validation(?array $validation = null)
 * @method self|AdminixLinkModule|null newItemLink(?AdminixLinkModule $new_item_link = null)
 * @method ListField[] fields()
 * @method ListFilter[] filters()
 * @method LinkModule[] actions()
 * @method ModuleTypeEnum type()
 */
class ListModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    use ModuleMagicMethods;

    protected ?string $name = null;
    protected ?string $title = null;
    protected ?int $pagination = null;
    protected ?string $data_source = null;
    protected ?array $relations = null;
    protected ?string $resource = null;
    protected bool $with_deleted = false;
    protected bool $editable = false;
    protected string $primary_key = 'id';
    protected ?array $criteria = null;
    protected ?ListSearch $search = null;
    protected ?AdminixLinkModule $new_item_link = null;
    protected ?Sorting $sorting = null;
    protected array $data = [];
    protected ModuleTypeEnum $type = ModuleTypeEnum::LIST;
    protected array $validation = [];

    /** @var ListField[] */
    protected array $fields = [];

    /** @var ListFilterInterface[] */
    protected array $filters = [];

    /** @var LinkModule[] */
    protected array $actions = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getPagination(): ?int
    {
        return $this->pagination;
    }

    public function setPagination(?int $pagination): static
    {
        $this->pagination = $pagination;
        return $this;
    }

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function getRelations(): ?array
    {
        return $this->relations;
    }

    public function setDataSource(string $data_source, ?array $relations = null): static
    {
        $this->data_source = $data_source;
        if (!empty($relations)) {
            $this->relations = $relations;
        }
        return $this;
    }

    public function getResource(): ?string
    {
        return $this->resource;
    }

    public function setResource(?string $resource): static
    {
        $this->resource = $resource;
        return $this;
    }

    public function isWithDeleted(): bool
    {
        return $this->with_deleted;
    }

    public function withDeleted(): static
    {
        $this->with_deleted = true;
        return $this;
    }

    public function isEditable(): bool
    {
        return $this->editable;
    }

    public function editable(): static
    {
        $this->editable = true;
        return $this;
    }

    public function getPrimaryKey(): string
    {
        return $this->primary_key;
    }

    public function setPrimaryKey(string $primary_key): static
    {
        $this->primary_key = $primary_key;
        return $this;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function setCriteria(?array $criteria): static
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function setFields(array $fields): static
    {
        $this->fields = $fields;
        return $this;
    }

    public function addField(ListField $field): static
    {
        $this->fields[] = $field;
        return $this;
    }

    public function addFields(ListField ...$fields): static
    {
        foreach ($fields as $field) {
            $this->fields[] = $field;
        }

        return $this;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function setFilters(array $filters): static
    {
        $this->filters = $filters;
        return $this;
    }

    public function addFilter(ListFilterInterface $filter): static
    {
        $this->filters[] = $filter;
        return $this;
    }

    public function addFilters(ListFilterInterface ...$filters): static
    {
        foreach ($filters as $filter) {
            $this->filters[] = $filter;
        }

        return $this;
    }

    public function getActions(): array
    {
        return $this->actions;
    }

    public function setActions(array $actions): static
    {
        $this->actions = $actions;
        return $this;
    }

    public function addAction(LinkModule $action): static
    {
        $this->actions[] = $action;
        return $this;
    }

    public function addActions(LinkModule ...$actions): static
    {
        foreach ($actions as $action) {
            $this->actions[] = $action;
        }

        return $this;
    }

    public function getSearch(): ?ListSearch
    {
        return $this->search;
    }

    public function setSearch(ListSearch $search): static
    {
        $this->search = $search;
        return $this;
    }

    public function getNewItemLink(): ?AdminixLinkModule
    {
        return $this->new_item_link;
    }

    public function setNewItemLink(?AdminixLinkModule $new_item_link): static
    {
        $this->new_item_link = $new_item_link;
        return $this;
    }

    public function getSorting(): ?Sorting
    {
        return $this->sorting;
    }

    public function setSorting(Sorting $sorting): static
    {
        $this->sorting = $sorting;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }


    public function getValidation(): array
    {
        return $this->validation;
    }

    public function setValidation(array $validation): static
    {
        $this->validation = $validation;
        return $this;
    }

}
