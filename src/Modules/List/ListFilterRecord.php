<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|mixed value(?mixed $value = null)
 */
class ListFilterRecord
{
    use ModuleMagicMethods;

    protected string $name;
    protected mixed $value = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): static
    {
        $this->value = $value;
        return $this;
    }
}
