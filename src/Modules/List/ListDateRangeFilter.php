<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|string field(?string $field = null)
 * @method self|string valueTo(?string $value_to = null)
 * @method self|string valueFrom(?string $value_from = null)
 * @method self|bool filterTo(?bool $filter_to = null)
 * @method self|bool filterFrom(?bool $filter_from = null)
 */
class ListDateRangeFilter implements ListFilterInterface
{
    use ModuleMagicMethods;

    protected string $name;
    protected string $field;
    protected ?string $value_to = null;
    protected ?string $value_from = null;
    protected bool $with_time = false;
    protected bool $filter_to = true;
    protected bool $filter_from = true;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): static
    {
        $this->field = $field;
        return $this;
    }

    public function getValueTo(): ?string
    {
        return $this->value_to;
    }

    public function setValueTo(?string $value_to): static
    {
        $this->value_to = $value_to;
        return $this;
    }

    public function getValueFrom(): ?string
    {
        return $this->value_from;
    }

    public function setValueFrom(?string $value_from): static
    {
        $this->value_from = $value_from;
        return $this;
    }

    public function isWithTime(): bool
    {
        return $this->with_time;
    }

    public function withTime(): static
    {
        $this->with_time = true;
        return $this;
    }

    public function hasFilterTo(): bool
    {
        return $this->filter_to;
    }

    public function setFilterTo(bool $filter_to): static
    {
        $this->filter_to = $filter_to;
        return $this;
    }

    public function hasFilterFrom(): bool
    {
        return $this->filter_from;
    }

    public function setFilterFrom(bool $filter_from): static
    {
        $this->filter_from = $filter_from;
        return $this;
    }
}
