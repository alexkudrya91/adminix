<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\InputSelectSrc;
use AlexKudrya\Adminix\Modules\SelectRecord;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|ListFieldTypeEnum type(?ListFieldTypeEnum $type = null)
 * @method self|string field(?string $field = null)
 * @method self|string sorting(?string $sorting = null)
 * @method self|array|null selectRecords(?array $select_records = null)
 * @method self|array|null validation(?array $validation = null)
 * @method self|InputSelectSrc src(?InputSelectSrc $src = null)
 */
class ListField
{
    use ModuleMagicMethods;

    protected ?string $name = null;
    protected ListFieldTypeEnum $type = ListFieldTypeEnum::STRING;
    protected string $field;
    protected ?InputSelectSrc $src = null;
    protected bool $sortable = false;
    protected ?string $sorting = null;
    protected bool $editable = false;
    protected bool $required = false;
    /** @var SelectRecord[] */
    protected ?array $select_records = null;
    protected ?array $validation = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getType(): ListFieldTypeEnum
    {
        return $this->type;
    }

    public function setType(ListFieldTypeEnum $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): static
    {
        $this->field = $field;
        return $this;
    }

    public function getSrc(): ?InputSelectSrc
    {
        return $this->src;
    }

    public function setSrc(?InputSelectSrc $src): static
    {
        $this->src = $src;
        return $this;
    }

    public function isSortable(): bool
    {
        return $this->sortable;
    }

    public function sortable(): static
    {
        $this->sortable = true;
        return $this;
    }

    public function isEditable(): bool
    {
        return $this->editable;
    }

    public function editable(): static
    {
        $this->editable = true;
        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function required(): static
    {
        $this->required = true;
        $this->addValidationRule('required');
        return $this;
    }

    public function getSorting(): ?string
    {
        return $this->sorting;
    }

    public function setSorting(?string $sorting): static
    {
        $this->sorting = $sorting;
        return $this;
    }

    public function getSelectRecords(): array
    {
        return $this->select_records;
    }

    public function setSelectRecords(array $select_records): static
    {
        $this->select_records = $select_records;
        return $this;
    }

    public function addSelectRecord(SelectRecord $select_record): static
    {
        $this->select_records[] = $select_record;
        return $this;
    }

    public function addSelectRecords(SelectRecord ...$select_records): static
    {
        foreach ($select_records as $select_record) {
            $this->addSelectRecord($select_record);
        }

        return $this;
    }


    public function getValidation(): ?array
    {
        return $this->validation;
    }

    public function setValidation(?array $validation): static
    {
        $this->validation = $validation;
        return $this;
    }

    public function addValidationRule(string $rule): static
    {
        $this->validation[] = $rule;
        return $this;
    }

    public function addValidationRules(string ...$rules): static
    {
        foreach ($rules as $rule) {
            $this->validation[] = $rule;
        }

        return $this;
    }
}
