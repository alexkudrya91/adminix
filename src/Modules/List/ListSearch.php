<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|array searchFields(?array $search_fields = null)
 * @method self|string placeholder(?string $placeholder = null)
 * @method self|string query(?string $query = null)
 */
class ListSearch
{
    use ModuleMagicMethods;

    protected array $search_fields;
    protected string $placeholder;
    protected string $query = '';

    public function getSearchFields(): array
    {
        return $this->search_fields;
    }

    public function setSearchFields(array $search_fields): static
    {
        $this->search_fields = $search_fields;
        return $this;
    }

    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    public function setPlaceholder(string $placeholder): static
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function setQuery(string $query): static
    {
        $this->query = $query;
        return $this;
    }
}
