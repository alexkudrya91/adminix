<?php

namespace AlexKudrya\Adminix\Modules\List;

enum ListFieldTypeEnum: string
{
    case STRING = 'string';
    case TEXTAREA = 'textarea';
    case EMAIL = 'email';
    case INTEGER = 'integer';
    case FLOAT = 'float';
    case BOOLEAN = 'boolean';
    case DATETIME = 'datetime';
    case DATE = 'date';
    case IMAGE = 'image';
    case SELECT = 'select';
    case HIDDEN = 'hidden';
}
