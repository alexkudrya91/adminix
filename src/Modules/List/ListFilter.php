<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\InputSelectSrc;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|string field(?string $field = null)
 * @method self|InputSelectSrc src(?InputSelectSrc $src = null)
 * @method self|array filterRecords(?array $filter_records = null)
 * @method self|mixed value(?mixed $value = null)
 */
class ListFilter implements ListFilterInterface
{
    use ModuleMagicMethods;

    protected string $name;
    protected string $field;
    protected ?InputSelectSrc $src = null;
    protected bool $passive = false;
    protected mixed $value = null;

    /** @var ListFilterRecord[] */
    protected ?array $filter_records;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): static
    {
        $this->field = $field;
        return $this;
    }

    public function getSrc(): ?InputSelectSrc
    {
        return $this->src;
    }

    public function setSrc(?InputSelectSrc $src): static
    {
        $this->src = $src;
        return $this;
    }

    public function getFilterRecords(): ?array
    {
        return $this->filter_records;
    }

    public function setFilterRecords(?array $filter_records): static
    {
        $this->filter_records = $filter_records;
        return $this;
    }

    public function addFilterRecord(ListFilterRecord $filter_record): ?static
    {
        $this->filter_records[] = $filter_record;

        return $this;
    }

    public function addFilterRecords(ListFilterRecord ...$filter_records): static
    {
        foreach ($filter_records as $filter_record) {
            $this->addFilterRecord($filter_record);
        }
        return $this;
    }

    public function isPassive(): bool
    {
        return $this->passive;
    }

    public function passive(): static
    {
        $this->passive = true;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): static
    {
        $this->value = $value;
        return $this;
    }
}
