<?php

namespace AlexKudrya\Adminix\Modules\List;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|string field(?string $field = null)
 * @method self|string value(?string $value_to = null)
 */
class ListDateFilter implements ListFilterInterface
{
    use ModuleMagicMethods;

    protected string $name;
    protected string $field;
    protected ?string $value = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): static
    {
        $this->field = $field;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): static
    {
        $this->value = $value;
        return $this;
    }
}
