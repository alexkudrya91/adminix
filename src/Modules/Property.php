<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string key(?string $key = null)
 * @method self|string|null value(?string $value = null)
 */
class Property
{
    use ModuleMagicMethods;

    protected string $key;

    protected mixed $value = null;

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): self
    {
        $this->value = $value;
        return $this;
    }
}
