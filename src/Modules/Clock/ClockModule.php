<?php

namespace AlexKudrya\Adminix\Modules\Clock;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string id(?string $id = null)
 * @method self|string name(?string $name = null)
 * @method self|string title(?string $title = null)
 * @method self|string locale(?string $title = null)
 * @method ModuleTypeEnum type()
 */
class ClockModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    use ModuleMagicMethods;

    protected string $id;
    protected ?string $name = 'clock';
    protected ?string $title = null;
    protected string $locale = 'en';
    protected bool $date_enabled = true;
    protected bool $time_enabled = true;
    protected ModuleTypeEnum $type = ModuleTypeEnum::CLOCK;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): static
    {
        $this->locale = $locale;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }

    public function disableDate(): static
    {
        $this->date_enabled = false;
        return $this;
    }

    public function enableDate(): static
    {
        $this->date_enabled = true;
        return $this;
    }

    public function isDateEnabled(): bool
    {
        return $this->date_enabled;
    }

    public function disableTime(): static
    {
        $this->time_enabled = false;
        return $this;
    }

    public function enableTime(): static
    {
        $this->time_enabled = true;
        return $this;
    }

    public function isTimeEnabled(): bool
    {
        return $this->time_enabled;
    }
}
