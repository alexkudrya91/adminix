<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string nameField(?string $name_field = null)
 * @method self|string valueField(?string $value_field = null)
 * @method self|string dataSource(?string $data_source = null)
 * @method self|array|null criteria(?array $criteria = null)
 */
class InputSelectSrc
{
    use ModuleMagicMethods;

    protected string $data_source;
    protected string $name_field;
    protected string $value_field;
    protected ?array $criteria = null;

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function setDataSource(string $data_source): self
    {
        $this->data_source = $data_source;
        return $this;
    }

    public function getNameField(): string
    {
        return $this->name_field;
    }

    public function setNameField(string $name_field): self
    {
        $this->name_field = $name_field;
        return $this;
    }

    public function getValueField(): string
    {
        return $this->value_field;
    }

    public function setValueField(string $value_field): self
    {
        $this->value_field = $value_field;
        return $this;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function setCriteria(?array $criteria): self
    {
        $this->criteria = $criteria;
        return $this;
    }
}
