<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;

/**
 * @method self|string name(?string $name = null)
 * @method ModuleTypeEnum type()
 */
interface AdminixTopModuleInterface
{
    public function getType(): ModuleTypeEnum;
    public function getName(): string;
    public function setName(string $name): static;
}
