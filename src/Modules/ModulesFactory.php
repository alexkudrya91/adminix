<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\DataProviders\ChartLineModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\ChartBarModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\CounterModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\LinkModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\ListModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\DataProviders\NewResourceModuleDataProvider;
use AlexKudrya\Adminix\Modules\DataProviders\ResourceModuleDataProvider;
use Exception;

class ModulesFactory
{
    /**
     * @param string $type
     * @return ModuleDataProviderInterface
     * @throws Exception
     */
    public function handle(string $type): ModuleDataProviderInterface
    {
        return match ($type) {
            ModuleTypeEnum::LIST->value => new ListModuleDataProvider(),
            ModuleTypeEnum::CHART_LINE->value => new ChartLineModuleDataProvider(),
            ModuleTypeEnum::CHART_BAR->value => new ChartBarModuleDataProvider(),
            ModuleTypeEnum::COUNTER->value => new CounterModuleDataProvider(),
            ModuleTypeEnum::RESOURCE->value => new ResourceModuleDataProvider(),
            ModuleTypeEnum::NEW_RESOURCE->value => new NewResourceModuleDataProvider(),
            ModuleTypeEnum::LINK->value, ModuleTypeEnum::ADMINIX_LINK->value => new LinkModuleDataProvider(),
            default => throw new Exception('Invalid adminix module name: "' . $type . '"'),
        };
    }
}
