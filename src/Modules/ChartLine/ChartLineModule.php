<?php

namespace AlexKudrya\Adminix\Modules\ChartLine;

use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\CheckDataSource;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null id(?string $id = null)
 * @method self|string|null title(?string $title = null)
 * @method self|string name(?string $name = null)
 * @method self|ColorsEnum color(?ColorsEnum $color = null)
 * @method self|string dataSource(?string $data_source = null)
 * @method self|array|null criteria(?array $criteria = null)
 * @method self|int limit(?string $limit = null)
 * @method self|array data(?array $data = null)
 * @method self|array labels(?array $labels = null)
 * @method self|array values(?array $values = null)
 * @method self|string baseField(?string $base_field = null)
 * @method self|ChartLineValueTypeEnum valueType(?ChartLineValueTypeEnum $value_type = null)
 */
class ChartLineModule implements AdminixModuleInterface,AdminixTopModuleInterface
{
    use CheckDataSource, ModuleMagicMethods;

    protected ?string $id = null;
    protected ?string $title = null;
    protected ?string $name = null;
    protected ?ColorsEnum $color = null;
    protected ?ChartLineValueTypeEnum $value_type = null;
    protected string $base_field = 'created_at';
    protected ?string $data_source = null;
    protected ?array $criteria = null;
    protected int $limit = 30;
    protected array $data = [];
    protected array $labels = [];
    protected array $values = [];
    protected ModuleTypeEnum $type = ModuleTypeEnum::CHART_LINE;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): ColorsEnum
    {
        return $this->color;
    }

    public function getValueType(): ChartLineValueTypeEnum
    {
        return $this->value_type;
    }

    public function getBaseField(): string
    {
        return $this->base_field;
    }

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setColor(ColorsEnum $color): static
    {
        $this->color = $color;
        return $this;
    }

    public function setValueType(ChartLineValueTypeEnum $value_type): static
    {
        $this->value_type = $value_type;
        return $this;
    }

    public function setBaseField(string $base_field): static
    {
        $this->base_field = $base_field;
        return $this;
    }

    public function setDataSource(string $data_source): static
    {
        $this->data_source = $data_source;
        return $this;
    }

    public function setCriteria(?array $criteria): static
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function setLimit(int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getLabels(): array
    {
        return $this->labels;
    }

    public function setLabels(array $labels): static
    {
        $this->labels = $labels;
        return $this;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values): static
    {
        $this->values = $values;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }
}
