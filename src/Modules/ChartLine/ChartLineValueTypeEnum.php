<?php

namespace AlexKudrya\Adminix\Modules\ChartLine;

enum ChartLineValueTypeEnum: string
{
    case COUNT_DAILY = 'count_daily';
    case COUNT_MONTHLY = 'count_monthly';
}
