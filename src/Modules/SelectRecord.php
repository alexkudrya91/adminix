<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $name = null)
 * @method self|string|null value(?string $value = null)
 */
class SelectRecord
{
    use ModuleMagicMethods;

    protected string $name;

    protected mixed $value = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): self
    {
        $this->value = $value;
        return $this;
    }
}
