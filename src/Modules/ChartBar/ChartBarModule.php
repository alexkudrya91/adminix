<?php

namespace AlexKudrya\Adminix\Modules\ChartBar;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\CheckDataSource;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null id(?string $title = null)
 * @method self|string|null title(?string $title = null)
 * @method self|string name(?string $name = null)
 * @method self|string dataSource(?string $data_source = null)
 * @method self|array|null criteria(?array $criteria = null)
 * @method self|array|null labels(?array $labels = null)
 * @method self|array|null values(?array $values = null)
 * @method self|array|null colors(?array $colors = null)
 * @method ChartBarItem[] bars()
 */
class ChartBarModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    use CheckDataSource, ModuleMagicMethods;

    protected ?string $id = null;
    protected ?string $title = null;
    protected ?string $name = null;
    protected ?string $data_source = null;
    protected ?array $criteria = null;
    protected array $labels = [];
    protected array $values = [];
    protected array $colors = [];
    protected ModuleTypeEnum $type = ModuleTypeEnum::CHART_BAR;

    /** @var ChartBarItem[] */
    protected array $bars = [];

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function getBars(): array
    {
        return $this->bars;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setDataSource(string $data_source): static
    {
        $this->data_source = $data_source;
        return $this;
    }

    public function setCriteria(?array $criteria): static
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function addBar(ChartBarItem $bar): static
    {
        $this->bars[] = $bar;
        return $this;
    }

    public function addBars(ChartBarItem ...$bars): static
    {
        foreach ($bars as $bar) {
            $this->bars[] = $bar;
        }
        return $this;
    }

    public function getLabels(): array
    {
        return $this->labels;
    }

    public function setLabels(array $labels): static
    {
        $this->labels = $labels;
        return $this;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values): static
    {
        $this->values = $values;
        return $this;
    }

    public function getColors(): array
    {
        return $this->colors;
    }

    public function setColors(array $colors): static
    {
        $this->colors = $colors;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }
}
