<?php

namespace AlexKudrya\Adminix\Modules\ChartBar;

use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null label(?string $label = null)
 * @method self|ColorsEnum|null color(?ColorsEnum $color = null)
 * @method self|array|null criteria(?array $criteria = null)
 */
class ChartBarItem implements AdminixModuleInterface
{
    use ModuleMagicMethods;

    protected ?string $label = null;
    protected ?ColorsEnum $color = null;
    protected ?array $criteria = null;

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getColor(): ColorsEnum
    {
        return $this->color;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;
        return $this;
    }

    public function setColor(ColorsEnum $color): self
    {
        $this->color = $color;
        return $this;
    }

    public function setCriteria(?array $criteria): self
    {
        $this->criteria = $criteria;
        return $this;
    }
}
