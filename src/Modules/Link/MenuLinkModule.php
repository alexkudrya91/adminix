<?php

namespace AlexKudrya\Adminix\Modules\Link;


use AlexKudrya\Adminix\Modules\AdminixModuleInterface;

class MenuLinkModule extends LinkModule implements LinkInterface,AdminixModuleInterface
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
       return $this->title ?: ('[' . $this->uri . ']');
    }
}
