<?php

namespace AlexKudrya\Adminix\Modules\Link;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Enums\HttpMethodsEnum;
use Exception;

/**
 * @method self|string uri(?string $uri = null)
 * @method self|string|null title(?string $title = null)
 * @method self|string|null icon(?string $icon = null)
 * @method self|string|null name(?string $name = null)
 * @method self|array|null params(?array $params = null)
 * @method self|array|null criteria(?array $criteria = null)
 */
class AdminixLinkModule extends LinkModule implements LinkInterface,AdminixModuleInterface
{
    protected ModuleTypeEnum $type = ModuleTypeEnum::ADMINIX_LINK;

    /**
     * @throws Exception
     */
    public function setMethod(HttpMethodsEnum $method): static
    {
        throw new Exception("AdminixLinkModule unavailable to change http method.");
    }
}
