<?php

namespace AlexKudrya\Adminix\Modules\Link;

use AlexKudrya\Adminix\Enums\HttpMethodsEnum;

interface LinkInterface
{
    public function getUri(): string;

    public function getTitle(): ?string;

    public function getIcon(): ?string;

    public function getName(): string;

    public function getParams(): ?array;

    public function getCriteria(): ?array;

    public function getMethod(): HttpMethodsEnum;

    public function setUri(string $uri): static;

    public function setTitle(?string $title): static;

    public function setIcon(?string $icon): static;

    public function setName(?string $name): static;

    public function setParams(?array $params): static;

    public function setCriteria(?array $criteria): static;

    public function setMethod(HttpMethodsEnum $method): static;
}
