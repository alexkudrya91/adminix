<?php

namespace AlexKudrya\Adminix\Modules\Link;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Enums\HttpMethodsEnum;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string uri(?string $uri = null)
 * @method self|string|null title(?string $title = null)
 * @method self|string|null icon(?string $icon = null)
 * @method self|string|null name(?string $name = null)
 * @method self|array|null params(?array $params = null)
 * @method self|array|null criteria(?array $criteria = null)
 * @method self|HttpMethodsEnum method(?HttpMethodsEnum $method = null)
 * @method ModuleTypeEnum type()
 */
class LinkModule implements LinkInterface,AdminixModuleInterface,AdminixTopModuleInterface
{
    use ModuleMagicMethods;

    protected string $uri;
    protected ?string $title = null;
    protected ?string $icon = null;
    protected ?string $name = null;
    protected ?array $params = null;
    protected ?array $criteria = null;
    protected HttpMethodsEnum $method = HttpMethodsEnum::GET;
    protected ModuleTypeEnum $type = ModuleTypeEnum::LINK;

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }

    public function getCriteria(): ?array
    {
        return $this->criteria;
    }

    public function getMethod(): HttpMethodsEnum
    {
        return $this->method;
    }

    public function setUri(string $uri): static
    {
        $this->uri = $uri;
        return $this;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function setIcon(?string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setParams(?array $params): static
    {
        $this->params = $params;
        return $this;
    }

    public function setCriteria(?array $criteria): static
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function setMethod(HttpMethodsEnum $method): static
    {
        $this->method = $method;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }
}
