<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string title(?string $title = null)
 * @method self|array links(?array $links = null)
 */
class SidebarMenu
{
    use ModuleMagicMethods;

    protected string $title = '';

    protected array $links = [];

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getLinks(): array
    {
        return $this->links;
    }

    public function setLinks(array $links): static
    {
        $this->links = $links;
        return $this;
    }
}
