<?php

namespace AlexKudrya\Adminix\Modules\Counter;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\CheckDataSource;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null title(?string $title = null)
 * @method self|string|null name(?string $name = null)
 * @method self|string|null icon(?string $icon = null)
 * @method self|string|null dataSource(?string $data_source = null)
 * @method self|int result(?int $result = null)
 * @method self|array|null criteria(?array $criteria = null)
 * @method ModuleTypeEnum type()
 */
class CounterModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    use CheckDataSource, ModuleMagicMethods;

    protected ?string $title = null;
    protected ?string $name = null;
    protected ?string $icon = null;
    protected ?string $data_source = null;
    protected ?array $criteria = null;
    protected int $result = 0;
    protected ModuleTypeEnum $type = ModuleTypeEnum::COUNTER;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setIcon(?string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function setDataSource(string $data_source): static
    {
        $this->data_source = $data_source;
        return $this;
    }

    public function setCriteria(?array $criteria): static
    {
        $this->criteria = $criteria;
        return $this;
    }

    public function getResult(): int
    {
        return $this->result;
    }

    public function setResult(int $result): static
    {
        $this->result = $result;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }
}
