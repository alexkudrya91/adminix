<?php

namespace AlexKudrya\Adminix\Modules\Trait;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use Exception;

/**
 * @implements AdminixModuleInterface
 */
trait ModuleMagicMethods
{
    /**
     * @param string $name
     * @param array $arguments
     * @return self|null
     * @throws Exception
     */
    public static function __callStatic(string $name, array $arguments)
    {
        if (str_contains($name, '__')) return null;

        $obj = new static();

        $method = 'set' . ucfirst($name);

        if (method_exists($obj, $method)) {
            $obj->$method($arguments[0]);

            return $obj;
        }

        throw new Exception('Invalid ' . __CLASS__ . ' method name ' . $method);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call(string $name, array $arguments)
    {
        if (str_contains($name, '__')) return null;

        if (!$arguments) {
            $method = 'get' . ucfirst($name);

            if (!method_exists($this, $method)) {
                $method = 'is' . ucfirst($name);
            }

            if (!method_exists($this, $method)) {
                $method = 'has' . ucfirst($name);
            }

            if (method_exists($this, $method)) {
                return $this->$method();
            }
        } else {
            $method = 'set' . ucfirst($name);

            if (method_exists($this, $method)) {
                return $this->$method($arguments[0]);
            }
        }

        throw new Exception('Invalid ' . __CLASS__ . ' method name ' . $method);
    }
}
