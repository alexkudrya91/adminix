<?php

namespace AlexKudrya\Adminix\Modules\Trait;

use Illuminate\Database\Eloquent\Model;

trait CheckDataSource
{
    public function isDataSourceModel(): bool
    {
        return class_exists($this->data_source) && in_array(Model::class, class_parents($this->data_source));
    }

    public function isDataSourceDB(): bool
    {
        return !$this->isDataSourceModel();
    }
}
