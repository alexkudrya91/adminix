<?php

namespace AlexKudrya\Adminix\Modules;

enum ModuleTypeEnum: string
{
    case CHART_LINE = 'chart_line';
    case CHART_BAR = 'chart_bar';
    case COUNTER = 'counter';
    case LIST = 'list';
    case RESOURCE = 'resource';
    case NEW_RESOURCE = 'new_resource';
    case LINK = 'link';
    case ADMINIX_LINK = 'adminix_link';
}
