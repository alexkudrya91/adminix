<?php

namespace AlexKudrya\Adminix\Modules;

use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string field(?string $name = null)
 * @method self|string direction(?string $value = null)
 */
class Sorting
{
    use ModuleMagicMethods;

    const AVAILABLE_DIRECTIONS = ['asc', 'desc'];

    protected ?string $field = null;
    protected mixed $direction = 'asc';

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;
        return $this;
    }

    public function getDirection(): mixed
    {
        return $this->direction;
    }

    public function setDirection(string $direction): self
    {
        if (!in_array($direction, self::AVAILABLE_DIRECTIONS)) {
            throw new \Exception("Direction '{$direction}' not allowed! It can be either 'asc' or 'desc'.");
        }
        $this->direction = $direction;
        return $this;
    }
}
