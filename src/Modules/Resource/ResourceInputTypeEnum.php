<?php

namespace AlexKudrya\Adminix\Modules\Resource;

enum ResourceInputTypeEnum: string
{
    case STRING = 'string';
    case EMAIL = 'email';
    case INTEGER = 'integer';
    case BOOLEAN = 'boolean';
    case DATETIME = 'datetime';
    case DATE = 'date';
    case TIME = 'time';
    case WEEK = 'week';
    case MONTH = 'month';
    case IMAGE = 'image';
    case SELECT = 'select';
    case HIDDEN = 'hidden';
    case TEXT = 'text';
    case TEXTAREA = 'textarea';
    case EDITOR = 'editor';
    case CKEDITOR = 'ckeditor';
    case WYSIWYG = 'wysiwyg';
    case RANGE = 'range';
    case COLOR = 'color';
    case PASSWORD = 'password';
    case PHONE = 'phone';
    case TEL = 'tel';
}
