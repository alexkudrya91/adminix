<?php

namespace AlexKudrya\Adminix\Modules\Resource;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;

/**
 * @method self|string|null title(?string $title = null)
 * @method self|string name(?string $name = null)
 * @method self|string dataSource(?string $data_source = null)
 * @method self|string|null resource(?string $resource = null)
 * @method self|array|null validation(?array $validation = null)
 * @method self|array|null data(?array $data = null)
 * @method self|string|null successRedirectUri(?string $success_redirect_uri = null)
 * @method array fields()
 * @method array props()
 */
class NewResourceModule extends ResourceModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    protected ModuleTypeEnum $type = ModuleTypeEnum::NEW_RESOURCE;

    public function isReadonly(): bool
    {
        return false;
    }

    public function readonly(): static
    {
        return $this;
    }
}
