<?php

namespace AlexKudrya\Adminix\Modules\Resource;

use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\AdminixTopModuleInterface;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string|null title(?string $title = null)
 * @method self|string name(?string $name = null)
 * @method self|string dataSource(?string $data_source = null)
 * @method self|string|null resource(?string $resource = null)
 * @method self|array|null validation(?array $validation = null)
 * @method self|array|null data(?array $data = null)
 * @method self|string|null successRedirectUri(?string $success_redirect_uri = null)
 * @method array fields()
 * @method array props()
 */
class ResourceModule implements AdminixModuleInterface, AdminixTopModuleInterface
{
    use ModuleMagicMethods;

    protected ?string $name = null;
    protected ?string $title = null;
    protected ?string $data_source = null;
    protected ?string $resource = null;
    protected array $validation = [];
    protected array $data = [];
    protected bool $readonly = false;
    protected ?string $success_redirect_uri = null;
    protected ModuleTypeEnum $type = ModuleTypeEnum::RESOURCE;

    /** @var ResourceField[] */
    protected array $fields = [];

    /** @var ResourceProperty[] */
    protected array $props = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getDataSource(): string
    {
        return $this->data_source;
    }

    public function setDataSource(string $data_source): static
    {
        $this->data_source = $data_source;
        return $this;
    }

    public function getResource(): ?string
    {
        return $this->resource;
    }

    public function setResource(?string $resource): static
    {
        $this->resource = $resource;
        return $this;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function addField(ResourceField $field): static
    {
        $this->fields[] = $field;
        return $this;
    }

    public function addFields(ResourceField ...$fields): static
    {
        foreach ($fields as $field) {
            $this->fields[] = $field;
        }

        return $this;
    }

    public function getProps(): array
    {
        return $this->props;
    }

    public function addProp(ResourceProperty $property): static
    {
        $this->props[] = $property;
        return $this;
    }

    public function addProps(ResourceProperty ...$props): static
    {
        foreach ($props as $property) {
            $this->props[] = $property;
        }

        return $this;
    }

    public function getValidation(): array
    {
        return $this->validation;
    }

    public function setValidation(array $validation): static
    {
        $this->validation = $validation;
        return $this;
    }

    public function addFieldValidation(string $field, array $validation): static
    {
        $this->validation[$field] = $validation;
        return $this;
    }

    public function isReadonly(): bool
    {
        return $this->readonly;
    }

    public function readonly(): static
    {
        $this->readonly = true;
        return $this;
    }

    public function getSuccessRedirectUri(): ?string
    {
        return $this->success_redirect_uri;
    }

    public function setSuccessRedirectUri(?string $success_redirect_uri): static
    {
        $this->success_redirect_uri = $success_redirect_uri;
        return $this;
    }

    public function getType(): ModuleTypeEnum
    {
        return $this->type;
    }
}
