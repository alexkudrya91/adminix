<?php

namespace AlexKudrya\Adminix\Modules\Resource;

use AlexKudrya\Adminix\Modules\Property;

/**
 * @method self|string key(?string $key = null)
 * @method self|string|null value(?string $value = null)
 */
class ResourceProperty extends Property
{

}
