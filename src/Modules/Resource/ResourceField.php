<?php

namespace AlexKudrya\Adminix\Modules\Resource;

use AlexKudrya\Adminix\Modules\InputSelectSrc;
use AlexKudrya\Adminix\Modules\SelectRecord;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string name(?string $title = null)
 * @method self|ResourceInputTypeEnum type(?ResourceInputTypeEnum $type = null)
 * @method self|string field(?string $field = null)
 * @method self|InputSelectSrc src(?InputSelectSrc $src = null)
 * @method self|array|null selectRecords(?array $select_records = null)
 * @method self|array|null validation(?array $validation = null)
 * @method self|int|float|null min(?int|float $mix = null)
 * @method self|int|float|null max(?int|float $max = null)
 * @method self|int|float|null step(?int|float $step = null)
 * @method self|mixed|null value(?mixed $value = null)
 */
class ResourceField
{
    use ModuleMagicMethods;

    protected ?string $name = null;
    protected ResourceInputTypeEnum $type = ResourceInputTypeEnum::STRING;
    protected string $field;
    protected ?InputSelectSrc $src = null;
    protected bool $required = false;
    protected bool $readonly = false;
    protected bool $confirmed = false;
    protected ?array $validation = null;
    protected mixed $value = null;

    protected int|float|null $min = null;
    protected int|float|null $max = null;
    protected int|float|null $step = null;

    /** @var SelectRecord[] */
    protected ?array $select_records = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getType(): ResourceInputTypeEnum
    {
        return $this->type;
    }

    public function setType(ResourceInputTypeEnum $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): static
    {
        $this->field = $field;
        return $this;
    }

    public function getSrc(): ?InputSelectSrc
    {
        return $this->src;
    }

    public function setSrc(?InputSelectSrc $src): static
    {
        $this->src = $src;
        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function required(): static
    {
        $this->required = true;
        $this->addValidationRule('required');

        return $this;
    }

    public function isReadonly(): bool
    {
        return $this->readonly;
    }

    public function readonly(): static
    {
        $this->readonly = true;
        return $this;
    }

    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }

    public function confirmed(): static
    {
        $this->confirmed = true;
        $this->addValidationRule('confirmed');

        return $this;
    }

    public function getValidation(): ?array
    {
        return $this->validation;
    }

    public function setValidation(?array $validation): static
    {
        $this->validation = $validation;
        return $this;
    }

    public function addValidationRule(string $rule): static
    {
        $this->validation[] = $rule;
        return $this;
    }

    public function addValidationRules(string ...$rules): static
    {
        foreach ($rules as $rule) {
            $this->validation[] = $rule;
        }

        return $this;
    }

    public function getSelectRecords(): ?array
    {
        return $this->select_records;
    }

    public function setSelectRecords(?array $select_records): static
    {
        $this->select_records = $select_records;
        return $this;
    }

    public function addSelectRecord(SelectRecord $record): static
    {
        $this->select_records[] = $record;
        return $this;
    }

    public function addSelectRecords(SelectRecord ...$records): static
    {
        foreach ($records as $record) {
            $this->select_records[] = $record;
        }

        return $this;
    }

    public function getMin(): float|int|null
    {
        return $this->min;
    }

    public function setMin(float|int $min): static
    {
        $this->min = $min;
        return $this;
    }

    public function getMax(): float|int|null
    {
        return $this->max;
    }

    public function setMax(float|int $max): static
    {
        $this->max = $max;
        return $this;
    }

    public function getStep(): float|int|null
    {
        return $this->step;
    }

    public function setStep(float|int $step): static
    {
        $this->step = $step;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue(mixed $value): static
    {
        $this->value = $value;
        return $this;
    }
}
