<div class="d-flex flex-column flex-shrink-0 p-3 text-bg-dark adminix-sidebar">
    <div class="d-flex align-items-center mb-3 ms-2 mb-md-0 me-md-auto text-white hover-purple-text">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-feather"
             viewBox="0 0 16 16">
            <path d="M15.807.531c-.174-.177-.41-.289-.64-.363a3.765 3.765 0 0 0-.833-.15c-.62-.049-1.394 0-2.252.175C10.365.545 8.264 1.415 6.315 3.1c-1.95 1.686-3.168 3.724-3.758 5.423-.294.847-.44 1.634-.429 2.268.005.316.05.62.154.88.017.04.035.082.056.122A68.362 68.362 0 0 0 .08 15.198a.528.528 0 0 0 .157.72.504.504 0 0 0 .705-.16 67.606 67.606 0 0 1 2.158-3.26c.285.141.616.195.958.182.513-.02 1.098-.188 1.723-.49 1.25-.605 2.744-1.787 4.303-3.642l1.518-1.55a.528.528 0 0 0 0-.739l-.729-.744 1.311.209a.504.504 0 0 0 .443-.15c.222-.23.444-.46.663-.684.663-.68 1.292-1.325 1.763-1.892.314-.378.585-.752.754-1.107.163-.345.278-.773.112-1.188a.524.524 0 0 0-.112-.172ZM3.733 11.62C5.385 9.374 7.24 7.215 9.309 5.394l1.21 1.234-1.171 1.196a.526.526 0 0 0-.027.03c-1.5 1.789-2.891 2.867-3.977 3.393-.544.263-.99.378-1.324.39a1.282 1.282 0 0 1-.287-.018Zm6.769-7.22c1.31-1.028 2.7-1.914 4.172-2.6a6.85 6.85 0 0 1-.4.523c-.442.533-1.028 1.134-1.681 1.804l-.51.524-1.581-.25Zm3.346-3.357C9.594 3.147 6.045 6.8 3.149 10.678c.007-.464.121-1.086.37-1.806.533-1.535 1.65-3.415 3.455-4.976 1.807-1.561 3.746-2.36 5.31-2.68a7.97 7.97 0 0 1 1.564-.173Z"/>
        </svg>
        <span class="ms-3 fs-4 sidebar-title">{{$attributes['sidebar']->title() ?? 'AdminiX'}}</span>
    </div>

    <hr>

    <ul class="nav nav-pills links-list mb-auto">
        @foreach($attributes['sidebar']->links() as $link)
            <li class="nav-item text-nowrap adminix-menu-item">
                <a
                        @if (is_a($link, AlexKudrya\Adminix\Modules\Link\MenuLinkModule::class) || is_a($link, AlexKudrya\Adminix\Modules\Link\AdminixLinkModule::class))
                            href="{{ route(\AlexKudrya\Adminix\Providers\AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => $link->getUri()]) }}"
                        @elseif (is_a($link, AlexKudrya\Adminix\Modules\Link\LinkModule::class))
                            href="{{ route($link->getUri(), $link->getParams()) }}"
                        @endif
                        class="nav-link text-white mb-2 @if($attributes['uri'] === $link->getUri()) active @endif"
                        aria-current="page">
                    @if ($link->getIcon()) <i class="{{$link->getIcon()}} me-3"></i>@endif
                    {{$link->getTitle()}}
                </a>
            </li>
        @endforeach
    </ul>

    <div class="nav-item text-nowrap">
        <button class="theme-switcher nav-link text-white py-2 px-4">
            <div class="switcher-btn switcher-auto">
                <i class="bi bi-circle-half me-3"></i> <span class="hover-text">System</span>
            </div>
            <div class="switcher-btn switcher-light" style="display: none">
                <i class="bi bi-sun-fill me-3"></i> <span class="hover-text">Light</span>
            </div>
            <div class="switcher-btn switcher-dark" style="display: none">
                <i class="bi bi-moon-stars-fill me-3"></i> <span class="hover-text">Dark</span>
            </div>
        </button>
    </div>

    @if(!config('adminix.no_auth_access'))
        <hr>

        <div class="nav-item logout-btn text-nowrap">
            <a href="{{route('adminix_login_logout')}}" class="nav-link text-white py-2 px-4">
                <i class="bi bi-box-arrow-left me-3"></i> Logout
            </a>
        </div>
    @endif
</div>
