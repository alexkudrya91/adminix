@php
    /** @var AlexKudrya\Adminix\Modules\Counter\CounterModule $module_data */
    $module_data = $attributes['module_data'];

    $title = $module_data->title();
    $result = $module_data->result();
    $icon = $module_data->icon();
@endphp

<div class="counter-module col-6 col-xl-3">
    @if($title ?? null)
    <div class="adminix-counter" title="{{ $title }}: {{ $result }}">
        <div class="adminix-counter-title">{{ $title }}</div>
        <div class="adminix-counter-value">{{ $result }}</div>
        @if($icon ?? null)
            <i class="{{ $icon }}"></i>
        @endif
    </div>
    @endif
</div>
