@php
    use AlexKudrya\Adminix\Modules\List\ListFieldTypeEnum;
    use AlexKudrya\Adminix\Modules\Link\LinkInterface;
    use AlexKudrya\Adminix\Modules\List\ListFilterRecord;
    use AlexKudrya\Adminix\Modules\List\ListFilterInterface;
    use AlexKudrya\Adminix\Modules\List\ListFilter;
    use AlexKudrya\Adminix\Modules\List\ListDateFilter;
    use AlexKudrya\Adminix\Modules\List\ListDateRangeFilter;
    use AlexKudrya\Adminix\Enums\HttpMethodsEnum;
    use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
    use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
    use AlexKudrya\Adminix\Modules\SelectRecord;
    use Illuminate\Support\Carbon;

    /** @var AlexKudrya\Adminix\Modules\List\ListModule $module_data */
    $module_data = $attributes['module_data'];
    $page_name = $attributes['page_name'];
    $primary_key = $module_data->primaryKey();
    $title = $module_data->title();
    $new_item_link = $module_data->newItemLink();
    $search = $module_data->search();
    $name = $module_data->name();
    $filters = $module_data->filters();
    $fields = $module_data->fields();
    $editable = $module_data->isEditable();
    $actions = $module_data->actions();
    $data = $module_data->data();
    $data_source = $module_data->dataSource();
@endphp

<div class="list">

    {{-- TITLE --}}

    @if($title ?? null)
        <div class="list-header">
            <h2>{{ $title }}</h2>
        </div>
    @endif

    {{-- TITLE END --}}

    <div class="container justify-content-center m-2">

        {{-- NEW ITEM LINK --}}

        @if($new_item_link instanceof LinkInterface)
            <x-vendor.adminix.modules.link :module_data="$new_item_link"/>
        @endif

        {{-- NEW ITEM LINK END --}}

        {{-- SEARCH --}}

        @if($search)
            <form class="p-3 search-form d-inline-block text-nowrap col-9"
                  data-module="{{ $name }}">
                <input class="form-control search-input d-inline-block" type="search" name="query"
                       value="{{ $search->query() ?? ''}}"
                       @if($search->placeholder() ?? null) placeholder="{{ $search->placeholder() }}" @endif
                >
                <button class="btn d-inline-block ms-2">Search</button>
            </form>
        @endif

        {{-- SEARCH END --}}

    </div>

    {{-- FILTERS --}}

    @if(!empty($filters ?? []))
        <div class="row justify-content-start m-2">
            @php /** @var $filter ListFilterInterface */ @endphp
            @foreach($filters as $filter)
                @if($filter instanceof ListFilter)
                    <div class="nowrap row col-auto">
                        <div class="col-auto mb-2">
                            <label class="filter-label"
                                   for="filter{{ $name }}{{ $filter->name() }}">{{ $filter->name() }}
                                :</label>
                        </div>
                        <div class="col-auto mb-2 ps-0">
                            <select data-module="{{ $name }}"
                                    data-field="{{ $filter->field() }}"
                                    class="adminix-filter form-control"
                                    id="filter{{ $name }}{{ $filter->name() }}">
                                <option value=""></option>
                                @php /** @var $record ListFilterRecord */ @endphp
                                @foreach($filter->filterRecords() as $record)
                                    <option value="{{ (string) $record->value() }}"
                                            @if($filter->value() === (string) $record->value()) selected @endif>{{$record->name()}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @elseif($filter instanceof ListDateFilter)
                    <div class="nowrap row col-auto">

                        <div class="col-auto mb-2">
                            <label class="filter-label"
                                   for="filter{{ $name }}{{ $filter->name() }}">{{ $filter->name() }}
                            </label>
                        </div>

                        <div class="col-auto mb-2 ps-0">
                            <input
                                class="form-control adminix-filter date-filter"
                                type="date"
                                data-module="{{ $name }}"
                                data-field="{{ $filter->field() }}"
                                id="filter{{ $name }}{{ $filter->name() }}"
                                value="{{ $filter->value() }}"/>
                        </div>

                    </div>
                @elseif($filter instanceof ListDateRangeFilter)
                    <div class="nowrap row col-auto">

                        @if($filter->hasFilterFrom() || $filter->hasFilterTo())
                            <div class="col-auto mb-2">
                                <label class="filter-label"
                                       for="filter{{ $name }}{{ $filter->name() }}">{{ $filter->name() }}
                                </label>
                            </div>
                        @endif

                        @if($filter->hasFilterFrom())
                            <div class="col-auto mb-2 ps-0">
                                <label class="filter-label"
                                       for="filter{{ $name }}{{ $filter->name() }}from">From
                                    :</label>
                            </div>

                            <div class="col-auto mb-2 ps-0">
                                <input
                                        class="form-control adminix-filter @if($filter->isWithTime()) datetime-filter @else date-filter @endif"
                                        @if($filter->isWithTime()) type="datetime-local" @else type="date" @endif
                                        data-module="{{ $name }}"
                                        data-field="{{ $filter->field() }}-from"
                                        id="filter{{ $name }}{{ $filter->name() }}from"
                                        value="{{ $filter->valueFrom() }}"/>
                            </div>
                        @endif

                        @if($filter->hasFilterTo())
                            <div class="col-auto mb-2 ps-0">
                                <label class="filter-label"
                                       for="filter{{ $name }}{{ $filter->name() }}to">To
                                    :</label>
                            </div>

                            <div class="col-auto mb-2 ps-0">
                                <input
                                        class="form-control adminix-filter @if($filter->isWithTime()) datetime-filter @else date-filter @endif"
                                        @if($filter->isWithTime()) type="datetime-local" @else type="date" @endif
                                        data-module="{{ $name }}"
                                        data-field="{{ $filter->field() }}-to"
                                        id="filter{{ $name }}{{ $filter->name() }}to"
                                        value="{{ $filter->valueTo() }}"/>
                            </div>
                        @endif

                    </div>
                @endif
            @endforeach
        </div>
    @endif

    {{-- FILTERS END --}}

    {{-- TABLE --}}

    <table class="table">
        <thead>
        @foreach($fields as $field)
            @if($field->type() === ListFieldTypeEnum::HIDDEN)
                @continue
            @endif
            <th @if(in_array($field->type(), [ListFieldTypeEnum::BOOLEAN, ListFieldTypeEnum::INTEGER, ListFieldTypeEnum::FLOAT])) class="text-center" @endif>
                @if($field->isSortable())
                    <span @if($field->sorting() ?? null) class="sortable {{ $field->sorting() }} text-nowrap"
                          @else class="sortable text-nowrap"
                          @endif
                          data-field="{{ $field->field() }}"
                          data-module="{{ $name }}">
                @endif
                        {{ $field->name() }}
                        @if($field->isSortable() ?? false)
                            <i class="bi bi-caret-up-fill asc"></i>
                            <i class="bi bi-caret-down-fill desc"></i>
                    </span>
                @endif
            </th>
        @endforeach

        @if(($editable) === true)
            <th></th>
        @endif

        @if(!empty($actions ?? []))
            <th></th>
        @endif
        </thead>

        <tbody>
        @foreach(($data['data'] ?? $data) as $item)
            @if($editable === true)
                @php $form_id = 'form_' . $name . '_' . $item[$primary_key]; @endphp
                <form id="{{ $form_id }}" action="{{ route('adminix_save_resource') }}" method="POST">
                    <input type="hidden" name="_primary_key" value="{{ $primary_key }}">
                    <input type="hidden" name="adminix_data_source" value="{{ $data_source }}">
                    <input type="hidden" name="{{ $primary_key }}" value="{{ $item[$primary_key] }}">
                    <input type="hidden" name="adminix_module_name" value="{{ $name }}">
                </form>
            @endif
            <tr class="editable-row">

                @foreach($fields as $field)
                    @if($field->type() !== ListFieldTypeEnum::HIDDEN)
                        <td @if(in_array($field->type(), [ListFieldTypeEnum::BOOLEAN, ListFieldTypeEnum::INTEGER, ListFieldTypeEnum::FLOAT])) class="text-center" @endif> @endif
                            @switch($field->type())
                                @case(ListFieldTypeEnum::STRING)
                                    @if($editable === true && $field->isEditable() === true)
                                        <input class="form-control"
                                               form="{{  $form_id }}"
                                               type="text"
                                               placeholder="{{ $field->name() }}"
                                               name="{{ $field->field() }}"
                                               value="{{ $item[$field->field()] }}"
                                               @if($field->isRequired() === true) required @endif/>
                                    @else
                                        {{$item[$field->field()]}}
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::EMAIL)
                                    @if($editable === true && $field->isEditable() === true)
                                        <input class="form-control"
                                               form="{{$form_id}}"
                                               type="email"
                                               placeholder="{{ $field->name() }}"
                                               name="{{ $field->field() }}"
                                               value="{{ $item[$field->field()] }}"
                                               @if($field->isRequired() === true) required @endif/>
                                    @else
                                        {{ $item[$field->field()] }}
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::INTEGER)
                                @case(ListFieldTypeEnum::FLOAT)
                                    @if($editable === true && $field->isEditable() === true)
                                        <input class="form-control"
                                               form="{{$form_id}}"
                                               type="number"
                                               placeholder="{{ $field->name() }}"
                                               name="{{ $field->field() }}"
                                               value="{{ $item[$field->field()] }}"
                                               @if($field->isRequired() === true) required @endif/>
                                    @else
                                        {{ $item[$field->field()] }}
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::BOOLEAN)
                                    @if($editable === true && $field->isEditable() === true)
                                        <div class="form-check form-switch">
                                            <input type="hidden"
                                                   form="{{ $form_id }}"
                                                   name="{{ $field->field() }}"
                                                   value="off">
                                            <input class="form-check-input"
                                                   form="{{ $form_id }}"
                                                   type="checkbox"
                                                   role="switch"
                                                   name="{{ $field->field() }}"
                                                   @if($item[$field->field()] === true) checked @endif/>
                                        </div>
                                    @else
                                        <span class="adminix-badge {{$item[$field->field()] ? 'green-badge' : 'red-badge'}}">
                                            {{$item[$field->field()] ? 'Yes' : 'No'}}
                                        </span>
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::DATETIME)
                                    @if($editable === true && $field->isEditable() === true)
                                        <input class="form-control"
                                               form="{{ $form_id }}"
                                               type="datetime-local"
                                               placeholder="{{ $field->name() }}"
                                               name="{{ $field->field() }}"
                                               value="{{ $item[$field->field()] }}"
                                               @if(($field->isRequired() ?? false) === true) required @endif/>
                                    @else
                                        @if($item[$field->field()])
                                            {{(new Carbon($item[$field->field()]))->toDateTimeString()}}
                                        @endif
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::DATE)
                                    @if($editable === true && $field->isEditable() === true)
                                        <input class="form-control"
                                               form="{{ $form_id }}"
                                               type="date"
                                               placeholder="{{ $field->name() }}"
                                               name="{{ $field->field() }}"
                                               value="{{ $item[$field->field()] }}"
                                               @if(($field->isRequired() ?? false) === true) required @endif/>
                                    @else
                                        @if($item[$field->field()])
                                            {{(new Carbon($item[$field->field()]))->toDateString()}}
                                        @endif
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::IMAGE)
                                    @if($item[$field->field()])
                                        <img class="admin-list-avatar" src="{{$item[$field->field()]}}"/>
                                    @endif
                                    @break
                                @case(ListFieldTypeEnum::SELECT)
                                    @if($editable === true && $field->isEditable() === true)
                                        <select class="form-control"
                                                form="{{ $form_id }}"
                                                name="{{ $field->field() }}"
                                                @if($field->isRequired() === true) required @endif>
                                            @foreach($field->selectRecords() as $select_record)
                                                @php /** @var SelectRecord $select_record */ @endphp
                                                <option value="{{ $select_record->value() }}"
                                                        @if($item[$field->field()] == $select_record->value()) selected @endif>
                                                    {{ $select_record->name() }}
                                                </option>
                                            @endforeach
                                        </select>
                                    @else
                                        @if($item[$field->field()])
                                            @foreach($field->selectRecords() as $select_record)
                                                @php /** @var SelectRecord $select_record */ @endphp
                                                @if($item[$field->field()] == $select_record->value())
                                                    {{ $select_record->name() }}
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                    @break
                            @endswitch
                            @if(ListFieldTypeEnum::HIDDEN) </td>
                    @endif
                @endforeach

                @if($editable === true)
                    <input form="{{ $form_id }}" type="hidden" name="adminix_data_source" value="{{ $data_source }}">
                    <input form="{{ $form_id }}" type="hidden" name="adminix_module_name" value="{{ $name }}">
                    <input form="{{ $form_id }}" type="hidden" name="adminix_page_name" value="{{ $page_name }}">
                @endif

                @if($editable === true)
                    <td class="text-nowrap">
                        <button class="btn btn-circle editable-list-btn"
                                type="submit"
                                form="{{ $form_id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-floppy" viewBox="0 0 16 16">
                                <path d="M11 2H9v3h2z"/>
                                <path
                                        d="M1.5 0h11.586a1.5 1.5 0 0 1 1.06.44l1.415 1.414A1.5 1.5 0 0 1 16 2.914V14.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5v-13A1.5 1.5 0 0 1 1.5 0M1 1.5v13a.5.5 0 0 0 .5.5H2v-4.5A1.5 1.5 0 0 1 3.5 9h9a1.5 1.5 0 0 1 1.5 1.5V15h.5a.5.5 0 0 0 .5-.5V2.914a.5.5 0 0 0-.146-.353l-1.415-1.415A.5.5 0 0 0 13.086 1H13v4.5A1.5 1.5 0 0 1 11.5 7h-7A1.5 1.5 0 0 1 3 5.5V1H1.5a.5.5 0 0 0-.5.5m3 4a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 .5-.5V1H4zM3 15h10v-4.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5z"/>
                            </svg>
                        </button>

                        <button class="btn btn-circle editable-list-btn"
                                type="reset"
                                form="{{ $form_id }}">
                            <i class="bi bi-arrow-repeat"></i>
                        </button>
                    </td>
                @endif

                @if(!empty($item['_actions']))
                    <td class="text-nowrap">
                        @foreach($item['_actions'] as $action)
                            @php
                                $link = '';

                                if ($action->type() === ModuleTypeEnum::ADMINIX_LINK) {
                                    $link = route(AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => $action->uri()]);

                                    if (!empty($action->params() ?? [])) {
                                        foreach($action->params() as $param) {
                                            $link .= '/' . $param;
                                        }
                                    }
                                }

                                 if ($action->type() === ModuleTypeEnum::LINK) {
                                    $params = [];
                                    if (!empty($action->params() ?? [])) {
                                        foreach($action->params() as $param) {
                                            $params[] = $param;
                                        }
                                    }

                                    $link = route($action->uri(), $params);
                                }
                            @endphp

                            @if(($action->method() ?? HttpMethodsEnum::GET) === HttpMethodsEnum::GET)
                                <a class="btn text-nowrap" href="{{ $link }}">
                                    @if($action->icon() ?? null)
                                        <i class="{{ $action->icon() }}"></i>
                                    @endif

                                    @if($action->title() ?? null)
                                        {{ $action->title() }}
                                    @elseif($action->name() ?? null)
                                        {{ $action->name() }}
                                    @endif
                                </a>
                            @else
                                <form class="d-inline" action="{{ $link }}" method="{{ $action->method()?->value }}">
                                    <button class="btn text-nowrap" type="submit">
                                        @if($action->icon() ?? null)
                                            <i class="{{ $action->icon() }}"></i>
                                        @endif

                                        @if($action->title() ?? null)
                                            {{ $action->title() }}
                                        @elseif($action->name() ?? null)
                                            {{ $action->name() }}
                                        @endif
                                    </button>
                                </form>
                            @endif
                        @endforeach
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>

    {{-- TABLE END --}}

    {{-- PAGINATION --}}
    @if (($module_data->pagination() ?? 0) > 0 && $data['last_page'] != 1)
        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">
                <li class="page-item @if($data['current_page'] === 1) disabled @endif">
                    <a class="page-link px-0" href="{{$data['first_page_url']}}">1</a>
                </li>

                <li class="page-item @if(is_null($data['prev_page_url'])) disabled @endif">
                    <a class="page-link px-0" href="{{ $data['prev_page_url'] ?? '#' }}">
                        <i class="bi bi-caret-left"></i>
                    </a>
                </li>

                @foreach($data['links'] as $key => $link)
                    @if(is_numeric($link['label'])
                        && $link['label'] <= ($data['current_page'] + 3)
                        && $link['label'] >= ($data['current_page'] - 3))
                        <li class="page-item @if($data['current_page'] === (int) $link['label']) active @endif">
                            <a class="page-link px-0" href="{{$link['url']}}">{{ $link['label'] }}</a>
                        </li>
                    @endif
                @endforeach

                <li class="page-item @if(is_null($data['next_page_url'])) disabled @endif">
                    <a class="page-link px-0" href="{{$data['next_page_url'] ?? '#'}}">
                        <i class="bi bi-caret-right"></i>
                    </a>
                </li>

                <li class="page-item @if($data['current_page'] === $data['last_page']) disabled @endif">
                    <a class="page-link px-0" href="{{$data['last_page_url']}}">{{$data['last_page']}}</a>
                </li>
            </ul>
        </nav>
    @endif

    {{-- PAGINATION END --}}
</div>
