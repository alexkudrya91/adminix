@php
    /** @var AlexKudrya\Adminix\Modules\Clock\ClockModule $module_data */
    $module_data = $attributes['module_data'];

    $id = $module_data->id();
    $title = $module_data->title();
    $locale = $module_data->locale();
    $is_time = $module_data->isTimeEnabled();
    $is_date = $module_data->isDateEnabled();
@endphp

@if($is_time || $is_date)
<div class="clock-module col-12">
    @if($title ?? null)
    <div class="adminix-clock" id="{{ $id }}" @if($title) title="{{ $title }}" @endif>
        @if($is_time)
        <div class="adminix-clock-time"></div>
        @endif

        @if($is_date)
        <div class="adminix-clock-date">
            <div class="adminix-clock-weekday"></div>
            <div class="adminix-clock-day"></div>
            <div class="adminix-clock-year"></div>
        </div>
        @endif
    </div>
    @endif
</div>

<script>
    function setClock() {
        @if($is_date)
        $('#{{ $id }} .adminix-clock-date .adminix-clock-weekday').text(moment().locale('{{ $locale }}').format('dddd').toUpperCase())
        $('#{{ $id }} .adminix-clock-date .adminix-clock-day').text(moment().locale('{{ $locale }}').format('MMMM DD').toUpperCase())
        $('#{{ $id }} .adminix-clock-date .adminix-clock-year').text(moment().locale('{{ $locale }}').format('YYYY'))
        @endif

        @if($is_time)
        $('#{{ $id }} .adminix-clock-time').text(moment().locale('{{ $locale }}').format('HH:mm:ss').toUpperCase())
        @endif

        setTimeout(setClock, 1000);
    }

    setClock()
</script>
@endif