@php
    /** @var \AlexKudrya\Adminix\Modules\Link\LinkModule $module */
    $module = $attributes['module_data'];

    $link = '';
    if ($module->type() === \AlexKudrya\Adminix\Enums\ModuleTypeEnum::ADMINIX_LINK) {
        $link = route(\AlexKudrya\Adminix\Providers\AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => $module->uri()]);

        if (!empty($module->params() ?? [])) {
            foreach($module->params() as $param) {
                $link .= '/' . $param;
            }
        }
    }

    if ($module->type() === \AlexKudrya\Adminix\Enums\ModuleTypeEnum::LINK) {
        $params = [];
        if (!empty($module->params() ?? [])) {
            foreach($module->params() as $param) {
                $params[] = $param;
            }
        }

        $link = route($module->uri(), $params);
    }
@endphp

@if($module->method() === \AlexKudrya\Adminix\Enums\HttpMethodsEnum::GET)
    <a class="btn text-nowrap w-auto m-auto my-3" href="{{ $link }}">
        @if($module->icon() ?? null)
            <i class="{{ $module->icon() }}"></i>
        @endif
        @if($module->title() ?? null)
            {{ $module->title() }}
        @endif
    </a>
@else
    <form class="d-inline w-auto m-auto my-3" action="{{ $link }}" method="{{ $module->method()->value }}">
        <button class="btn text-nowrap" type="submit">
            @if($module->icon() ?? null)
                <i class="{{ $module->icon() }}"></i>
            @endif
            @if($module->title() ?? null)
                {{ $module->title() }}
            @endif
        </button>
    </form>
@endif
