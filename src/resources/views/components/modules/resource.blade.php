@php
    use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;
    use AlexKudrya\Adminix\Modules\Resource\ResourceField;
    use AlexKudrya\Adminix\Modules\SelectRecord;

    /** @var AlexKudrya\Adminix\Modules\Resource\ResourceModule  $module_data */
    $module_data = $attributes['module_data'];
    $page_name = $attributes['page_name'];

    $title = $module_data->title();
    $name = $module_data->name();
    $data = $module_data->data();
    $props = $module_data->props();
    $fields = $module_data->fields();
    $data_source = $module_data->dataSource();
    $readonly = $module_data->isReadonly();

    $tabindex = 1;
@endphp

<div class="resource">

    {{-- TITLE --}}
    @if($title ?? null)
        <div class="resource-header">
            <h2>{{ $title }} #{{ $props[0]->value() }}</h2>
        </div>
    @endif

    <form action="{{route('adminix_save_resource')}}" method="POST">
        @foreach($fields as $field)
            @php /** @var ResourceField $field */ @endphp

            <div class="row justify-content-center p-2">
                @switch($field->type())
                    @case(ResourceInputTypeEnum::TEXT)
                    @case(ResourceInputTypeEnum::TEXTAREA)
                        <div class="col-12 col-xl-5 col-lg-7 col-md-6 col-sm-8"><label for="{{ $field->field() }}_field">{{ $field->name() }}</label></div>
                        </div>
                        <div class="row justify-content-center p-2">
                        <div class="col-12 col-xl-5 col-lg-7 col-md-6 col-sm-8">
                        @break
                    @case(ResourceInputTypeEnum::EDITOR)
                    @case(ResourceInputTypeEnum::CKEDITOR)
                    @case(ResourceInputTypeEnum::WYSIWYG)
                        <div class="col-12 col-xl-5 col-lg-7 col-md-6 col-sm-8"><label for="{{ $field->field() }}_field">{{ $field->name() }}</label></div>
                        </div>
                        <div class="row justify-content-center p-2">
                        <div class="col-12 col-xl-10">
                        @break
                    @default
                        <div class="col-12 col-xl-2 col-lg-3 col-md-3 col-sm-4"><label for="{{ $field->field() }}_field">{{ $field->name() }}</label></div>
                        <div class="col-12 col-xl-3 col-lg-4 col-md-5 col-sm-6">
                @endswitch
                @switch($field->type())
                    @case(ResourceInputTypeEnum::IMAGE)
                        @if($data[$field->field()])
                            <img src="{{ $data[$field->field()] }}"/>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::STRING)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               name="{{ $field->field() }}"
                               id="{{ $field->field() }}_field"
                               value="{{ old( $field->field()) ?? $data[$field->field()] }}"
                               tabindex="{{ $tabindex }}"
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{ $field->field() }}" class="invalid-feedback">
                                {{ $errors->get($field->field())[0] }}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::TEXT)
                    @case(ResourceInputTypeEnum::TEXTAREA)
                        <textarea class="form-control @if($errors->has($field->field())) is-invalid @endif"
                                  name="{{ $field->field() }}"
                                  id="{{ $field->field() }}_field"
                                  tabindex="{{ $tabindex }}"
                                  @if($field->isRequired() === true) required @endif>
                            {{old($field->field()) ?? $data[$field->field()]}}
                        </textarea>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::EDITOR)
                    @case(ResourceInputTypeEnum::CKEDITOR)
                    @case(ResourceInputTypeEnum::WYSIWYG)
                        <textarea class="form-control @if($errors->has($field->field())) is-invalid @endif"
                                  name="{{ $field->field() }}"
                                  id="{{ $field->field() }}_field"
                                  tabindex="{{ $tabindex }}"
                                  @if($field->isRequired() === true) required @endif>
                            {{ old($field->field()) ?? $data[$field->field()] }}
                        </textarea>
                        <script>CKEDITOR.replace( '{{ $field->field() }}_field' );</script>
                        @if($errors->has($field->field()))
                            <div id="validation_{{ $field->field() }}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::DATE)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="date"
                               name="{{ $field->field() }}"
                               id="{{ $field->field() }}_field"
                               value="{{old($field->field()) ?? $data[$field->field()]}}"
                               tabindex="{{$tabindex}}"
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::DATETIME)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="datetime-local"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               value="{{old($field->field()) ?? $data[$field->field()]}}"
                               tabindex="{{$tabindex}}"
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::TIME)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="time"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if(is_numeric($field->min())) min="{{ $field->min() }}" @endif
                               @if(is_numeric($field->max())) min="{{ $field->max() }}" @endif
                               @if($field->isRequired() === true) required @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::WEEK)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="week"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if(is_numeric($field->min())) min="{{ $field->min() }}" @endif
                               @if(is_numeric($field->max())) min="{{ $field->max() }}" @endif
                               @if($field->isRequired() === true) required @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::MONTH)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="month"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if(is_numeric($field->min())) min="{{ $field->min() }}" @endif
                               @if(is_numeric($field->max())) min="{{ $field->max() }}" @endif
                               @if($field->isRequired() === true) required @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::INTEGER)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="number"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               value="{{old($field->field()) ?? $data[$field->field()]}}"
                               tabindex="{{$tabindex}}"
                               @if(is_numeric($field->min())) min="{{ $field->min() }}" @endif
                               @if(is_numeric($field->max())) min="{{ $field->max() }}" @endif
                               @if(is_numeric($field->step())) min="{{ $field->step() }}" @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::RANGE)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="range"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if(is_numeric($field->min())) min="{{ $field->min() }}" @endif
                               @if(is_numeric($field->max())) min="{{ $field->max() }}" @endif
                               @if(is_numeric($field->step())) min="{{ $field->step() }}" @endif
                               @if($field->isRequired() === true) required @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::COLOR)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="color"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if($field->isRequired() === true) required @endif
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::EMAIL)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="email"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               value="{{old($field->field()) ?? $data[$field->field()]}}"
                               tabindex="{{$tabindex}}"
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::PASSWORD)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="password"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               tabindex="{{$tabindex}}"
                               value="{{old($field->field())}}"
                               @if($field->isRequired() === true) required @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif

                        @if($field->isConfirmed() === true)
                            @php
                                $tabindex++
                            @endphp

                            <input class="form-control mt-3 @if($errors->has($field->field().'_confirmed')) is-invalid @endif"
                                   type="password"
                                   name="{{$field->field()}}_confirmed"
                                   id="{{$field->field()}}_confirmed_field"
                                   tabindex="{{$tabindex}}"
                                   value="{{old($field->field())}}"
                                   @if($field->isRequired() === true) required @endif/>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::PHONE)
                    @case(ResourceInputTypeEnum::TEL)
                        <input class="form-control @if($errors->has($field->field())) is-invalid @endif"
                               type="tel"
                               name="{{$field->field()}}"
                               id="{{$field->field()}}_field"
                               value="{{old($field->field()) ?? $data[$field->field()]}}"
                               @if($field->isReadonly() === true || ($readonly ?? false) === true) readonly @endif/>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                        @break
                    @case(ResourceInputTypeEnum::BOOLEAN)
                        <input id='{{$field->field()}}_field_hidden' type='hidden' value='off' name='{{$field->field()}}'>
                        <div class="form-check form-switch">
                            <input class="form-check-input"
                                   type="checkbox"
                                   role="switch"
                                   name="{{$field->field()}}"
                                   id="{{$field->field()}}_field"
                                   tabindex="{{$tabindex}}"
                                   @if((old($field->field()) ?? $data[$field->field()]) === true) checked @endif
                                   @if($field->isReadonly() === true || ($readonly ?? false) === true) disabled @endif/>
                        </div>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif

                        @break
                    @case(ResourceInputTypeEnum::SELECT)
                        <select class="form-control @if($errors->has($field->field())) is-invalid @endif"
                                name="{{$field->field()}}"
                                id="{{$field->field()}}_field"
                                tabindex="{{$tabindex}}"
                                @if($field->isReadonly() === true || ($readonly ?? false) === true) disabled @endif>

                            @php /** @var SelectRecord $select_item */ @endphp

                            @foreach($field->selectRecords() as $select_item)
                                <option value="{{ $select_item->value() }}"
                                        @if((old($field->field()) ?? $data[$field->field()]) == $select_item->value()) selected @endif
                                >{{ $select_item->name() }}</option>
                            @endforeach
                        </select>
                        @if($errors->has($field->field()))
                            <div id="validation_{{$field->field()}}" class="invalid-feedback">
                                {{$errors->get($field->field())[0]}}
                            </div>
                        @endif
                @endswitch
                </div>
            </div>

            @php
                $tabindex++
            @endphp
        @endforeach

        <input type="hidden" name="adminix_data_source" value="{{ $data_source }}">
        <input type="hidden" name="adminix_module_name" value="{{ $name }}">
        <input type="hidden" name="adminix_page_name" value="{{ $page_name }}">
        <input type="hidden" name="{{ $props[0]->key() }}" value="{{ $props[0]->value() }}">
        <input type="hidden" name="_primary_key" value="{{ $props[0]->key() }}">

        @csrf

        @if(($readonly ?? false) === false)
            <div class="row justify-content-center my-3">
                <button class="btn w-auto mx-2 px-4" type="submit" tabindex="{{ $tabindex }}">Save</button>
                <button class="btn w-auto mx-2 px-4" type="reset" tabindex="{{ $tabindex+1 }}">Reset</button>
            </div>
        @endif
    </form>
</div>
