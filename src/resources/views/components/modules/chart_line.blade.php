@php
    /** @var AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule $module_data */
    $module_data = $attributes['module_data'];

    $id = $module_data->id();
    $title = $module_data->title();
    $color = $module_data->color();
    $labels = $module_data->labels();
    $values = $module_data->values();

    $jsElementConstName = 'ctx' . $id;
    $jsColorConstName = 'bgColor' . $id;
@endphp

<div class="chart-line-module col-12 col-xl-6">

    <div class="chart-line">
        @if($title ?? null)
            <h2>{{ $title }}</h2>
        @endif

        <canvas id="{{ $id }}"></canvas>
    </div>


    <script>
        const {{ $jsElementConstName }} = document.getElementById('{{ $id }}');

        const {{ $jsColorConstName }} = transparentize('{{ $color }}');

        new Chart({{ $jsElementConstName }}, {
            type: 'line',
            data: {
                labels: [{{implode(', ', $labels)}}],
                datasets: [{
                    label: null,
                    data: [{{implode(', ', $values)}}],
                    fill: true,
                    borderColor: '{{ $color }}',
                    tension: 0.3,
                    backgroundColor: (context) => {
                        return getGradient(context, {{ $jsColorConstName }})
                    }
                }]
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    legend: {
                        display: false
                    }
                }
            }
        });
    </script>

</div>
