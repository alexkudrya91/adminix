@php
    /** @var AlexKudrya\Adminix\Modules\ChartBar\ChartBarModule $module_data */
    $module_data = $attributes['module_data'];

    $title = $module_data->title();
    $id = $module_data->id();
    $labels = $module_data->labels();
    $colors = $module_data->colors();
    $values = $module_data->values();

    $jsElementConstName = 'ctx' . $id;
@endphp

<div class="chart-line-module col-12 col-xl-6">

    <div class="chart-bar">
        @if($title ?? null)
            <h2>{{ $title }}</h2>
        @endif

        <canvas id="{{ $id }}"></canvas>
    </div>

    <script>
        const {{ $jsElementConstName }} = document.getElementById('{{ $id }}');

        new Chart({{ $jsElementConstName }}, {
            type: 'bar',
            data: {
                labels: [@foreach($labels as $label)"{{ $label }}", @endforeach],
                datasets: [{
                    data: [{{ implode(',', $values) }}],
                    fill: true,
                    tension: 0.3,
                    backgroundColor: [@foreach($colors as $color)transparentize('{{$color}}', '80'), @endforeach],
                    borderColor: [@foreach($colors as $color)"{{$color}}", @endforeach],
                    borderWidth: 3
                }]
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    legend: {
                        display: false
                    }
                }
            }
        });
    </script>

</div>
