<!DOCTYPE html>
<html lang="{{ \Illuminate\Support\Facades\App::getLocale() }}" style="height: 100%">
<head>
    <title>
        @if(config('adminix.app_title'))
            {{ config('adminix.app_title') }}
        @else
            AdminiX
        @endif
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/adminix/styles.css')}}">
    <link rel="stylesheet" href="{{asset('css/adminix/styles-dark.css')}}">
    <script src="{{asset('js/adminix/script.js')}}"></script>
</head>
<body class="notranslate d-flex align-items-center py-4 bg-body-tertiary h-100 auth-page @if(($_COOKIE['adminix_dark_theme'] ?? 'false') === 'true') dark-mode @endif">

{{ $slot }}

<button class="theme-switcher nav-link text-white py-2 px-4" style="position: fixed;bottom: 40px;left: 20px;">
    <div class="switcher-btn switcher-auto">
        <i class="bi bi-circle-half me-3"></i> <span class="hover-text">System</span>
    </div>
    <div class="switcher-btn switcher-light" style="display: none">
        <i class="bi bi-sun-fill me-3"></i> <span class="hover-text">Light</span>
    </div>
    <div class="switcher-btn switcher-dark" style="display: none">
        <i class="bi bi-moon-stars-fill me-3"></i> <span class="hover-text">Dark</span>
    </div>
</button>

<div class="bg"></div>

<footer class="text-secondary text-center fixed-bottom mb-3">&copy; Adminix {{ \Illuminate\Support\Carbon::now()->format('Y') }} </footer>
</body>
</html>
