<!DOCTYPE html>
<html lang="{{ \Illuminate\Support\Facades\App::getLocale() }}">
<head>
    <title>
        @if(config('adminix.app_title'))
            {{ config('adminix.app_title') }}
        @else
            AdminiX
        @endif
    </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/adminix/styles.css')}}">
    <link rel="stylesheet" href="{{asset('css/adminix/sidebars.css')}}">
    <link rel="stylesheet" href="{{asset('css/adminix/ckeditor.css')}}">
    <link rel="stylesheet" href="{{asset('css/adminix/styles-dark.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="{{asset('js/adminix/script.js')}}"></script>
    <script src="{{asset('js/adminix/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('js/adminix/moment.js')}}"></script>
</head>
<body class="notranslate @if(($_COOKIE['adminix_dark_theme'] ?? 'false') === 'true') dark-mode @endif">

{{ $slot }}

<div class="bg"></div>

</body>
</html>
