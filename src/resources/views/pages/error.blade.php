<x-vendor.adminix.layouts.auth-layout>
    <main class="error-page w-100 m-auto h-auto text-center">
        <h1 class="text-center hover-purple-text">
            {{ $message }}
        </h1>

        <a class="btn m-auto my-3"
           href="{{route(\AlexKudrya\Adminix\Providers\AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => ''])}}">
            Back to Homepage</a>
    </main>
</x-vendor.adminix.layouts.auth-layout>
