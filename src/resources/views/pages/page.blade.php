<x-vendor.adminix.layouts.layout>
    <main class="d-flex flex-nowrap">
        <x-vendor.adminix.sidebar :sidebar="$sidebar" :uri="$uri"/>
        <div class="container-fluid overflow-y-scroll hide-scrollbar">
            <div class="container position-relative">
                <div class="row">
                    @foreach($modules as $module)

                        @switch($module->type())
                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::LIST)
                                <x-vendor.adminix.modules.list :module_data="$module" :page_name="$name"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::COUNTER)
                                <x-vendor.adminix.modules.counter :module_data="$module"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::CLOCK)
                                <x-vendor.adminix.modules.clock :module_data="$module"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::CHART_BAR)
                                <x-vendor.adminix.modules.chart_bar :module_data="$module"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::CHART_LINE)
                                <x-vendor.adminix.modules.chart_line :module_data="$module"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::RESOURCE)
                                <x-vendor.adminix.modules.resource :module_data="$module" :page_name="$name"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::NEW_RESOURCE)
                                <x-vendor.adminix.modules.new_resource :module_data="$module" :page_name="$name"/>
                                @break

                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::LINK)
                            @case(\AlexKudrya\Adminix\Enums\ModuleTypeEnum::ADMINIX_LINK)
                                <x-vendor.adminix.modules.link :module_data="$module"/>
                                @break
                        @endswitch

                    @endforeach
                </div>

                <footer class="text-center my-3 text-secondary d-block text-center">&copy;
                    Adminix {{ \Illuminate\Support\Carbon::now()->format('Y') }} </footer>

            </div>
        </div>
    </main>
</x-vendor.adminix.layouts.layout>
