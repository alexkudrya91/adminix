<?php

namespace AlexKudrya\Adminix;

interface AdminixPageProvider
{
    public static function get(): AdminixPage;
}
