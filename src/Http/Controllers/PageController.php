<?php

namespace AlexKudrya\Adminix\Http\Controllers;

use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
use AlexKudrya\Adminix\Services\AdminixHelper;
use AlexKudrya\Adminix\Services\PageGenerator;
use AlexKudrya\Adminix\Services\ResourceRepository;
use AlexKudrya\Adminix\Services\Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PageController
{
    public function __construct(
        private AdminixHelper $helper,
        private PageGenerator $generator,
        private Validator $validator,
        private ResourceRepository $repository,
    )
    {}

    public function handleRequest(Request $request)
    {
        $params = explode('/', $request->route('adm_param'));

        if (env('APP_ENV') === 'production') {
            try {
                $page_config = $this->generator->generatePage($params);
            } catch (\Exception $exception) {
                return view('vendor.adminix.error', ['message' => $exception->getMessage()]);
            }
        } else {
            $page_config = $this->generator->generatePage($params);
        }

        return view('vendor.adminix.page', $page_config->toArray());
    }

    public function saveResource(Request $request): RedirectResponse
    {
        $data_source = $request->input('adminix_data_source');
        $module_name = $request->input('adminix_module_name');
        $page_name = $request->input('adminix_page_name');
        $primary_key = $request->input('_primary_key');
        $primary_value = $request->input($primary_key);
        $input_data = Arr::except(
            $request->input(),
            [
                'adminix_data_source',
                'adminix_module_name',
                'adminix_page_name',
                '_token',
                '_primary_key',
                $primary_key
            ]
        );

        $input_data = $this->helper->modifyCheckboxValues($input_data);

        if ($validation = $this->helper->getValidationRules($page_name, $module_name)) {
            $validation_result = $this->validator->validation(
                validation: $validation,
                input_data: $input_data,
                primary_value: $primary_value,
                primary_key: $primary_key,
                ignoreUnique: true
            );

            if (!$validation_result->isValid()) {
                return redirect()
                    ->back()
                    ->withInput($validation_result->getInputData())
                    ->withErrors($validation_result->getMessages());
            }
        }

        $updatable_list = $this->helper->getUpdatablefields($page_name, $module_name);
        $input_data = Arr::only($input_data, $updatable_list);

        $this->repository->updateResource(
            data_source: $data_source,
            input_data: $input_data,
            primary_key: $primary_key,
            primary_value:$primary_value
        );

        return redirect()->back();
    }

    public function createResource(Request $request): RedirectResponse
    {
        $data_source = $request->input('adminix_data_source');
        $module_name = $request->input('adminix_module_name');
        $page_name = $request->input('adminix_page_name');
        $input_data = Arr::except(
            $request->input(),
            [
                'adminix_data_source',
                'adminix_module_name',
                'adminix_page_name',
                '_token'
            ]
        );

        $input_data = $this->helper->modifyCheckboxValues($input_data);

        if ($validation = $this->helper->getValidationRules($page_name, $module_name)) {
            $validation_result = $this->validator->validation(
                validation: $validation,
                input_data: $input_data
            );

            if (!$validation_result->isValid()) {
                return redirect()
                    ->back()
                    ->withInput($validation_result->getInputData())
                    ->withErrors($validation_result->getMessages());
            }
        }

        $updatable_list = $this->helper->getUpdatablefields($page_name, $module_name);
        $input_data = Arr::only($input_data, $updatable_list);

        $this->repository->createResource(
            data_source: $data_source,
            input_data: $input_data
        );

        if ($success_redirect_uri = $this->helper->getModuleConfig($page_name, $module_name, 'success_redirect_uri')) {
            return redirect()->route(AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => $success_redirect_uri]);
        }

        return redirect()->back();
    }
}
