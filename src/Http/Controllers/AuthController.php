<?php

namespace AlexKudrya\Adminix\Http\Controllers;

use AlexKudrya\Adminix\AdminixUserInterface;
use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AuthController
{
    private AdminixUserInterface $user_model;

    public function __construct() {
        $this->user_model = App::make(config('adminix.admin_user_model'));
    }

    public function loginForm(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $title = config('adminix.app_title');

        return view('vendor.adminix.auth.login', ['title' => $title]);
    }

    public function login(Request $request)
    {
        $credentials = array_merge(
            $request->only(['email', 'password']),
            $this->user_model->getAdminCriteria()
        );

        if (!auth()->attempt(
            $credentials,
            $request->post('remember')
        )) {
            return redirect()
                ->route(AdminixServiceProvider::LOGIN_ROUTE)
                ->withInput($request->only(['email']))
                ->withErrors(['email' => 'Invalid credentials']);
        }

        return redirect()->route(AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => '']);
    }

    public function logout(): RedirectResponse
    {
        auth()->logout();

        return redirect()->route(AdminixServiceProvider::LOGIN_ROUTE);
    }
}
