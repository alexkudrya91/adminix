<?php

namespace AlexKudrya\Adminix\Http\Middleware;

use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
use AlexKudrya\Adminix\AdminixUserInterface;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GuestMiddleware
{
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        if (auth()->check() && !(auth()->user() instanceof AdminixUserInterface)) {
            throw new \Exception('User model must implements AdminixUserInterface');
        }

        if (auth()->check() && auth()->user()->admin()) {
            return redirect(route(AdminixServiceProvider::PAGE_ROUTE, ['adm_param' => '']));
        }

        return $next($request);
    }
}
