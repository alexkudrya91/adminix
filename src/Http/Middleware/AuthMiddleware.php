<?php

namespace AlexKudrya\Adminix\Http\Middleware;

use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
use AlexKudrya\Adminix\AdminixUserInterface;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthMiddleware
{
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        if (config('adminix.no_auth_access', false)) {
            return $next($request);
        }

        if (!auth()->check()) {
            return redirect(route(AdminixServiceProvider::LOGIN_ROUTE));
        }

        if (!(auth()->user() instanceof AdminixUserInterface)) {
            throw new \Exception('User model must implements AdminixUserInterface');
        }

        if (!auth()->user()->admin()) {
            return redirect(route(AdminixServiceProvider::LOGIN_ROUTE));
        }

        return $next($request);
    }
}
