<?php

namespace AlexKudrya\Adminix;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\SidebarMenu;
use AlexKudrya\Adminix\Modules\Trait\ModuleMagicMethods;

/**
 * @method self|string uri(?string $uri = null)
 * @method self|string name(?string $name = null)
 * @method self|SidebarMenu sidebar(?SidebarMenu $sidebar = null)
 * @method AdminixModuleInterface[] modules()
 */
class AdminixPage
{
    use ModuleMagicMethods;

    protected bool $home = false;
    protected ?string $uri = null;
    protected ?string $name = null;

    /** @var AdminixModuleInterface[] */
    protected array $modules = [];

    protected SidebarMenu $sidebar;

    public function isHomePage(): bool
    {
        return $this->home;
    }

    public function setAsHomePage(): static
    {
        $this->home = true;
        return $this;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function setUri(string $uri): static
    {
        $this->uri = $uri;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getModules(): array
    {
        return $this->modules;
    }

    public function addModule(AdminixModuleInterface $module): static
    {
        $this->modules[] = $module;
        return $this;
    }

    public function addModules(AdminixModuleInterface ...$modules): static
    {
        foreach ($modules as $module) {
            $this->modules[] = $module;
        }

        return $this;
    }

    public function getSidebar(): SidebarMenu
    {
        return $this->sidebar;
    }

    public function setSidebar(SidebarMenu $sidebar): static
    {
        $this->sidebar = $sidebar;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'home' => $this->isHomePage(),
            'modules' => $this->getModules(),
            'uri' => $this->getUri(),
            'sidebar' => $this->getSidebar(),
            'name' => $this->getName(),
        ];
    }
}
