<?php

namespace AlexKudrya\Adminix;

trait AdminixUser
{
    public function getAdminCriteria(): array
    {
        return [
            'is_admin' => true
        ];
    }

    public function admin(): bool
    {
        foreach ($this->getAdminCriteria() as $field => $value) {
            if ($this->$field !== $value) return false;
        }

        return true;
    }
}
