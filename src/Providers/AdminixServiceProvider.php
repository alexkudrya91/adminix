<?php

namespace AlexKudrya\Adminix\Providers;

use AlexKudrya\Adminix\Console\PageMakeCommand;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AdminixServiceProvider extends ServiceProvider
{
    const LOGIN_ROUTE = 'adminix_login';
    const PAGE_ROUTE = 'adminix_page';

    private array $commands = [
        PageMakeCommand::class
    ];

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/adminix.php' => config_path('adminix.php')
            ], 'config');

            $this->publishes([
                __DIR__ . '/../Pages' => base_path('app/Adminix/Pages')
            ], 'examples');

            $this->publishes([
                __DIR__.'/../resources/views/pages' => $this->app->resourcePath('views/vendor/adminix'),
                __DIR__.'/../resources/views/components' => $this->app->resourcePath('views/components/vendor/adminix'),
                __DIR__.'/../resources/css' => $this->app->publicPath('css/adminix'),
                __DIR__.'/../resources/js' => $this->app->publicPath('js/adminix'),
            ], 'laravel-assets');

            $this->commands($this->commands);

            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }

        Route::middleware('web')->prefix(config('adminix.route_prefix'))->group(function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/auth.php');
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });
    }
}
