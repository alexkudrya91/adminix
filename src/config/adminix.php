<?php

use AlexKudrya\Adminix\Modules\Link\MenuLinkModule;

return [
    'route_prefix' => env("ADMINIX_PREFIX", 'adminix'),

    'admin_user_model' => \App\Models\User::class,

    'pages' => [
        \App\Adminix\Pages\IndexPage::get(),
        \App\Adminix\Pages\UsersPage::get(),
        \App\Adminix\Pages\UserPage::get(),
        \App\Adminix\Pages\NewUserPage::get(),
    ],

    'menu' => [
        'title' => 'AdminiX',
        'links' => [
            MenuLinkModule::title('Dashboard')->uri(\App\Adminix\Pages\IndexPage::URI)->icon('bi bi-speedometer2'),
            MenuLinkModule::title('Users')->uri(\App\Adminix\Pages\UsersPage::URI)->icon('bi bi-people-fill'),
        ]
    ],

    'app_title' => null,

    'no_auth_access' => false
];
