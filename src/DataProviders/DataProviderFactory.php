<?php

namespace AlexKudrya\Adminix\DataProviders;

use AlexKudrya\Adminix\DataProviders\Implementations\ChartLineModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\ChartBarModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\ClockModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\CounterModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\LinkModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\ListModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\NewResourceModuleDataProvider;
use AlexKudrya\Adminix\DataProviders\Implementations\ResourceModuleDataProvider;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\ChartBar\ChartBarModule;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule;
use AlexKudrya\Adminix\Modules\Clock\ClockModule;
use AlexKudrya\Adminix\Modules\Counter\CounterModule;
use AlexKudrya\Adminix\Modules\Link\LinkModule;
use AlexKudrya\Adminix\Modules\List\ListModule;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;
use AlexKudrya\Adminix\Modules\Resource\ResourceModule;
use Exception;

class DataProviderFactory
{
    /**
     * @param AdminixModuleInterface $module
     * @return ModuleDataProviderInterface
     * @throws Exception
     */
    public function getDataProvider(AdminixModuleInterface $module): ModuleDataProviderInterface
    {
        return match (true) {
            ($module instanceof ListModule) => new ListModuleDataProvider(),
            ($module instanceof ChartLineModule) => new ChartLineModuleDataProvider(),
            ($module instanceof ChartBarModule) => new ChartBarModuleDataProvider(),
            ($module instanceof CounterModule) => new CounterModuleDataProvider(),
            ($module instanceof NewResourceModule) => new NewResourceModuleDataProvider(),
            ($module instanceof ResourceModule) => new ResourceModuleDataProvider(),
            ($module instanceof LinkModule) => new LinkModuleDataProvider(),
            ($module instanceof ClockModule) => new ClockModuleDataProvider(),
            default => throw new Exception('Invalid adminix module: "' . $module::class . '"'),
        };
    }
}
