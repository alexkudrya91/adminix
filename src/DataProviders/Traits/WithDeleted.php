<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;

trait WithDeleted
{
    /**
     * @param BuilderContract $builder
     * @param AdminixModuleInterface $module
     * @return BuilderContract
     * @throws Exception
     */
    protected function withDeleted(BuilderContract $builder, AdminixModuleInterface $module): BuilderContract
    {
        if (!method_exists($module, 'isWithDeleted')) {
            throw new Exception('Module: "'.$module::class.'" does not have with_delete property');
        }

        if ($module->isWithDeleted()) {

            if (!$builder->hasMacro('withTrashed')) {
                throw new Exception('Module: "'.$module::class.'" model has not SoftDelete trait');
            }

            $builder->withTrashed();
        }

        return $builder;
    }
}
