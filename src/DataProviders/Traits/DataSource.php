<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\List\ListModule;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Support\Facades\DB;

trait DataSource
{
    /**
     * @param AdminixModuleInterface $module
     * @return BuilderContract
     * @throws Exception
     */
    protected function builderFromDataSource(AdminixModuleInterface $module): BuilderContract
    {
        if (!method_exists($module, 'getDataSource')) {
            throw new Exception('Module: "'.$module::class.'" does not have data source');
        }

        if (!$data_source = $module->getDataSource()) {
            throw new Exception('Absent data source in module: "'.$module::class.'"');
        }

        if (class_exists($data_source)) {
            $builder = $data_source::query();
            if ($module instanceof ListModule && is_array($relations = $module->getRelations())) {
                $builder->with($relations);
            }
        } else {
            $builder = DB::table($data_source);
        }

        return $builder;
    }
}
