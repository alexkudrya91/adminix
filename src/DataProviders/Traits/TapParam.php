<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use Illuminate\Support\Arr;

trait TapParam
{
    /**
     * @param string|int|float $value
     * @param array $params
     * @return string
     */
    protected function tapParam(string|int|float $value, array $params): string
    {
        if (is_string($value) && str_contains($value, ':')) {
            $value_parts = explode(':', $value);
            if ($value_parts[0] === 'param') {
                $param_number = $value_parts[1];
                $value = $params[$param_number] ?? $value;
            }
            if ($value_parts[0] === 'record') {
                $param_key = $value_parts[1];
                $value = Arr::get($params, $param_key) ?? $value;
            }
        }

        return $value;
    }
}
