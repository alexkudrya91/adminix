<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Property;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;

trait Props
{
    use TapParam;

    /**
     * @param BuilderContract $builder
     * @param AdminixModuleInterface $config
     * @param array $params
     * @return array
     * @throws Exception
     */
    protected function includeProps(BuilderContract $builder, AdminixModuleInterface $config, array $params): array
    {
        if (!$config->props() ?? []) {
            throw new Exception('Absent props in module: "'.$config->name().'"');
        }

        /** @var Property $prop */
        foreach ($config->props() as $prop) {
            $value = $prop->value();
            $prop->value($this->tapParam($value, $params));
            $builder->where($prop->key(), $prop->value());
        }

        return [$builder, $config];
    }
}
