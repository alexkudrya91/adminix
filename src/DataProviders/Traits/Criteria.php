<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Support\Arr;

trait Criteria
{
    use TapParam;

    /**
     * @param BuilderContract $builder
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return BuilderContract
     */
    protected function includeCriteria(
        BuilderContract $builder,
        AdminixModuleInterface $module,
        array $params = []
    ): BuilderContract
    {
        if (method_exists($module, 'getCriteria')
            && !empty($criteria = $module->getCriteria())) {

            if (Arr::isAssoc($criteria)) {
                foreach ($criteria as $field => $value) {
                    $builder = $this->addToBuilder($builder, $field, $value, '=', $params);
                }
            } else {
                foreach ($criteria as $criterion) {
                    $field = $criterion[0];
                    $operator = '=';
                    $value = $criterion[count($criterion) - 1];

                    if (count($criterion) === 3) {
                        $operator = $criterion[1];
                    }

                    $builder = $this->addToBuilder($builder, $field, $value, $operator, $params);
                }
            }
        }

        return $builder;
    }

    /**
     * @param BuilderContract $builder
     * @param string $field
     * @param mixed $value
     * @param string $operator
     * @param array $params
     * @return BuilderContract
     */
    private function addToBuilder(
        BuilderContract $builder,
        string $field,
        mixed $value,
        string $operator,
        array $params): BuilderContract
    {
        if (is_array($value)) {
            if ($operator === '=') {
                $builder->whereIn($field, $value);
            } elseif ($operator === '!=') {
                $builder->whereNotIn($field, $value);
            }
        } elseif (is_null($value)) {
            if ($operator === '=') {
                $builder->whereNull($field);
            } elseif ($operator === '!=') {
                $builder->whereNotNull($field);
            }
        } else {
            $value = $this->tapParam($value, $params);

            $builder->where($field, $operator, $value);
        }

        return $builder;
    }
}
