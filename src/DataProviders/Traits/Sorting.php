<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\List\ListModule;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

trait Sorting
{
    /**
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function includeSorting(BuilderContract $builder, ListModule $module): array
    {
        $has_sorting = false;

        foreach ($module->fields() as $field) {
            if (!$field->isSortable()) {
                continue;
            }

            $sorting_key = 'sort-' . $module->name() . '-' . $field->field();

            if (request()->has($sorting_key)) {
                $field->setSorting(request()->get($sorting_key));

                $builder->orderBy($field->field(), request()->get($sorting_key));

                $has_sorting = true;
            }
        }

        $sorting = $module->sorting();

        if (!$has_sorting && $sorting instanceof \AlexKudrya\Adminix\Modules\Sorting) {
            /** @var \AlexKudrya\Adminix\Modules\Sorting $sorting */
            $sorting = $module->sorting();

            $sort_field = null;
            $sort_direction = 'asc';

            if ($sorting instanceof \AlexKudrya\Adminix\Modules\Sorting) {
                $sort_field = $sorting->field();
                $sort_direction = $sorting->direction();
            }

            if (is_string($sort_field) && is_string($sort_direction)) {
                $builder->orderBy($sort_field, $sort_direction);
            }
        }

        return [$builder, $module];
    }
}
