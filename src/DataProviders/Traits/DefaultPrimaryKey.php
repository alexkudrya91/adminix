<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\List\ListModule;

trait DefaultPrimaryKey
{
    /**
     * @param ListModule $module
     * @return ListModule
     */
    protected function defaultPrimaryKey(ListModule $module): ListModule
    {
        if (!$module->primaryKey()) {
            $module->primaryKey('id');
        }

        return $module;
    }
}
