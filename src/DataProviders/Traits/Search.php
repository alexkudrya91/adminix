<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\List\ListModule;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

trait Search
{
    /**
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function includeSearch(BuilderContract $builder, ListModule $module): array
    {
        if ($module->search()) {
            if (request()->has('search-' . $module->name())) {
                $searchQuery = request()->get('search-' . $module->name());
                $module->search()->query($searchQuery);

                $builder->where(function ($query) use ($searchQuery, $module) {
                    foreach ($module->search()->searchFields() as $key => $search_field) {
                        if ($key === 0) {
                            $query->where($search_field, "ILIKE", '%'.$searchQuery.'%');
                        } else {
                            $query->orWhere($search_field, "ILIKE", '%'.$searchQuery.'%');
                        }
                    }
                });
            }
        }

        return [$builder, $module];
    }
}
