<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\List\ListDateFilter;
use AlexKudrya\Adminix\Modules\List\ListDateRangeFilter;
use AlexKudrya\Adminix\Modules\List\ListFilter;
use AlexKudrya\Adminix\Modules\List\ListFilterRecord;
use AlexKudrya\Adminix\Modules\List\ListModule;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Support\Facades\DB;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

trait Filters
{
    /**
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    protected function includeFilters(BuilderContract $builder, ListModule $module): array
    {
        foreach ($module->filters() ?? [] as &$filter) {
            if ($filter instanceof ListFilter) {
                [$filter, $builder, $module] = $this->includeFilter($filter, $builder, $module);
            }
            elseif ($filter instanceof ListDateFilter) {
                [$filter, $builder, $module] = $this->includeDateFilter($filter, $builder, $module);
            }
            elseif ($filter instanceof ListDateRangeFilter) {
                [$filter, $builder, $module] = $this->includeDateRangeFilter($filter, $builder, $module);
            }
        }

        return [$builder, $module];
    }

    /**
     * @param ListFilter $filter
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    private function includeFilter(ListFilter $filter, BuilderContract $builder, ListModule $module): array
    {
        if (!$filter->name()) {
            throw new Exception("Absent name in filter");
        }

        if (!$filter->src() && !$filter->filterRecords()) {
            throw new Exception("Absent src and records in {$filter->name()} filter");
        }

        if (!$filter->field()) {
            throw new Exception("Absent field in {$filter->name()} filter");
        }

        if ($src = $filter->src()) {
            if (!$data_source = $src->dataSource()) {
                throw new Exception("Absent data_source in {$filter->name()} filter src");
            }

            if (!$src->nameField()) {
                throw new Exception("Absent name_field in {$filter->name()} filter src");
            }

            if (!$src->valueField()) {
                throw new Exception("Absent value_field in {$filter->name()} filter src");
            }

            if (class_exists($data_source)) {
                $filter_builder = $data_source::query();
            } else {
                $filter_builder = DB::table($data_source);
            }

            if ($criteria = $src->criteria()) {
                $filter_builder->where($criteria);
            }

            $filter_data = [];

            $name_field = $src->nameField();
            $value_field = $src->valueField();

            foreach (($filter_builder->get() ?? []) as $filter_record) {
                $filter_data[] = (new ListFilterRecord())
                    ->name($filter_record->$name_field)
                    ->value($filter_record->$value_field);
            }

            $filter->filterRecords($filter_data);
        }

        /** @var ListFilterRecord $filterRecord */
        foreach ($filter->filterRecords() as &$filterRecord) {
            if ($filterRecord->value() === TRUE) {
                $filterRecord->value('1');
            } elseif ($filterRecord->value() === FALSE) {
                $filterRecord->value('0');
            }
        }

        $filter_parameter_key = 'filter-' . $module->name() . '-' . $filter->field();

        if (request()->has($filter_parameter_key)) {
            $filter_parameter = request()->get($filter_parameter_key);
            $filter->setValue($filter_parameter);

            if (!$filter->isPassive()) {
                $builder->where($filter->field(), $filter_parameter);
            }
        } else {
            $filter->setValue(null);
        }

        return [$filter, $builder, $module];
    }

    /**
     * @param ListDateFilter $filter
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    private function includeDateFilter(ListDateFilter $filter, BuilderContract $builder, ListModule $module): array
    {
        if (!$filter->name()) {
            throw new Exception("Absent name in date filter");
        }

        if (!$filter->field()) {
            throw new Exception("Absent field in {$filter->name()} date filter");
        }

        $filter_parameter_key = 'filter-' . $module->name() . '-' . $filter->field();

        if (request()->has($filter_parameter_key)) {
            $filter_value = request()->get($filter_parameter_key);

            $filter->setValue($filter_value);

            $filter_from = (new Carbon($filter_value))->startOfDay()->toDateTimeString();
            $filter_to = (new Carbon($filter_value))->endOfDay()->toDateTimeString();

            $builder->whereBetween($filter->field(), [$filter_from, $filter_to]);
        }

        return [$filter, $builder, $module];
    }

    /**
     * @param ListDateRangeFilter $filter
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    private function includeDateRangeFilter(ListDateRangeFilter $filter, BuilderContract $builder, ListModule $module): array
    {
        if (!$filter->name()) {
            throw new Exception("Absent name in date filter");
        }

        if (!$filter->field()) {
            throw new Exception("Absent field in {$filter->name()} date filter");
        }

        if (!$filter->filterTo() && !$filter->filterFrom()) {
            throw new Exception("Date filter can not be with disabled both \"From\" and \"To\" filters");
        }

        $filter_from_parameter_key = 'filter-' . $module->name() . '-' . $filter->field() . '-from';
        $filter_to_parameter_key = 'filter-' . $module->name() . '-' . $filter->field() . '-to';

        $filter_from_value = null;
        $filter_to_value = null;

        $filter_from = null;
        $filter_to = null;

        if ($filter->filterFrom() && request()->has($filter_from_parameter_key)) {
            $filter_from_value = request()->get($filter_from_parameter_key);
            $filter->setValueFrom($filter_from_value);
        }

        if ($filter->filterTo() && request()->has($filter_to_parameter_key)) {
            $filter_to_value = request()->get($filter_to_parameter_key);
            $filter->setValueTo($filter_to_value);
        }

        if ($filter_from_value) {
            $filter_from = (new Carbon($filter_from_value))->toDateTimeString();
            if (!$filter->isWithTime()) {
                $filter_from = (new Carbon($filter_from_value))->startOfDay()->toDateTimeString();
            }
        }

        if ($filter_to_value) {
            $filter_to = (new Carbon($filter_to_value))->toDateTimeString();
            if (!$filter->isWithTime()) {
                $filter_to = (new Carbon($filter_to_value))->endOfDay()->toDateTimeString();
            }
        }

        if ($filter_from && $filter_to) {
            $builder->whereBetween($filter->field(), [$filter_from, $filter_to]);
        } elseif ($filter_from && !$filter_to) {
            $builder->where($filter->field(), '>', $filter_from);
        } elseif (!$filter_from && $filter_to) {
            $builder->where($filter->field(), '<', $filter_to);
        }

        return [$filter, $builder, $module];
    }
}
