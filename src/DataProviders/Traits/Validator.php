<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\ChartBar\ChartBarItem;
use Exception;

trait Validator
{
    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateName(AdminixModuleInterface $module): void
    {
        if (!method_exists($module, 'getName') && !$module->getName()) {
            throw new Exception('Absent name in module ' . get_class($module));
        }
    }

    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateUri(AdminixModuleInterface $module): void
    {
        if (!method_exists($module, 'getUri') && !$module->getUri()) {
            throw new Exception('Absent uri in module ' . get_class($module));
        }
    }

    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateDataSource(AdminixModuleInterface $module): void
    {
        if (!method_exists($module, 'getDataSource') && !$module->getUri()) {
            throw new Exception('Absent data_source in module ' . get_class($module));
        }
    }

    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateColor(AdminixModuleInterface $module): void
    {
        if (!method_exists($module, 'getColor') && !$module->getColor()) {
            throw new Exception('Absent color in module ' . get_class($module));
        }
    }

    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateValueType(AdminixModuleInterface $module): void
    {
        if (!method_exists($module, 'getValueType') && !$module->getValueType()) {
            throw new Exception('Absent value_type in module ' . get_class($module));
        }
    }

    /**
     * @param AdminixModuleInterface $module
     * @return void
     * @throws Exception
     */
    protected function validateBars(AdminixModuleInterface $module): void
    {
        $bars = $module->bars() ?? [];

        if (!count($bars)) {
            throw new Exception('Absent bars in module ' . get_class($module));
        }

        /** @var ChartBarItem $bar */
        foreach ($bars as $bar) {
            if (!method_exists($bar, 'getLabel') && !$bar->getLabel()) {
                throw new Exception('Absent label in '. get_class($bar) .' in module ' . get_class($module));
            }
            if (!method_exists($bar, 'getColor') && !$bar->getColor()) {
                throw new Exception('Absent color in '. get_class($bar) .' in module ' . get_class($module));
            }
        }
    }
}
