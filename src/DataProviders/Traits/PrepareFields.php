<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;

trait PrepareFields
{
    /**
     * @param AdminixModuleInterface $module
     * @return AdminixModuleInterface
     */
    protected function prepareFields(AdminixModuleInterface $module): AdminixModuleInterface
    {
        if (method_exists($module, 'setFields') && method_exists($module, 'getFields')) {
            $module->setFields(collect($module->getFields())->keyBy('field')->toArray());
        }

        return $module;
    }
}
