<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\List\ListModule;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

trait GetDbData
{
    /**
     * @param BuilderContract $builder
     * @param ListModule $module
     * @return Collection|LengthAwarePaginator
     */
    protected function getDataFromDb(BuilderContract $builder, ListModule $module): Collection|LengthAwarePaginator
    {
        if ($module->pagination() ?? 0) {
            return $builder->paginate($module->pagination())
                ->appends(
                    request()->query()
                );
        } else {
            return $builder->get();
        }
    }
}
