<?php

namespace AlexKudrya\Adminix\DataProviders\Traits;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\List\ListModule;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

trait Resource
{
    /**
     * @param Collection|LengthAwarePaginator $queryResult
     * @param ListModule $module
     * @return mixed
     */
    protected function includeResourceCollection(Collection|LengthAwarePaginator $queryResult, ListModule $module): mixed
    {
        $resource = $module->resource();

        if ($resource
            && class_exists($resource)
            && in_array(JsonResource::class, class_parents($resource))) {
            return $resource::collection($queryResult);
        } else {
            return $queryResult;
        }
    }

    /**
     * @param mixed $record
     * @param AdminixModuleInterface $config
     * @return array
     */
    protected function includeResource(mixed $record, AdminixModuleInterface $config): array
    {
        if ($config->resource() ?? null
            && class_exists($config->resource())) {
            $resource_class = $config->resource();
            $resource = new $resource_class($record);
            return $resource->resolve();
        } else {
            return $record->toArray();
        }
    }
}
