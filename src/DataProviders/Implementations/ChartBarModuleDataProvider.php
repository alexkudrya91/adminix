<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Criteria;
use AlexKudrya\Adminix\DataProviders\Traits\DataSource;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\ChartBar\ChartBarModule;
use Exception;
use Illuminate\Support\Str;

class ChartBarModuleDataProvider implements ModuleDataProviderInterface
{
    use Criteria, DataSource, Validator;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return ChartBarModule
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): ChartBarModule
    {
        /** @var ChartBarModule $module */
        if (!$module instanceof ChartBarModule) {
            throw new Exception('DataProvider "'.__CLASS__.'" can process only ChartBarModule module');
        }

        $this->validateName($module);
        $this->validateBars($module);
        $this->validateDataSource($module);

        $module->labels($this->getLabels($module));
        $module->colors($this->getColors($module));
        $module->values($this->getValues($module));

        $module->id(Str::random(8));

        return $module;
    }

    /**
     * @param ChartBarModule $module
     * @return array
     */
    private function getLabels(ChartBarModule $module): array
    {
        $out = [];

        foreach ($module->bars() as $bar) {
            $out[] = $bar->label();
        }

        return $out;
    }

    /**
     * @param ChartBarModule $module
     * @return array
     */
    private function getColors(ChartBarModule $module): array
    {
        $out = [];

        foreach ($module->bars() as $bar) {
            $out[] = $bar->color();
        }

        return $out;
    }

    /**
     * @param ChartBarModule $module
     * @return array
     * @throws Exception
     */
    private function getValues(ChartBarModule $module): array
    {
        $out = [];

        foreach ($module->bars() as $bar) {
            $builder = $this->builderFromDataSource($module);
            $builder = $this->includeCriteria($builder, $module);
            $builder = $this->includeCriteria($builder, $bar);
            $out[] = $builder->count();
        }

        return $out;
    }
}
