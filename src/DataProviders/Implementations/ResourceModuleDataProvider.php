<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Criteria;
use AlexKudrya\Adminix\DataProviders\Traits\DataSource;
use AlexKudrya\Adminix\DataProviders\Traits\PrepareFields;
use AlexKudrya\Adminix\DataProviders\Traits\Props;
use AlexKudrya\Adminix\DataProviders\Traits\Resource;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;
use AlexKudrya\Adminix\Modules\Resource\ResourceModule;
use AlexKudrya\Adminix\Modules\SelectRecord;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ResourceModuleDataProvider implements ModuleDataProviderInterface
{
    use Props, DataSource, Validator, Criteria, Resource, PrepareFields;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return AdminixModuleInterface
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): AdminixModuleInterface
    {
        $this->validateName($module);
        $this->validateDataSource($module);

        $builder = $this->builderFromDataSource($module);

        [$builder, $module] = $this->includeProps($builder, $module, $params);

        $builder = $this->includeCriteria($builder, $module, $params);

        if ($builder->hasMacro('withTrashed')) {
            $builder->withTrashed();
        }

        $record = $builder->first();

        if (is_null($record)) {
            throw new Exception('Record not found', 404);
        }

        $data = $this->includeResource($record, $module);

        $module = $this->prepareData($data, $module);

        return $module;
    }

    /**
     * @param array $data
     * @param ResourceModule $module
     * @return AdminixModuleInterface
     */
    private function prepareData(array $data, ResourceModule $module): AdminixModuleInterface
    {
        $module_data = [];

        /** @var ResourceField $field */
        foreach ($module->fields() as $field) {
            $value = Arr::get($data, $field->field());
            $module_data[$field->field()] = $value;

            if ($field->type() === ResourceInputTypeEnum::SELECT) {
                if ($field->src()) {
                    if (class_exists($field->src()->dataSource())) {
                        $select_builder = $field->src()->dataSource()::query();
                    } else {
                        $select_builder = DB::table($field->src()->dataSource());
                    }

                    if ($field->src()->criteria()
                        && is_array($field->src()->criteria())
                        && !empty($field->src()->criteria())) {
                        $select_builder->where($field->src()->criteria());
                    }

                    $name_field = $field->src()->nameField();
                    $value_field = $field->src()->valueField();

                    foreach (($select_builder->get() ?? []) as $select_record) {
                        $field->addSelectRecord(
                            SelectRecord::name($select_record->$name_field)->value($select_record->$value_field)
                        );
                    }
                }
            }
        }

        $module->data($module_data);

        return $module;
    }
}
