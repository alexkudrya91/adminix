<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\PrepareFields;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;
use AlexKudrya\Adminix\Modules\SelectRecord;
use Exception;
use Illuminate\Support\Facades\DB;

class NewResourceModuleDataProvider implements ModuleDataProviderInterface
{
    use Validator, PrepareFields;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return AdminixModuleInterface
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): AdminixModuleInterface
    {
        $this->validateName($module);

        $module = $this->prepareData($module, $params);

        return $module;
    }

    /**
     * @param NewResourceModule $module
     * @param $params
     * @return NewResourceModule
     */
    private function prepareData(AdminixModuleInterface $module, $params): NewResourceModule
    {
        /** @var ResourceField $field */
        foreach ($module->fields() as $field) {
            if ($field->type() === ResourceInputTypeEnum::SELECT) {
                if (!is_null($field->src())) {
                    if (class_exists($field->src()->dataSource())) {
                        $select_builder = $field->src()->dataSource()::query();
                    } else {
                        $select_builder = DB::table($field->src()->dataSource());
                    }

                    if ($field->src()->criteria()) {
                        $select_builder->where($field->src()->criteria());
                    }

                    $name_field = $field->src()->nameField();
                    $value_field = $field->src()->valueField();

                    foreach (($select_builder->get() ?? []) as $select_record) {
                        $field->addSelectRecord(
                            SelectRecord::name($select_record->$name_field)->value($select_record->$value_field)
                        );
                    }
                }
            }

            if ($field->type() === ResourceInputTypeEnum::HIDDEN) {
                if (str_contains($field->value(), 'param:')) {
                    $parts = explode(':', $field->value());
                    if ($parts[0] === 'param' && array_key_exists($parts[1], $params)) {
                        $field->value($params[$parts[1]]);
                    }
                }
            }

        }

        return $module;
    }
}
