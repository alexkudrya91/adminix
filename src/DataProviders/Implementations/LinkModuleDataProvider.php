<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\TapParam;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Link\LinkModule;
use Exception;

class LinkModuleDataProvider implements ModuleDataProviderInterface
{
    use Validator, TapParam;

    /**
     * @param LinkModule $module
     * @param array $params
     * @return LinkModule
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): AdminixModuleInterface
    {
        $this->validateName($module);
        $this->validateUri($module);

        $module_params = $module->params() ?? [];

        foreach ($module_params as &$param) {
            $param = $this->tapParam($param, $params);
        }

        $module->params($module_params);

        return $module;
    }
}
