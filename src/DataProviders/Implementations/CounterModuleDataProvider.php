<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Criteria;
use AlexKudrya\Adminix\DataProviders\Traits\DataSource;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Counter\CounterModule;
use Exception;

class CounterModuleDataProvider implements ModuleDataProviderInterface
{
    use Criteria, DataSource, Validator;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return CounterModule
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): CounterModule
    {
        /** @var CounterModule $module */
        if (!$module instanceof CounterModule) {
            throw new Exception('DataProvider "'.__CLASS__.'" can process only CounterModule module');
        }

        $this->validateName($module);
        $this->validateDataSource($module);

        $builder = $this->builderFromDataSource($module);

        $builder = $this->includeCriteria($builder, $module, $params);

        $module->result($builder->count());

        return $module;
    }
}
