<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Criteria;
use AlexKudrya\Adminix\DataProviders\Traits\DataSource;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineValueTypeEnum;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Database\Query\Builder as BuilderContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PDO;

class ChartLineModuleDataProvider implements ModuleDataProviderInterface
{
    use Criteria, DataSource, Validator;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return ChartLineModule
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): ChartLineModule
    {
        /** @var ChartLineModule $module */
        if (!$module instanceof ChartLineModule) {
            throw new Exception('DataProvider "'.__CLASS__.'" can process only ChartLineModule module');
        }

        $this->validateName($module);
        $this->validateDataSource($module);
        $this->validateValueType($module);
        $this->validateColor($module);

        $builder = $this->builderFromDataSource($module);

        $builder = $this->includeCriteria($builder, $module, $params);

        if ($module->valueType() === ChartLineValueTypeEnum::COUNT_DAILY) {
            $module = $this->countDaily($builder, $module);
        }

        if ($module->valueType() === ChartLineValueTypeEnum::COUNT_MONTHLY) {
            $module = $this->countMonthly($builder, $module);
        }

        $module->setId(Str::random(8));

        return $module;
    }

    /**
     * @param BuilderContract $builder
     * @param ChartLineModule $module
     * @return ChartLineModule
     */
    private function countDaily(BuilderContract $builder, ChartLineModule $module): ChartLineModule
    {
        $base_field = $module->baseField() ?? 'created_at';

        $data = $builder->where($base_field, '>=', \Carbon\Carbon::now()->subDays($module->limit())->startOfDay())
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get([
                DB::raw('Date('.$base_field.') as date'),
                DB::raw('COUNT(*) as "items"')
            ])->keyBy('date')->toArray();

        $labels = [];
        $values = [];

        for ($i=0; $i<=$module->limit(); $i++) {
            $carbon = Carbon::now()->subDays($module->limit())->addDays($i);

            $labels[] = mb_strcut($carbon->toDateString(), 8);
            $values[] = $data[$carbon->toDateString()]['items'] ?? 0;
        }

        $module->data($data)->labels($labels)->values($values);

        return $module;
    }

    /**
     * @param BuilderContract $builder
     * @param ChartLineModule $module
     * @return ChartLineModule
     */
    private function countMonthly(BuilderContract $builder, ChartLineModule $module): ChartLineModule
    {
        $base_field = $module->baseField() ?? 'created_at';
        $db_driver = DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME);

        $get_arr = [];

        if ($db_driver === 'pgsql') {
            $get_arr[] = DB::raw("TO_CHAR(\"".$base_field."\", 'YYYY-MM') as \"date\"");
        } elseif ($db_driver === 'mysql') {
            $get_arr[] = DB::raw('DATE_FORMAT('.$base_field.', "%Y-%m") as "date"');
        }

        $get_arr[] = DB::raw('COUNT(*) as "items"');

        $data = $builder->where($base_field, '>=', \Carbon\Carbon::now()->subMonths($module->limit())->startOfMonth())
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get($get_arr)->keyBy('date')->toArray();

        $labels = [];
        $values = [];

        for ($i=0; $i<$module->limit(); $i++) {
            $carbon = Carbon::now()->subMonths($module->limit())->startOfMonth()->addMonths($i);

            $labels[] = mb_strcut($carbon->toDateString(), 5, 2);
            $values[] = $data[mb_strcut($carbon->toDateString(), 0, 7)]['items'] ?? 0;
        }

        $module->data($data)->labels($labels)->values($values);

        return $module;
    }
}
