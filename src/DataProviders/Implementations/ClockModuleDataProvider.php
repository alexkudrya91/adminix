<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\Clock\ClockModule;
use Exception;
use Illuminate\Support\Str;

class ClockModuleDataProvider implements ModuleDataProviderInterface
{
    use Validator;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return ClockModule
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): ClockModule
    {
        /** @var ClockModule $module */
        if (!$module instanceof ClockModule) {
            throw new Exception('DataProvider "'.__CLASS__.'" can process only ClockModule module');
        }

        $this->validateName($module);

        $module->id(Str::random(8));

        return $module;
    }
}
