<?php

namespace AlexKudrya\Adminix\DataProviders\Implementations;

use AlexKudrya\Adminix\DataProviders\Traits\Criteria;
use AlexKudrya\Adminix\DataProviders\Traits\DataSource;
use AlexKudrya\Adminix\DataProviders\Traits\Filters;
use AlexKudrya\Adminix\DataProviders\Traits\GetDbData;
use AlexKudrya\Adminix\DataProviders\Traits\PrepareFields;
use AlexKudrya\Adminix\DataProviders\Traits\Resource;
use AlexKudrya\Adminix\DataProviders\Traits\Search;
use AlexKudrya\Adminix\DataProviders\Traits\Sorting;
use AlexKudrya\Adminix\DataProviders\Traits\TapParam;
use AlexKudrya\Adminix\DataProviders\Traits\Validator;
use AlexKudrya\Adminix\DataProviders\Traits\DefaultPrimaryKey;
use AlexKudrya\Adminix\DataProviders\Traits\WithDeleted;
use AlexKudrya\Adminix\DataProviders\ModuleDataProviderInterface;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\List\ListFieldTypeEnum;
use AlexKudrya\Adminix\Modules\List\ListModule;
use AlexKudrya\Adminix\Modules\SelectRecord;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ListModuleDataProvider implements ModuleDataProviderInterface
{
    use Search,
        Filters,
        Sorting,
        GetDbData,
        Criteria,
        DataSource,
        WithDeleted,
        Resource,
        PrepareFields,
        Validator,
        DefaultPrimaryKey, TapParam;

    /**
     * @param AdminixModuleInterface $module
     * @param array $params
     * @return ListModule
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    public function handle(AdminixModuleInterface $module, array $params = []): ListModule
    {
        /** @var ListModule $module */
        if (!$module instanceof ListModule) {
            throw new Exception('DataProvider "'.__CLASS__.'" can process only ListModule module');
        }

        $this->validateName($module);
        $this->validateDataSource($module);

        $builder = $this->builderFromDataSource($module);
        $builder = $this->withDeleted($builder, $module);
        $builder = $this->includeCriteria($builder, $module, $params);

        list ($builder, $module) = $this->includeFilters($builder, $module);
        list ($builder, $module) = $this->includeSearch($builder, $module);
        list ($builder, $module) = $this->includeSorting($builder, $module);

        $queryResult = $this->getDataFromDb($builder, $module);
        $data = $this->includeResourceCollection($queryResult, $module);

        $module = $this->prepareData($queryResult, $data, $module, $params);

        /** @var ListModule $module */
        $module = $this->defaultPrimaryKey($module);
        return $module;
    }

    /**
     * @param Collection|LengthAwarePaginator $queryResult
     * @param $input_data
     * @param ListModule $module
     * @param array $module_params
     * @return ListModule
     */
    private function prepareData(
        Collection|LengthAwarePaginator $queryResult,
                                        $input_data,
        ListModule $module,
        array $module_params = []
    ): ListModule
    {
        if ($module->pagination() ?? 0) {
            $output_data = $queryResult->toArray();
            $output_data['data'] = [];
        } else {
            $output_data = [];
        }

        foreach ($input_data as $record) {
            $item = [];

            if ($module->getResource()) {
                $record = $record->resolve();
            } else {
                $record = $record->toArray();
            }

            foreach ($module->fields() as $field) {
                $field_name = $field->field();
                $value = Arr::get($record, $field_name);
                $item[$field_name] = $value;
            }

            if ($module->actions()) {
                foreach ($module->actions() as $action) {
                    if (!empty($action->criteria() ?? [])) {

                        foreach($action->criteria() as $criteria) {
                            [$criteria_field, $operator, $criteria_value] = $criteria;

                            if (!is_null($criteria_value)) {
                                $result = match($operator) {
                                    '==' => ($item[$criteria_field] == $criteria_value),
                                    '!=' => ($item[$criteria_field] != $criteria_value),
                                };
                            } else {
                                $result = match($operator) {
                                    '==' => is_null($item[$criteria_field]),
                                    '!=' => !is_null($item[$criteria_field]),
                                };
                            }

                            if (!$result) continue 2;
                        }
                    }

                    $item_action = clone($action);
                    $params = [];
                    foreach ($action->params() as $param) {
                        $param = $this->tapParam($param, $record);
                        $param = $this->tapParam($param, $module_params);
                        $params[] = $param;
                    }
                    $item_action->params($params);
                    $item['_actions'][] = $item_action;
                }
            }

            if ($module->pagination() ?? 0) {
                $output_data['data'][] = $item;
            } else {
                $output_data[] = $item;
            }
        }

        foreach ($module->fields() as $field) {
            if ($field->type() === ListFieldTypeEnum::SELECT) {
                if ($field->src()) {
                    $src_data_source = $field->src()->dataSource();
                    if (class_exists($src_data_source)) {
                        $select_builder = $src_data_source::query();
                    } else {
                        $select_builder = DB::table($src_data_source);
                    }

                    if ($criteria = $field->src()->criteria()) {
                        $select_builder->where($criteria);
                    }

                    $name_field = $field->src()->nameField();
                    $value_field = $field->src()->valueField();

                    foreach (($select_builder->get() ?? []) as $select_record) {

                        $field->addSelectRecord(
                            SelectRecord::name($select_record->$name_field)->value($select_record->$value_field)
                        );
                    }
                }
            }
        }

        return $module->data($output_data);
    }
}
