<?php

namespace AlexKudrya\Adminix\DataProviders;

use AlexKudrya\Adminix\Modules\AdminixModuleInterface;

interface ModuleDataProviderInterface
{
    public function handle(AdminixModuleInterface $module, array $params = []): AdminixModuleInterface;
}
