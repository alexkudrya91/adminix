<?php

namespace App\Adminix\Pages;


use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineValueTypeEnum;
use AlexKudrya\Adminix\Modules\Counter\CounterModule;
use App\Enums\GameStatusEnum;
use App\Models\Game;
use App\Models\User;
use Carbon\Carbon;

class IndexPage
{
    const URI = '/';

    public static function get(): AdminixPage
    {
        $page = new AdminixPage();

        $page->setAsHomePage();

        $page->uri(self::URI);

        $page->name('index');

        $page->addModule(
            ChartLineModule::title('USER REGISTRATIONS DAILY')
                ->name('user_registrations')
                ->color(ColorsEnum::DEEP_PINK)
                ->valueType(ChartLineValueTypeEnum::COUNT_DAILY)
                ->limit(30)
                ->dataSource(User::class)
        );

        $page->addModule(
            ChartLineModule::title('USER REGISTRATIONS MONTHLY')
            ->name('user_registrations_year')
            ->color(ColorsEnum::ORANGE)
            ->valueType(ChartLineValueTypeEnum::COUNT_MONTHLY)
            ->limit(6)
            ->dataSource(User::class)
        );

        $page->addModule(
            CounterModule::title('TOTAL USERS')
                ->name('total_users')
                ->icon('bi bi-people')
                ->dataSource(User::class)
                ->criteria([
                    ['is_bot', false],
                ])
        );

        $page->addModule(
            CounterModule::title('TODAY REGISTRATIONS')
                ->name('today_registrations')
                ->icon('bi bi-person-add')
                ->dataSource(User::class)
                ->criteria([
                    ['is_bot', false],
                    ['created_at', '>=', Carbon::today()->startOfDay()->toDateTimeString()]
                ])
        );

        return $page;
    }
}
