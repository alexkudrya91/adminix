<?php

namespace App\Adminix\Pages;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;
use AlexKudrya\Adminix\Modules\Resource\ResourceModule;
use AlexKudrya\Adminix\Modules\Resource\ResourceProperty;


class UserPage
{
    const URI = 'user';

    public static function get(): AdminixPage
    {
        $page = new AdminixPage();

        $page->uri(self::URI);

        $page->name('user');

        $page->addModule(
            ResourceModule::name('User')
                ->title('USER')
                ->addProp(ResourceProperty::key('id')->value('param:1'))
                ->dataSource(\App\Models\User::class)
                ->successRedirectUri(UsersPage::URI)
                ->addFields(
                    ResourceField::name('Name')
                        ->field('name')
                        ->type(ResourceInputTypeEnum::STRING)
                        ->required()
                        ->addValidationRules(
                            'string',
                            'required'
                        ),

                    ResourceField::name('Email')
                        ->field('email')
                        ->type(ResourceInputTypeEnum::EMAIL)
                        ->required()
                        ->addValidationRules(
                            'email',
                            'required',
                            "unique:".\App\Models\User::class.",email"
                        ),

                    ResourceField::name('Admin')
                        ->field('is_admin')
                        ->type(ResourceInputTypeEnum::BOOLEAN)
                        ->addValidationRule('boolean'),

                    ResourceField::name('Registered')
                        ->field('created_at')
                        ->type(ResourceInputTypeEnum::DATETIME)
                        ->readonly(),

                    ResourceField::name('Email verified')
                        ->field('email_verified_at')
                        ->type(ResourceInputTypeEnum::DATETIME)
                        ->readonly(),
                )
        );

        return $page;
    }
}
