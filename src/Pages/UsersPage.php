<?php

namespace App\Adminix\Pages;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Link\AdminixLinkModule;
use AlexKudrya\Adminix\Modules\List\ListField;
use AlexKudrya\Adminix\Modules\List\ListFieldTypeEnum;
use AlexKudrya\Adminix\Modules\List\ListFilter;
use AlexKudrya\Adminix\Modules\List\ListFilterRecord;
use AlexKudrya\Adminix\Modules\List\ListModule;
use AlexKudrya\Adminix\Modules\List\ListSearch;
use AlexKudrya\Adminix\Modules\Sorting;
use App\Models\User;

class UsersPage
{
    const URI = 'users';

    public static function get(): AdminixPage
    {
        $page = new AdminixPage();

        $page->uri(self::URI);

        $page->name('users');

        $page->addModule(
            ListModule::name('users')
                ->title('USERS')
                ->dataSource(User::class)
                ->pagination(20)
                ->withDeleted()
                ->editable()
                ->sorting(Sorting::field('id')->direction('asc'))
                ->search(ListSearch::searchFields(['name', 'email'])->placeholder('Search users'))
                ->addFields(
                    ListField::name('Name')
                        ->field('name')
                        ->type(ListFieldTypeEnum::STRING)
                        ->sortable(),

                    ListField::name('Email')
                        ->field('email')
                        ->type(ListFieldTypeEnum::EMAIL)
                        ->sortable(),

                    ListField::name('Registration')
                        ->field('created_at')
                        ->type(ListFieldTypeEnum::DATETIME)
                        ->sortable(),

                    ListField::name('Administrator')
                        ->field('is_admin')
                        ->type(ListFieldTypeEnum::BOOLEAN),

                    ListField::field('id')
                        ->type(ListFieldTypeEnum::HIDDEN),
                )->addFilter(
                    ListFilter::field('is_admin')->name('Admin')
                        ->addFilterRecords(
                            ListFilterRecord::name('True')->value(true),
                            ListFilterRecord::name('False')->value(false),
                        )
                )->addActions(
                    AdminixLinkModule::name('edit')
                        ->title('EDIT')
                        ->icon('bi bi-pencil')
                        ->uri(UserPage::URI)
                        ->params(['record:id']),
                )
                ->newItemLink(
                    AdminixLinkModule::name('new_user_btn')
                        ->title('NEW USER')
                        ->icon('bi bi-person-fill-add')
                        ->uri(NewUserPage::URI)
                )
        );

        return $page;
    }
}
