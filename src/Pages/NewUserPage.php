<?php

namespace App\Adminix\Pages;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;
use App\Http\Resources\UserAdminResource;
use App\Models\User;

class NewUserPage
{
    const URI = 'new_user';

    public static function get(): AdminixPage
    {
        $page = new AdminixPage();

        $page->uri(self::URI);

        $page->name('new_user');

        $page->addModule(
            NewResourceModule::name('User')
                ->title('NEW USER')
                ->dataSource(User::class)
                ->addFields(
                    ResourceField::name('Name')
                        ->field('name')
                        ->type(ResourceInputTypeEnum::STRING)
                        ->required()
                        ->addValidationRules(
                            'string',
                            'required'
                        ),

                    ResourceField::name('Email')
                        ->field('email')
                        ->type(ResourceInputTypeEnum::EMAIL)
                        ->required()
                        ->addValidationRules(
                            'email',
                            'required',
                            "unique:". User::class.",email"
                        ),

                    ResourceField::name('Password')
                        ->field('password')
                        ->type(ResourceInputTypeEnum::PASSWORD)
                        ->confirmed()
                        ->required()
                        ->addValidationRules(
                            'string',
                            'required',
                            'confirmed'
                        ),

                    ResourceField::name('Admin')
                        ->field('is_admin')
                        ->type(ResourceInputTypeEnum::BOOLEAN)
                        ->addValidationRule('boolean'),
                )
                ->successRedirectUri(UsersPage::URI)
        );

        return $page;
    }
}
