<?php

namespace AlexKudrya\Adminix\Dto;

use Illuminate\Validation\Validator;

class ValidationResultDto
{
    private bool $is_valid;

    private array $input_data;

    private array $rules;

    private ?array $messages;

    public function handle(Validator $validator)
    {
        $this->is_valid = !$validator->fails();

        if (!$this->isValid()) {
            $this->setMessages($validator->getMessageBag()->getMessages());
        }
    }

    public function getInputData(): array
    {
        return $this->input_data;
    }

    public function setInputData(array $input_data): void
    {
        $this->input_data = $input_data;
    }

    public function setRules(array $rules): void
    {
        $this->rules = $rules;
    }

    private function setMessages(array $messages): void
    {
        $this->messages = $messages;
    }

    public function isValid(): bool
    {
        return $this->is_valid;
    }

    public function getMessages(): ?array
    {
        return $this->messages;
    }
}
