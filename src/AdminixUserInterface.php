<?php

namespace AlexKudrya\Adminix;

interface AdminixUserInterface
{
    public function getAdminCriteria(): array;
}
