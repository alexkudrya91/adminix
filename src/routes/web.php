<?php

use Illuminate\Support\Facades\Route;

Route::post('/save', [\AlexKudrya\Adminix\Http\Controllers\PageController::class, 'saveResource'])
    ->name('adminix_save_resource')
    ->middleware(\AlexKudrya\Adminix\Http\Middleware\AuthMiddleware::class);

Route::post('/create', [\AlexKudrya\Adminix\Http\Controllers\PageController::class, 'createResource'])
    ->name('adminix_create_resource')
    ->middleware(\AlexKudrya\Adminix\Http\Middleware\AuthMiddleware::class);

Route::any('/{adm_param?}', [\AlexKudrya\Adminix\Http\Controllers\PageController::class, 'handleRequest'])
    ->name('adminix_page')
    ->where('adm_param', '.*')
    ->middleware(\AlexKudrya\Adminix\Http\Middleware\AuthMiddleware::class);
