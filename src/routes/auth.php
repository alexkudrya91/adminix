<?php

use AlexKudrya\Adminix\Http\Controllers\AuthController;
use AlexKudrya\Adminix\Providers\AdminixServiceProvider;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Adminix Auth Routes
|--------------------------------------------------------------------------
*/

Route::middleware(\AlexKudrya\Adminix\Http\Middleware\GuestMiddleware::class)
    ->group(function () {
        Route::get('login', [AuthController::class, 'loginForm'])
            ->name(AdminixServiceProvider::LOGIN_ROUTE);

        Route::post('login', [AuthController::class, 'login'])
            ->name('adminix_login_action')
            ->middleware(VerifyCsrfToken::class)
        ;
    });

Route::get('logout', [AuthController::class, 'logout'])
    ->name('adminix_login_logout')
    ->middleware(\AlexKudrya\Adminix\Http\Middleware\AuthMiddleware::class);
