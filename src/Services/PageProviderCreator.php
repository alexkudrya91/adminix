<?php

namespace AlexKudrya\Adminix\Services;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use InvalidArgumentException;

class PageProviderCreator
{
    protected $files;

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    public function create($name, $path): string
    {
        $this->ensurePageDoesntAlreadyExist($name, $path);

        $stub = $this->getStub();

        $path = $this->getPath($name, $path);

        $this->files->ensureDirectoryExists(dirname($path));

        $this->files->put(
            $path, $this->populateStub($stub, $name)
        );

        return $path;
    }

    protected function ensurePageDoesntAlreadyExist($name, $path = null): void
    {
        $className = $this->getClassName($name);

        if (! empty($path)) {
            $files = $this->files->glob($path.'/*.php');

            foreach ($files as $file) {
                if ($path.'/'.$className.'.php' === $file) {
                    throw new InvalidArgumentException("A {$className} class already exists.");
                }
            }
        }
    }

    protected function getStub(): string
    {
        return $this->files->get(__DIR__ . '/../stub/page_provider.stub');
    }

    protected function populateStub($stub, $name)
    {
        $class_name = $this->getClassName($name);
        $page_name = $this->getName($name);

        if (! is_null($name)) {
            $stub = str_replace(
                ['{{class_name}}'],
                $class_name, $stub
            );
            $stub = str_replace(
                ['{{page_name}}'],
                $page_name, $stub
            );
        }

        return $stub;
    }

    protected function getClassName($name): string
    {
        $class_name = Str::studly($name);

        if (! Str::endsWith($class_name, 'Page')) {
            $class_name .= 'Page';
        }

        return $class_name;
    }

    protected function getName($name): string
    {
        $page_name = Str::kebab($name);

        if (Str::endsWith($page_name, '-page')) {
            $page_name = substr($page_name, -0, -5);;
        }

        return $page_name;
    }

    protected function getPath($name, $path): string
    {
        return $path.'/'.$this->getClassName($name).'.php';
    }
}
