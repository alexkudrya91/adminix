<?php

namespace AlexKudrya\Adminix\Services;

use Illuminate\Support\Facades\DB;

class ResourceRepository
{
    /**
     * @param string $data_source
     * @param array $input_data
     * @param string $primary_key
     * @param mixed $primary_value
     * @return int
     */
    public function updateResource(
        string $data_source,
        array $input_data,
        string $primary_key,
        mixed $primary_value): int
    {
        if (class_exists($data_source)) {
            $builder = $data_source::query();
        } else {
            $builder = DB::table($data_source);
        }

        return $builder->where($primary_key, $primary_value)
            ->update($input_data);
    }

    /**
     * @param string $data_source
     * @param array $input_data
     * @return mixed
     */
    public function createResource(
        string $data_source,
        array $input_data
    ): mixed
    {
        if (class_exists($data_source)) {
            $builder = $data_source::query();
        } else {
            $builder = DB::table($data_source);
        }

        return $builder->create($input_data);
    }
}