<?php

namespace AlexKudrya\Adminix\Services;

use AlexKudrya\Adminix\Dto\ValidationResultDto;

class Validator
{
    /**
     * @param array $validation
     * @param array $input_data
     * @param mixed|null $primary_value
     * @param string $primary_key
     * @param bool $ignoreUnique
     * @return ValidationResultDto
     */
    public function validation(
        array $validation,
        array $input_data,
        mixed $primary_value = null,
        string $primary_key = 'id',
        bool $ignoreUnique = false): ValidationResultDto
    {
        if ($ignoreUnique) {
            $validation = $this->validationUniqueIgnoreCurrent(
                validation: $validation,
                primary_value: $primary_value,
                primary_key: $primary_key
            );
        }

        $validator = \Illuminate\Support\Facades\Validator::make($input_data, $validation);

        $dto = new ValidationResultDto();
        $dto->handle($validator);

        if (!$dto->isValid()) {
            $dto->setInputData($input_data);
            $dto->setRules($validation);
        }

        return $dto;
    }

    /**
     * @param array $validation
     * @param mixed $primary_value
     * @param string $primary_key
     * @return array
     */
    private function validationUniqueIgnoreCurrent(
        array $validation,
        mixed $primary_value,
        string $primary_key = 'id'
    ): array
    {
        foreach ($validation as &$fields) {
            foreach ($fields as &$rule) {
                if (strripos($rule, 'unique') !== false) {
                    $rule .= ',' . $primary_value . ',' . $primary_key;
                }
            }
        }

        return $validation;
    }
}
