<?php

namespace AlexKudrya\Adminix\Services;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\DataProviders\DataProviderFactory;
use AlexKudrya\Adminix\Modules\Link\LinkInterface;
use AlexKudrya\Adminix\Modules\Link\MenuLinkModule;
use AlexKudrya\Adminix\Modules\SidebarMenu;
use Exception;

class PageGenerator
{
    /**
     * @param array $params
     * @return AdminixPage
     * @throws Exception
     */
    public function generatePage(array $params): AdminixPage
    {
        $uri = $params[0];

        if ($uri === '') {
            $uri = '/';
        }

        /** @var AdminixPage[] $pages */
        $pages = config('adminix.pages');

        foreach ($pages as $page) {

            if (is_null($page->uri())) {
                throw new Exception('Absent URI in page ' . get_class($page));
            }

            if (is_null($page->name())) {
                throw new Exception('Absent name in page ' . get_class($page));
            }

            if ($uri === $page->uri()) {

                foreach ($page->modules() as &$module) {
                    $data_provider = (new DataProviderFactory())->getDataProvider($module);
                    $module = $data_provider->handle($module, \Arr::except($params, [0]));
                }

                $page->sidebar($this->generateMenu());

                return $page;
            }
        }

        throw new Exception("Adminix page with URI \"$uri\" not found", 404);
    }

    /**
     * @return SidebarMenu
     * @throws Exception
     */
    protected function generateMenu(): SidebarMenu
    {
        $out_links = [];

        $menu_title = config('adminix.menu.title');
        $menu_links = config('adminix.menu.links');

        if (!$menu_title) {
            throw new Exception("Absent menu.title in Adminix config");
        }

        if (!$menu_links) {
            throw new Exception("Absent menu.links in Adminix config");
        }


        foreach ($menu_links as $menu_link) {
            if ($menu_link instanceof LinkInterface) {
                $out_links[] = $menu_link;
            } elseif (is_array($menu_link)) {
                $out_links[] = (new MenuLinkModule())
                ->setUri($menu_link['uri'])
                ->setUri($menu_link['title'] ?? null)
                ->setUri($menu_link['icon'] ?? null);
            }
        }

        return SidebarMenu::title($menu_title)->links($out_links);
    }
}
