<?php

namespace AlexKudrya\Adminix\Services;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Enums\ModuleTypeEnum;
use AlexKudrya\Adminix\Modules\AdminixModuleInterface;
use AlexKudrya\Adminix\Modules\List\ListField;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;

class AdminixHelper
{
    /**
     * @param string $page_name
     * @param string $module_name
     * @param string $config_name
     * @return mixed
     */
    public function getModuleConfig(string $page_name, string $module_name, string $config_name): mixed
    {
        $config = config('adminix');

        foreach ($config['pages'] as $page) {
            /** @var AdminixPage $page */
            foreach ($page->modules() as $module) {
                /** @var AdminixModuleInterface $module */

                if ($module->name() === $module_name && $page_name === $page->name()) {
                    return $module->$config_name() ?? null;
                }
            }
        }

        return null;
    }

    /**
     * @param $page_name
     * @param $module_name
     * @return array
     */
    public function getValidationRules($page_name, $module_name): array
    {
        $module_validation = $this->getModuleConfig($page_name, $module_name, 'validation') ?? [];

        $fields = $this->getModuleConfig($page_name, $module_name, 'fields') ?? [];

        foreach ($fields as $field) {
            /** @var ResourceField|ListField $field */

            if (!is_array($module_validation[$field->field()] ?? null)) {
                $module_validation[$field->field()] = [];
            }

            foreach ($field->validation() ?? [] as $rule) {
                if (!in_array($rule, $module_validation[$field->field()], true)) {
                    $module_validation[$field->field()][] = $rule;
                }
            }

            if (empty($module_validation[$field->field()])) {
                unset($module_validation[$field->field()]);
            }
        }

        return $module_validation;
    }

    /**
     * @param string $page_name
     * @param string $module_name
     * @return array
     */
    public function getUpdatableFields(string $page_name, string $module_name): array
    {
        $module_type = $this->getModuleConfig($page_name, $module_name, 'type');
        $fields = $this->getModuleConfig($page_name, $module_name, 'fields');

        $out = [];

        foreach ($fields as $field) {
            /** @var ResourceField|ListField $field */

            if ($module_type !== ModuleTypeEnum::LIST &&
                ($field->isReadonly() ?? false) === true) {
                continue;
            }

            if ($module_type === ModuleTypeEnum::LIST &&
                ($field->isEditable() ?? false) !== true ) {
                continue;
            }

            $out[] = $field->field();
        }

        return $out;
    }

    /**
     * @param array $input_data
     * @return array
     */
    public function modifyCheckboxValues(array $input_data): array
    {
        foreach ($input_data as &$value) {
            if ($value === 'on') $value = TRUE;
            if ($value === 'off') $value = FALSE;
        }

        return $input_data;
    }
}
