<?php

namespace AlexKudrya\Adminix\Console;

use AlexKudrya\Adminix\Services\PageProviderCreator;
use Exception;
use Illuminate\Contracts\Console\PromptsForMissingInput;
use Illuminate\Console\Command;

class PageMakeCommand extends Command implements PromptsForMissingInput
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'make:adminix_page {name : The name of the page}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Adminix page provider file';


    public function __construct(
        private  PageProviderCreator $creator
    )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $name = trim($this->input->getArgument('name'));

        $this->writePageProvider($name);
    }

    /**
     * Write the Adminix page provider file to disk.
     *
     * @param string $name
     * @return void
     * @throws Exception
     */
    protected function writePageProvider(string $name): void
    {
        $file = $this->creator->create($name, $this->getTargetPath());

        $this->components->info(sprintf('Adminix page provider [%s] created successfully.', $file));
    }

    /**
     * @return string
     */
    protected function getTargetPath(): string
    {
        return $this->laravel->basePath().'/app/Adminix/Pages';
    }

    /**
     * Prompt for missing input arguments using the returned questions.
     *
     * @return array
     */
    protected function promptForMissingArgumentsUsing()
    {
        return [
            'name' => ['What should the Adminix page be named?', 'E.g. ProductsPage'],
        ];
    }
}
