# Chart Bar


![image.png](b1948a22fb2fc06691c1de449dbe87d5.png)

## Description

The **`Chart-Bar`** module displays the ratio of the number of records of a certain entity in accordance with specified criteria. For example it can display ratio of the number of users by roles.

## Configuration

```php

use App\Models\User;
use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Modules\ChartBar\ChartBarItem;
use AlexKudrya\Adminix\Modules\ChartBar\ChartBarModule;

    ...

    $page = new AdminixPage();
    
    ...
    
    $page->addModule(
        ChartBarModule::title('USERS BY ROLES')
            ->name('users_by_roles')
            ->dataSource(User::class)
            ->criteria([
                ['is_banned', '=', false],
            ])
            ->addBars(
                ChartBarItem::label('Client')
                    ->color(ColorsEnum::MEDIUM_SEA_GREEN)
                    ->criteria([['role_id', '=', 1]]),
                    
                ChartBarItem::label('Guest')
                    ->color(ColorsEnum::ROYAL_BLUE)
                    ->criteria([['role_id', '=', 2]]),
                    
                ChartBarItem::label('Manager')
                    ->color(ColorsEnum::ORANGE)
                    ->criteria([['role_id', '=', 3]]),
                    
                ChartBarItem::label('Admin')
                    ->color(ColorsEnum::FIRE_BRICK)
                    ->criteria([['role_id', '=', 4]]),
            )
    );
```

To add to the page **Chart Bar** module you need paste `AlexKudrya\Adminix\Modules\ChartBar\ChartBarModule` class 
instance as an argument to `addModule` method of `AdminixPage` object.

## `ChartBarModule` Configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**title**
</td>
<td>

Title of the chart on top of this module

```php
title('USERS BY ROLES')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**name**
</td>
<td>

Name of module, must be unique in current page.

```php
name('users_by_roles')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**dataSource**
</td>
<td>

Source of data for chart, can be an `Eloquent Model` - `\App\Models\User::class` or name of table in database `users` or `public.users`

```php
dataSource(User::class)
// or
dataSource('users')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**criteria**
</td>
<td>

Database `Builder` criteria all bars in chart. For example to not show banned users you need paste this:

```php
criteria([
    ['is_banned', '!=', 1]
])
```

Or you can paste parameter from route:

```php
criteria([
    ['store_id', '=', 'param:1']
)
```

For example, for `https://site.com/adminix/users_by_store/2` it wiil be mean that you need dynamics for `store_id == 2`.
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**bars**
</td>
<td>Array of bars for this chart with personal configuration for each of them</td>
<td>

**Required**
</td>
</tr>
</table>

## Bars

To add **Bars** to your **ChartBar** module you need paste `AlexKudrya\Adminix\Modules\ChartBar\ChartBarItem` class 
instance as an argument to `addBar` or `addBars` method of `ChartBarModule` object.

## `ChartBarItem` configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**label**
</td>
<td>

Label for current bar in chart

```php
label('Client')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**color**
</td>
<td>

Color of current bar in chart, can be provided only by `AlexKudrya\Adminix\Enums\ColorsEnum` Enum class

```php
color(ColorsEnum::MEDIUM_SEA_GREEN)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**criteria**
</td>
<td>

Criteria for selection record to count in current bar

For example to display in bar number of users with `role_id == 3` you need paste this:

```php
criteria([
    ['role_id', '=', 3],
])
```
</td>
<td>

**Required**
</td>
</tr>
</table>

## Appearance examples

![image.png](e17a8cbd789ec6e4ac5f3692ff81b2ea.png)

![image.png](174a5f388f1a891507bfd4078a7c14eb.png)
