# Modules

Every page in Adminix panel consist from basic parts - **Modules**.

There are can be a lot of modules on single page, or just one.
You may customize every page, every module, every detail such as you want, everything you need for realize your goals.

* [**List**](List.md)
* [**Link**](Link.md)
* [**Resource**](Resource.md)
* [**New Resource**](New-resource.md)
* [**Chart-Bar**](Chart-Bar.md)
* [**Chart-Line**](Chart-Line.md)
* [**Counter**](Counter.md)
* [**Clock**](Clock.md)