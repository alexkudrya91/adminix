# Clock

![lsaff0cm0cm2303ifmf2k29fk39k3_3.png](lsaff0cm0cm2303ifmf2k29fk39k3.png)

## Description

Just a clock for your dashboard. Cool, isn't it? Just for fun.

## Configuration

```php
use App\Models\User;
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Clock\ClockModule;
use Carbon\Carbon;

    ...

    $page = new AdminixPage();
    
    ...

    $page->addModule(
        ClockModule::title('Current date and time')
            ->locale('en')
            ->name('clock')
    );
    ...
```
To add to the page **Clock** module you need paste `AlexKudrya\Adminix\Modules\Clock\ClockModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object.

## `ClockModule` configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**title**
</td>
<td>

Title visible on mouseover.

```php
 title('Current date and time')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**name**
</td>
<td>

Name of module, must be unique in current page.

```php
name('clock')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**locale**
</td>
<td>

Locale for date text. By default `en`.

Available locales you can see here [Moment.js](https://momentjs.com/)

```php
locale('en')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**disableDate**
</td>
<td>

Disabling date text, if you want to see only time on your clock module.

```php
disableDate()
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**disableTime**
</td>
<td>

Disabling time, if you want to see only date text on your clock module.

```php
disableTime()
```
</td>
<td>

**Optional**
</td>
</tr>
</table>