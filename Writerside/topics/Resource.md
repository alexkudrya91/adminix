# Resource

1. [**Configuration**](#resource-configuration)
2. [**Fields**](#fields)
3. [**Validation**](#validation)
4. [**Appearance examples**](#appearance-examples)

## Description

The "Resource" module purpose - render a form to edit some exited record of some entity

Format is very similar to [New resource](New-resource.md) module.
Difference - `Resource` module is a form for working with exiting records, `NewResource` - is a form for creating new records.

## `Resource` configuration

To add to the page `NewResource` module you need paste `AlexKudrya\Adminix\Modules\Resource\NewResourceModule`
class instance as an argument to `addModule` method of `AdminixPage` object.

Example:

```php
use App\Models\User;
use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;

    ...

    $page = new AdminixPage();
    
    ...
    
    $page->addModule(
        NewResourceModule::title('NEW USER')
            ->title('NEW USER')
                ->dataSource(User::class)
                ->successRedirectUri(UsersPage::URI)
                ->addFields(...)
    )

...
```

### `NewResource` configuration methods

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

name
</td>
<td>

Name of module, must be unique in current page.

```php
name('new_user')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

title
</td>
<td>

Title on the top of module

```php
title('NEW USER')
```
</td>
<td>

**Optional**
</td>
</tr>

<tr>
<td>

addProp

addProps
</td>
<td>

An array of fields for properties in url to select the desired record from the database.

For example:
The url is - `https://domain.my/adminix/user/341`, then `'param:1'` become to `341`

An argument can be only `AlexKudrya\Adminix\Modules\Resource\ResourceProperty` 

```php
->addProp(
    ResourceProperty::key('id')->value('param:1')
)
```
or

```php
->addProps(
    ResourceProperty::key('id')->value('param:1'),
    ResourceProperty::key('is_admin')->value(false),
)
```
</td>
<td>

**Optional**
</td>
</tr>

<tr>
<td>

dataSource
</td>
<td>

Source of data for new entity creation, can be an `Eloquent Model`, for example `\App\Models\User::class`
or name of table in database `users` or `public.users`

```php
dataSource(\App\Models\User::class)
```

or

```php
dataSource('users')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>
addField

addFields
</td>
<td>

Methods by which you can add fields for current `NewRosurce` module. Details described below. [#Fields](#fields)

Argument can be only `AlexKudrya\Adminix\Modules\Resource\ResourceField` class instance.

```PHP
->addField(
    ResourceField::name('Name')
        ->field('name')
        ->type(ResourceInputTypeEnum::STRING)
        ->required()
)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

addFieldValidation
</td>
<td>

Method by which you can add validation rules for field in current `NewRosurce` module.

Format similar to native [**Laravel Validation**](https://laravel.com/docs/master/validation#quick-writing-the-validation-logic)
feature.

Details described below. [#Validation](#validation)

```PHP
->addFieldValidation(
    'name',
    [
        'string',
        'required',
        "unique:". User::class.",name"
    ]
)
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

readonly
</td>
<td>

All fields become not editable, render not `inputs` but just **text**
```php
readonly()
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

successRedirectUri
</td>
<td>

An uri of adminix page, where user will be redirected in case of successful record creation.

```php
successRedirectUri(UsersPage::URI)
```
</td>
<td>

**Optional**
</td>
</tr>
</table>

## Fields

Fields is an array, of input fields to be rendered in the Form.

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;
use AlexKudrya\Adminix\Modules\InputSelectSrc;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;

$page = new AdminixPage();

$page->addModule(
    NewResourceModule...
        ->addFields(
            ResourceField::name('Name')
                ->field('name')
                ->type(ResourceInputTypeEnum::STRING)
                ->required(),

            ResourceField::name('Email')
                ->field('email')
                ->type(ResourceInputTypeEnum::EMAIL)
                ->required(),
                
            ResourceField::name('Password')
                ->field('password')
                ->type(ResourceInputTypeEnum::PASSWORD)
                ->confirmed()
                ->required(),
                
            ResourceField::name('Role')
                ->field('role_id')
                ->type(ResourceInputTypeEnum::SELECT)
                ->required()
                ->src(
                    InputSelectSrc::dataSource(Role::class)
                        ->nameField('name')
                        ->valueField('id')
                ),
                
            ResourceField::name('Admin')
                ->field('is_admin')
                ->type(ResourceInputTypeEnum::HIDDEN)
                ->value(false),
        )
)

...
```

To add **field** to your `NewResource` module you need paste `AlexKudrya\Adminix\Modules\Resource\ResourceField` class
instance as an argument to `addField` or `addFields` method of `NewResourceModule` object.

### `ResourceField` configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**name**
</td>
<td>

Title of the libel displayed near to the input

```php
'name' => 'Email',
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**type**
</td>
<td>

Type of rendered input, can be provided only by `AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum` Enum class.

Available types:

* **`STRING`**,
* **`EMAIL`**,
* **`INTEGER`**,
* **`BOOLEAN`**,
* **`SELECT`**,
* **`DATE`**,
* **`DATETIME`**,
* **`TIME`**,
* **`WEEK`**,
* **`MONTH`**,
* **`IMAGE`**,
* **`HIDDEN`**,
* **`PASSWORD`**, // but useless for exiting record...
* **`RANGE`**,
* **`PHONE`** / **`TEL`**,
* **`COLOR`** (colorpicker),
* **`TEXT`** / **`TEXTAREA`** (multiline text),
* **`EDITOR`** / **`CKEDITOR`** / **`WYSIWYG`** (advanced text editor)

```php
type(ResourceInputTypeEnum::EMAIL)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**field**
</td>
<td>

Name of field in database table where new record will be created.

```php
field('role_id')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**required**
</td>
<td>

If enabled - field will be required for form submitting, and form wil not be submitted until this field wil not be filled.
By default, is disabled.

```php
required()
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**src**
</td>
<td>

Required for filed with type `SELECT` if **select_records** is empty. Need to display correct `<options/>` of `<select/>`input.

For example, `users` table has related table `roles` , and relation maked by `role_id` field in `users` table. To dasplay select with options from `roles` table, you need to configure **src** correctly.

Example:

```php
ResourceField::name('Role')
    ->field('role_id')
    ->type(ResourceInputTypeEnum::SELECT)
    ->required()
    ->src(
        InputSelectSrc::dataSource(Role::class)
            ->nameField('name')
            ->valueField('id')
    ),
```

| setting    | Description |
|------------|-------------|
| dataSource | Source of data for filed items, can be an `Eloquent Model` - `\App\Models\Role::class` or name of table in database `roles`or `public.roles` |
| nameField  | Name of field which will be a label in `<option/>` |
| valueField | Name of field which will be a value in `<option/>` (usually it is "id") |

</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**addSelectRecords**
</td>
<td>

Required for fields with type `SELECT` as an alternative for `src()`.
Required if **`src()`** is empty. It is a list of options, where `name()` is a title of `<option/>`, and `value()`is a value of `<option/>`

Arguments can be only `AlexKudrya\Adminix\Modules\SelectRecord` class instances.


```php
ResourceField::name('Role')
    ->field('role_id')
    ->type(ResourceInputTypeEnum::SELECT)
    ->addSelectRecords(
        SelectRecord::name('Admin')->value(1),
        SelectRecord::name('Manager')->value(2),
        SelectRecord::name('Seller')->value(3),
        SelectRecord::name('Guest')->value(4),
    )
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**value**
</td>
<td>

Used only for fields with **type** `HIDDEN` and provides fixed **value** to it.

```php
value('some text')
```

Or you can paste parameter from route, if you provided it. For example `https://site.com/adminix/new_order/315`, **315** in this case will be pasted as value into this hidden input

```php
value('param:1')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**confirmed**
</td>
<td>

Can be used only for fields with **type** `PASSWORD`.
Determines password confirmation and renders additional `PASSWORD` input below current for password confirmation.

Validation for password confirmation working automatically.

```php
confirmed()
```
</td>
<td>

**Optional**
</td>
</tr>

<tr>
<td>

**addValidationRules**
</td>
<td>

Adding validation rules for current field.

Validation for password confirmation working automatically.

Format similar to Laravel native Validation feature.

Details here [Validation](#validation).

```php
->addValidationRules(
    'integer',
    'required',
    'exists:'. Role::class.',id'
),
```
</td>
<td>

**Optional**
</td>
</tr>
</table>

## Validation

An array of validation rules for form input data. Format similar to Laravel native Validation feature.

[**#Laravel Validation**](https://laravel.com/docs/master/validation#quick-writing-the-validation-logic)

There are 3 ways to add validation rules to your `newResource` form:
* Add rules to every `ResourceField` personally, by using `addValidationRules()` method
* Add rules to every **field** by using `addFieldValidation()` method of `NewResourceModule` object
* Add all rules for add **fields** by using `validation()` method of `NewResourceModule` object

Way 1 example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;
use AlexKudrya\Adminix\Modules\InputSelectSrc;
use AlexKudrya\Adminix\Modules\Resource\ResourceField;
use AlexKudrya\Adminix\Modules\Resource\ResourceInputTypeEnum;

$page = new AdminixPage();

$page->addModule(
    NewResourceModule...
        ->addFields(
            ResourceField::name('Name')
                ->field('name')
                ->type(ResourceInputTypeEnum::STRING)
                ->required()
                ->addValidationRules(
                    'string',
                    'required',
                    "unique:". User::class.",name"
                ),

            ResourceField::name('Email')
                ->field('email')
                ->type(ResourceInputTypeEnum::EMAIL)
                ->required()
                ->addValidationRules(
                    'email',
                    'required',
                    "unique:". User::class.",email"
                ),
                
            ResourceField::name('Password')
                ->field('password')
                ->type(ResourceInputTypeEnum::PASSWORD)
                ->confirmed()
                ->required()
                ->addValidationRules(
                    'string',
                    'required',
                    'confirmed'
                ),
                
            ResourceField::name('Role')
                ->field('role_id')
                ->type(ResourceInputTypeEnum::SELECT)
                ->required()
                ->src(
                    InputSelectSrc::dataSource(Role::class)
                        ->nameField('name')
                        ->valueField('id')
                )
                ->addValidationRules(
                    'integer',
                    'required',
                    'exists:'. Role::class.',id'
                ),
        )
)

...
```

Way 2 example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;

$page = new AdminixPage();

$page->addModule(
    NewResourceModule...
        ->addFields(...)
        ->addFieldValidation('name', [
            'string',
            'required',
            'unique:App\Models\User,name',
        ])
        ->addFieldValidation('email', [
                'email',
                'required',
                'unique:App\Models\User,email',
        ])
        ->addFieldValidation('password', [
            'string',
            'required',
            "confirmed",
        ])
        ->addFieldValidation('role_id', [
            'integer',
            'required',
            'exists:App\Models\Role,id',
        ])
)
...
```

Way 3 example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Resource\NewResourceModule;

$page = new AdminixPage();

$page->addModule(
    NewResourceModule...
        ->addFields(...)
        ->validation([
            'name' => [
                'string',
                'required',
                'unique:App\Models\User,name',
            ],
            'email' => [
                'email',
                'required',
                'unique:App\Models\User,email',
            ],
            'password' => [
                'string',
                'required',
                "confirmed",
            ],
            'role_id' => [
                'integer',
                'required',
                'exists:App\Models\Role,id',
             ],
        ])
)
...
```

## Appearance examples

Example: User id = 1 Record form for edition

![image.png](b7f41485688ae2599d5fcf5c85c807f8.png)