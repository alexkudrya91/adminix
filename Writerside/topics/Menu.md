# Menu

Menu, located on left side of screen in your admin panel, configured in a file `confing/adminix.php`, in `menu` array

```php
// confing/adminix.php

   return [

       ...

       'menu' => [
           'title' => 'MyAdmin',
           'links' => [
              ...
           ]
       ]
   ]
```

| Method | Description                                                         |
|--------|---------------------------------------------------------------------|
| title  | Text title in the top-left corner on of your admin panel            |
| links  | List of links in left-side menu. Link configuration described below |

Example:

```php
// confing/adminix.php

use AlexKudrya\Adminix\Modules\Link\MenuLinkModule;
use App\Adminix\Pages\IndexPage;
use App\Adminix\Pages\UsersPage;

... 
        'links' => [
            MenuLinkModule::title('Dashboard')
                ->uri(IndexPage::URI)
                ->icon('bi bi-speedometer2'),
            
            MenuLinkModule::title('Users')
                ->uri(UsersPage::URI)
                ->icon('bi bi-people-fill'),
            ...
        ]
...
```

Link configuration:

<table>
<tr>
<th>Method</th>
<th>Description</th>
</tr>
<tr>
<td>uri</td>
<td>

Defines the URL address of the Link.

Structure - **{Base URL}** **/ {Prefix (from config)} / {Page URI}**

Example if page uri set to "users"- `https://mydomain.com/adminix/users`

Exception - "/", it is mean Adminix home page - `https://mydomain.com/adminix`

Best practice - Put URI into Page as a constant, as in this example
</td>
</tr>
<tr>
<td>title</td>
<td>Text title of the Link</td>
</tr>
<tr>
<td>icon</td>
<td>

(optional) Icon class name from [Bootsrap icons](https://icons.getbootstrap.com/)

Example: to generate in the Link an icon `<i class="bi bi-people-fill"></i>` you need paste here `bi bi-people-fill` class name
</td>
</tr>
</table>

Rendered example:

```php
// confing/adminix.php

use AlexKudrya\Adminix\Modules\Link\MenuLinkModule;
use App\Adminix\Pages\IndexPage;
use App\Adminix\Pages\UsersPage;
use App\Adminix\Pages\ProductsPage;

... 
    'menu' => [
        'title' => 'MyAdmin',
        'links' => [
            MenuLinkModule::title('Dashboard')
                ->uri(IndexPage::URI)
                ->icon('bi bi-speedometer2'),
            
            MenuLinkModule::title('Users')
                ->uri(UsersPage::URI)
                ->icon('bi bi-people-fill'),
            
            MenuLinkModule::title('Products')
                ->uri(ProductsPage::URI)
                ->icon('bi bi-cart4')
        ]
    ]
...
```

![image.png](2d0bf0e35334cbbf09ab72ad7df7dbc1.png)
