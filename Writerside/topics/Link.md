# Link 

## Description

The "Link" module purpose - render a link button to some route in Laravel app, or to some page in Adminix panel.

## Configuration

```php
use App\Models\User;
use AlexKudrya\Adminix\Modules\Link\AdminixLinkModule;
use AlexKudrya\Adminix\Modules\Link\LinkModule;
use AlexKudrya\Adminix\Enums\HttpMethodsEnum;

    ...

    $page = new AdminixPage();
    
    ...

    $page->addModules(
        AdminixLinkModule::name('user_cart_btn')
            ->title('USER CART')
            ->uri(UserCartPage::URI)
            ->icon('bi bi-cart3')
            ->params(['param:1']),

        LinkModule::name('user_account_btn')
            ->title('USER ACCOUNT')
            ->uri('user_account')
            ->icon('bi bi-person')
            ->params(['param:1'])
            
        LinkModule::name('user_ban_btn')
            ->title('BAN USER')
            ->uri('user_ban')
            ->icon('bi bi-person-fill-slash')
            ->method(HttpMethodsEnum::POST)
            ->params(['param:1'])
    );  
    ...
]
```

To add to the page **Link** to some page in your Adminix panel, you need paste `AlexKudrya\Adminix\Modules\Link\AdminixLinkModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object. This link used only for navigation 
between adminix pages, difened in adminix config file

To add to the page **Link** to some page outside the Adminix panel, but in your Laravel Application, you need paste `AlexKudrya\Adminix\Modules\Link\LinkModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object. This link used to access any 
route in your Laravel application (route must be named and optionally protected by 
`AlexKudrya\Adminix\Http\Middleware\AuthMiddleware`. Use this middleware only after `web` and `auth` middlewares)

## `AdminixLinkModule` and `LinkModule` configurations

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>name</td>
<td>

Name of module, must be unique in current page.

```php
name('user_cart_btn')
```
</td>
<td>

**Required**
</td>
</tr>

<tr>
<td>uri</td>
<td>

In case **`AdminixLinkModule`** uri means the `uri` directive of page which defined in Adminix config file.
In this case better to use `URI` constant from target page provider class.

```php
uri(UserCartPage::URI)
```

In case **`LinkModule`** uri means the name of any route defined in router of your Laravel application.
Just copy route name from router.

```php
uri('user_account')
```

</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>title</td>
<td>

Title which will be displayed on the button

```php
name('USER CART')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>icon</td>
<td>

Icon class name from [Bootsrap icons](https://icons.getbootstrap.com/)

Example: to generate in the Link an icon `<i class="bi bi-people-fill"></i>` you need paste here `bi bi-people-fill` class name

```php
 icon('bi bi-cart3')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>method</td>
<td>

Type of HTTP request, can be provided only by `AlexKudrya\Adminix\Enums\HttpMethodsEnum` Enum class

In case **`AdminixLinkModule`** method can be only **GET** so it is redundant.

In case **`LinkModule`** method must be match to the route defined in **uri** directive.

By default, it is equal to `HttpMethodsEnum::GET`

```php
method(HttpMethodsEnum::POST)
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>params</td>
<td>

It is list of parameters which you added to link. When `value` = "`param:1`", means that first parameter of current page will be the parameter. For example, current page url - `https://example.com/adminix/user/123`, where `user` is a page name, and `123` is a parameter,

link url will be `https://example.com/adminix/user_cart/123`

Example:

```php
 params([
     ['value' => 'param:1']
 ])
```
</td>
<td>

**Optional**
</td>
</tr>
</table>

## Appearance example

![image.png](c74c623f5b04c67463c720a30100d183.png)