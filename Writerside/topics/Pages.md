# Pages list

In the Adminix, page is consists of modules.

For now, available modules there are:

- [List](List.md)
- [Link](Link.md)
- [Resource](Resource.md)
- [New resource](New-resource.md)
- [Chart-Line](Chart-Line.md)
- [Chart-Bar](Chart-Bar.md)
- [Counter](Counter.md)
- [Clock](Clock.md)

To configure your page, you need to put to `page` array in `config/adminix.php`, an array with two keys: `uri` and `modules`

Example:

```php
// config/adminix.php  

use App\Adminix\Pages\IndexPage;
use App\Adminix\Pages\UsersPage;

    ...
    'pages' => [
         IndexPage::get(),
         UsersPage::get(),
         ...
    ]
    ...
```
Every **page** is an instance of `AdminixPageProvider` interface, and has hiw own personal configuration, details in [Page configurations](Page.md)