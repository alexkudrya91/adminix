# Installation


```
composer require alex-kudrya/adminix
```

In `app.php` add to `providers` array `\AlexKudrya\Adminix\Providers\AdminixServiceProvider::class`

```php
// config/app.php

'providers' => [ 
    ... 
    \AlexKudrya\Adminix\Providers\AdminixServiceProvider::class,
    ...
 ]
```

Publish package **config** to your app

```
php artisan vendor:publish --provider=AlexKudrya\Adminix\Providers\AdminixServiceProvider
```

## Authorization configuration
**!Note**: If you planned to provide access to admin panel without authorization you do not  need to do it.

Run migrations

```
php artisan migrate
```

Add `AdminixUser` Trait to your user's model (usually it is `/App/Models/User`) and implements it to `AdminixUserInterface`

```php
// App/Models/User.php

namespace App\Models;

use AlexKudrya\Adminix\AdminixUser;
use AlexKudrya\Adminix\AdminixUserInterface;

...

class User extends Authenticatable implements AdminixUserInterface
{
    use AdminixUser;

...

}
```

In `config/adminix.php` make sure that in `'admin_user_model'` is your user's model

```php
// config/adminix.php

return [
    'route_prefix' => env("ADMINIX_PREFIX", 'adminix'),

    'admin_user_model' => \App\Models\User::class,

    'pages' => [...],

    'menu' => [...]
]
```

## Robots.txt
Optionally you can disallow indexing `adminix` pages for search engines in `robots.txt`:

```
User-agent: *

Allow: /
Disallow: /adminix
```

So, you have done the preliminary installation, next you need to [**configure**](Configurartion.md) the [**pages**](Pages.md) and [**menus**](Menu.md) of your admin panel.