# Chart Line

1. [**Description**](#description)
2. [**Configuration**](#configuration)
3. [**`ChartLineModule` configuration**](#chartlinemodule-configuration)
4. [**Appearance examples**](#appearance-examples)

## Description

**Chart-line** is a chart which displays dynamic of creation records of some entity, **`users`**, **`orders`**, **`comments`** etc. Chart can display daily or monthly results.

![image.png](4b2291532993646a9ebe1dee6592cdca.png)

## Configuration

Example: (there 2 variants of this module)

```php
use App\Models\User;
use AlexKudrya\Adminix\Enums\ColorsEnum;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineValueTypeEnum;
use AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule;

    ...

    $page = new AdminixPage();

    ...

    $page->addModule(
        ChartLineModule::title('USER REGISTRATIONS DAILY')
            ->name('user_registrations')
            ->color(ColorsEnum::DEEP_PINK)
            ->valueType(ChartLineValueTypeEnum::COUNT_DAILY)
            ->limit(30)
            ->dataSource(User::class)
            ->criteria([
                 ['role_id', [2,3,4]]  
            ]),
            
        ChartLineModule::title('USER REGISTRATIONS YEAR')
            ->name('user_registrations_year')
            ->color(ColorsEnum::ORANGE)
            ->valueType(ChartLineValueTypeEnum::COUNT_MONTHLY)
            ->limit(6)
            ->dataSource('users')
            ->criteria([
                 ['role_id', 'param:1']  
            ]),  
    )
    ...
```
To add to the page **Chart Line** module you need paste `AlexKudrya\Adminix\Modules\ChartLine\ChartLineModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object.

## `ChartLineModule` Configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**title**
</td>
<td>

Title of the chart on top of this module

```php
title('USER REGISTRATIONS DAILY')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**name**
</td>
<td>

Name of module, must be unique in current page.

```php
name('user_registrations')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**color**
</td>
<td>

Color of lines in chart, can be provided only by `AlexKudrya\Adminix\Enums\ColorsEnum` Enum class

```php
color(ColorsEnum::DEEP_PINK)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**valueType**
</td>
<td>

Type of interval for records counting. Can be provided only by `AlexKudrya\Adminix\Modules\ChartLine\ChartLineValueTypeEnum`
Enum class. Has only two options: `COUNT_DAILY` and `COUNT_MONTHLY`

```php
valueType(ChartLineValueTypeEnum::COUNT_DAILY)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**baseField**
</td>
<td>

Field with date / datetime (usually it is a record creation timestamp). By default, it is **`created_at`**

```php
baseField('created_at')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**dataSource**
</td>
<td>

Source of data for chart, can be an `Eloquent Model` - `\App\Models\User::class` or name of table in database `users` or `public.users`

```php
dataSource(\App\Models\User::class)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**criteria**
</td>
<td>

Database `Builder` criteria to render records. For example to not show administrator you need paste this:

```php
criteria([
    ['role_id', '!=', 1]
)
```

Or you can paste parameter from route:

```php
criteria([
    ['role_id', '=', 'param:1']
)
```

For example, for **`https://site.com/adminix/users_by_role/2`** it wiil be mean that you need dynamics for **`role_id == 2`**.
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**limit**
</td>
<td>

limit of **days / months** for displaying in the chart

```php
limit(30)
```
</td>
<td>

**Required**
</td>
</tr>
</table>

## Appearance examples

Here is an example of **Users daily registrations** (**`value_type`** = **`count_daily`**, **`limit`**= **`30`**)

![image.png](e58dce44f668dc78e6de4f592a5a208e.png)

Here is an example of **Users monthly registrations** (**`value_type`** = **`count_monthly`**, **`limit`**= **`6`**)

![image.png](e5aef4aba985a840d9a8a1c3a05e785b.png)