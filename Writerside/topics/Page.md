# Page

Page configuration providing by class implements interface `AlexKudrya\Adminix\AdminixPageProvider`

```PHP
    // app/Adminix/Pages/IndexPage.php
    
    namespace App\Adminix\Pages;
    
    use AlexKudrya\Adminix\AdminixPageProvider;
    use AlexKudrya\Adminix\AdminixPage;
    
    class IndexPage implements AdminixPageProvider
    {
        const URI = '/';
    
        public static function get(): AdminixPage
        {
            $page = new AdminixPage();
            
            $page->setAsHomePage();
    
            $page->uri(self::URI);
    
            $page->name('index');
            
            $page->addModules(...);
            
            return $page;
        }
    }
```

Every page provider class must implement `get()` method which return `AlexKudrya\Adminix\AdminixPage` class instance

## Create new Adminix page

To create new page to should use next Artisan command:
```Shell
 php artisan make:adminix_page
```

you also can get your new page the name, for example `Products`:

```Shell
 php artisan make:adminix_page products
```

Result, new Adminix page file will be created - `app/Adminix/Pages/ProductsPage.php`

```PHP
// app/Adminix/Pages/ProductsPage.php
<?php

namespace App\Adminix\Pages;

use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\AdminixPageProvider;

class ProductsPage implements AdminixPageProvider
{
    const URI = 'products';

    public static function get(): AdminixPage
    {
        $page = new AdminixPage();

        $page->uri(self::URI);

        $page->name('products');

        $page->addModules(
            // ToDo: add required modules ...
        );

        return $page;
    }
}
```

After that, you should add this class to `pages` array in `config/adminix.php`:

```php
// config/adminix.php  

use App\Adminix\Pages\IndexPage;
use App\Adminix\Pages\UsersPage;
use App\Adminix\Pages\ProductsPage;

    ...
    'pages' => [
         IndexPage::get(),
         UsersPage::get(),
         ProductsPage::get(), // <- Here is your new page
         ...
    ]
    ...
```
And if you need, you can add to sidebar menu link to this page:

```php
// confing/adminix.php

use AlexKudrya\Adminix\Modules\Link\MenuLinkModule;
use App\Adminix\Pages\IndexPage;
use App\Adminix\Pages\UsersPage;
use App\Adminix\Pages\Products;

... 
    'menu' => [
        'title' => 'Admin panel',
        'links' => [
            MenuLinkModule::title('Dashboard')
                ->uri(IndexPage::URI)
                ->icon('bi bi-speedometer2'),

            MenuLinkModule::title('Users')
                ->uri(UsersPage::URI)
                ->icon('bi bi-people-fill'),
                
           // Below is your new link to your new page -> 

            MenuLinkModule::title('Products')
                ->uri(Products::URI)
                ->icon('bi bi-boxes'),
                
            ...
        ]
    ],
...
```



## Adminix page configuration


<table>
<tr>
<th>Method</th>
<th>Description</th>
</tr>
<tr>
<td>

**setAsHomePage**
</td>
<td>
Define home page for Adminix panel. 

Must be used **only for one** Adminix page (usually it is `index` page).
</td>
</tr>
<tr>
<td>

**uri**
</td>
<td>

Defines the URL address of adminix page.

**Required**.

Must be **unique**.

Structure - **[Base URL]** **/ [Prefix (from config)] / [Page URI]**.

Example if page uri set to `products` page full URL wil be - `https://mydomain.com/adminix/products`.

Recommended to define `URI` constant for using it in this method and for link in [menu configuration](Menu.md).
</td>
</tr>
<tr>
<td>

**name**
</td>
<td>

Defines system name for page.

**Required**.

Must be **unique**.

</td>
</tr>
<tr>
<td>

**addModule / addModules**
</td>
<td>
Adding module(s) to the page.

Every module is an instance of `AdminixTopModuleInterface` with his personal configuration. 

They are rendered in the same order in which they are executed by `addModule` or `addModules` methods in `AdminixPage` object.

How to configure modules described in [Modules](Modules.md).
</td>
</tr>
</table>