# Introduction

Adminix is an easy and fast customizable administration panel for working with database entities.

## How to start

- [**Installation**](Installation.md)
- [**Configuration**](Configurartion.md)
- [**Modules**](Modules.md)

![image.png](4465cec111982fd7b5a3563af28bddfc.png)

![image.png](fa23749ad9bd4db2809a9a83120dc186.png)