# List

1. [**Description**](#description)
2. [**Configuration**](#configuration)
3. [**Fields**](#fields)
4. [**Searching**](#searching)
5. [**Filters**](#filters)
6. [**Actions**](#actions)
7. [**New item link**](#new-item-link)
8. [**Appearance examples**](#appearance-examples)

## Description

The "List" module purpose - render list of some records from your database to work with them.


## Configuration

Basic example:

```php
use App\Models\User;
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListModule;

    ...

    $page = new AdminixPage();
    
    ...
    
    $page->addModule(
        ListModule::name('users')
            ->title('USERS')
            ->dataSource(User::class)
            ->pagination(20)
            ->addFields(...)
    )
    ...
```
To add to the page **List** module you need paste `AlexKudrya\Adminix\Modules\List\ListModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object.

## `ListModule` configuration

Full settings list:

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>name</td>
<td>

Name of module, must be unique in current page.

```php
name('users')
```
</td>
<td>Required</td>
</tr>
<tr>
<td>title</td>
<td>

Title on the top of the module

```php
title('USERS')
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>pagination</td>
<td>

Number of items per page. To disable pagination must be equal to **0** or not exist.

```php
pagination(20)
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>dataSource</td>
<td>

Source of data for List, can be an `Eloquent Model` - `\App\Models\User::class` or name of table in database `users` or `public.users`

```php
dataSource(\App\Models\User::class)
```
</td>
<td>Required</td>
</tr>
<tr>
<td>withDeleted</td>
<td>

Determines render or not deleted records (working with models with included `SoftDeletes` trait)

By default, is not active

```php
withDeleted()
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>editable</td>
<td>

Determines possibility to edit records directly in the List.
Editable possibility for every field determines in his personal config.
By default, is not active

```php
editable(),
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>primaryKey</td>
<td>

Primary key in source table. Used for **editable** mode. Usually it is "id"

```php
primaryKey('id')
```
</td>
<td>

Required if **editable** mode is **enabled**
</td>
</tr>
<tr>
<td>criteria</td>
<td>

Database `Builder` criteria to render records. For example to not show administrator you need paste this:

```php
criteria([
    ['is_admin', '=', false]
])
```

Or you can paste parameter from route:

```php
criteria([
    ['user_id', '=', 'param:1']
])
```

For example, for **`https://site.com/adminix/comments/21`** it will be mean that you need all comments where `user_id ==`**`21`**.
</td>
<td>Optional</td>
</tr>
<tr>
<td>resource</td>
<td>

Laravel `JsonResource` to provide data. 
Example: `\App\Http\Resources\UserAdminResource::class`

```php
resource(\App\Http\Resources\UserAdminResource::class)
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>addField<br/>addFields</td>
<td>

Methods by which you can add fields to **List** for displaying.

Arguments can be only `AlexKudrya\Adminix\Modules\List\ListField` class instances.

Detailed here [#Fields](#fields)

```php
addField(
    ListField::name('Name')
        ->field('name')
        ->type(ListFieldTypeEnum::STRING)
        ->sortable()
)
```
</td>
<td>Required</td>
</tr>
<tr>
<td>search</td>
<td>

Render "Search" input on the top of module.

Argument - `AlexKudrya\Adminix\Modules\List\ListSearch` class instance.

```PHP
search(
    ListSearch::searchFields(['name', 'email'])
        ->placeholder('Search users')
)
```

Detailed settings described below. [#Searching](#searching)
</td>
<td>Optional</td>
</tr>
<tr>
<td>addFilter<br/>addFilters</td>
<td>

Methods by which you can add filters for current **List**.

Arguments can be only several classes instances:
- `AlexKudrya\Adminix\Modules\List\ListFilter`
- `AlexKudrya\Adminix\Modules\List\ListDateFilter`
- `AlexKudrya\Adminix\Modules\List\ListDateRangeFilter`

```PHP
->addFilters(
    ListFilter::field('role_id')
        ->name('Role')
        ->src(
            InputSelectSrc::nameField('name')
                ->valueField('id')
                ->dataSource(UserRole::class)
        ),
    
    ListDateRangeFilter::field('created_at')
        ->name('Registration time')
        ->withTime()
)
```

Detailed settings described below. [#Filters](#filters)
</td>
<td>Optional</td>
</tr>
<tr>
<td>addAction<br/>addActions</td>
<td>

Methods by which you can add action buttons (Links) for every item in current **List** module.

Arguments can be only several classes instances:
- `AlexKudrya\Adminix\Modules\Link\AdminixLinkModule`
- `AlexKudrya\Adminix\Modules\Link\LinkModule`

```PHP
addAction(
    AdminixLinkModule::name('user_details')
        ->title('DETAILS')
        ->icon('bi bi-pencil')
        ->uri(UserPage::URI)
        ->params(['record:id']),
)
```

Detailed settings described below. [#Actions](#actions)
</td>
<td>Optional</td>
</tr>
<tr>
<td>sorting</td>
<td>

Basic sorting for records in current List module. 

Argument can be only `AlexKudrya\Adminix\Modules\Sorting` class instance.

Example:

```php
sorting(
    Sorting::field('id')
        ->direction('asc')
)
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>newItemLink</td>
<td>

Setting for "New Item" button rendered between Title and List, near to Search input.

Format is similar to [Adminix Link](Link.md) module. 

```PHP
newItemLink(
    AdminixLinkModule::name('new_user_btn')
        ->title('NEW USER')
        ->icon('bi bi-person-fill-add')
        ->uri(NewUserPage::URI)
)
```

Detailed here [#New item link](#new-item-link)
</td>
<td>Optional</td>
</tr>
</table>

## Fields

Fields is an array, of fields to be rendered in the List.

To add **Field** to the **List** module you need paste `AlexKudrya\Adminix\Modules\List\ListField` class
instance as an argument to `addField` or `addFields` method of `ListModule` object.

Example:

```php

use App\Models\User;
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListModule;

    ...

    $page = new AdminixPage();

    ...

    $page->addModule(
        ListModule::name('users')
            ->title('USERS')
            ->dataSource(User::class)
            ->pagination(20)
            ->addFields(
                ListField::name('Avatar')
                    ->field('avatar')
                    ->type(ListFieldTypeEnum::IMAGE),

                ListField::name('Name')
                    ->field('name')
                    ->type(ListFieldTypeEnum::STRING)
                    ->sortable()
                    ->editable(),

                ListField::name('Role')
                    ->field('role_id')
                    ->type(ListFieldTypeEnum::SELECT)
                    ->editable()
                    ->src(
                        InputSelectSrc::nameField('name')
                            ->valueField('id')
                            ->dataSource(Role::class)
                    )
                    ->addValidationRules(
                        'integer',
                        'required',
                        'exists:'. Role::class.',id'
                    ),

                ListField::name('Rating')
                    ->field('rating')
                    ->type(ListFieldTypeEnum::INTEGER)
                    ->editable()
                    ->sortable()
                    ->addValidationRules(
                        'integer',
                        'required',
                        'min:0',
                    ),

                ListField::name('Registration')
                    ->field('created_at')
                    ->type(ListFieldTypeEnum::DATETIME)
                    ->sortable(),

                ListField::name('Administrator')
                    ->field('is_admin')
                    ->type(ListFieldTypeEnum::BOOLEAN),

                ListField::field('deleted_at')
                    ->type(ListFieldTypeEnum::HIDDEN),

                ListField::field('id')
                    ->type(ListFieldTypeEnum::HIDDEN),
            )
    )
    ...

```

## `ListField` configurations

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>
 
</th>
</tr>
<tr>
<td>name</td>
<td>

Title of the field displayed on the top of table (inside `<th/>` tag)

```php
name('Role')
```
</td>
<td>Required</td>
</tr>
<tr>
<td>type</td>
<td>

Type of rendered field, can be provided only by `AlexKudrya\Adminix\Modules\List\ListFieldTypeEnum` Enum class

Available types for rendered field: 
- `STRING`
- `EMAIL`
- `INTEGER`
- `FLOAT`
- `BOOLEAN`
- `SELECT`
- `DATE`
- `DATETIME`
- `HIDDEN`
- `TEXTAREA`

It determines data format to render, and type of input for **`editable`** mode.

Example:

```php
type(ListFieldTypeEnum::SELECT)
```
</td>
<td>Required</td>
</tr>
<tr>
<td>field</td>
<td>

Name of field in database or **resource** (if resource was provided).

```php
 field('role_id')
```
</td>
<td>Required</td>
</tr>
<tr>
<td>sortable</td>
<td>

Determines the sorting possibility for current field. 

Can not be used to fields which provided by resource, and not exist in database table, or his name was modified. 

By default, is not enabled.

```php
sortable()
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>editable</td>
<td>

Determines the editable possibility for current field. 

Can not be used to fields which provided by resource, and not exist in database table, or his name was modified. 

This will not be working if in **`List`** module **`editable`** mode is not enabled. 

To working correctly requires primary key (usually it is "id" field) in this **fields** list (can be `hidden` type).

By default, is not enabled.

```php
editable()
```
</td>
<td>Optional</td>
</tr>
<tr>
<td>src</td>
<td>

Required for fields with type `SELECT`. Need to display correct value from related table.

For example, `users` table has related table `roles`, and relation made by `role_id` field in `users` table.
To display normal role name from `roles` table, not `role_id` from `users` table, you need to configure **src** correctly.

Argument can be only `AlexKudrya\Adminix\Modules\InputSelectSrc` class instance.

Example:

```php
ListField::name('Role')
    ->field('role_id')
    ->type(ListFieldTypeEnum::SELECT)
    ->editable()
    ->src(
        InputSelectSrc::nameField('name')
            ->valueField('id')
            ->dataSource(Role::class)
    )
```
<br/>

`InputSelectSrc` configuration:

<table>
<tr>
<th>Methods</th>
<th>Description</th>
</tr>
<tr>
<td>dataSource</td>
<td>

Source of data for filed items, can be an `Eloquent Model` - `\App\Models\Role::class` or name of table in database `roles`or `public.roles`

```php
dataSource(\App\Models\Role::class)
```
</td>
</tr>
<tr>
<td>nameField</td>
<td>

Name of field in related table to be displayed

```php
nameField('name')
```
</td>
</tr>
<tr>
<td>valueField</td>
<td>

Name of primary key field related to main table (usually it is "id")

```php
valueField('id')
```
</td>
</tr>
</table>

</td>
<td>

</td>
</tr>
<tr>
<td>

addSelectRecords
</td>
<td>

Required for fields with type `SELECT` as an alternative for `src()`.
Required if **src** is empty. It is a list of options, where `name()` is a title of `<option/>`, and `value()`is a value of `<option/>`

Arguments can be only `AlexKudrya\Adminix\Modules\SelectRecord` class instances.

Example:

```php
ListField::name('Role')
    ->field('role_id')
    ->type(ListFieldTypeEnum::SELECT)
    ->editable()
    ->addSelectRecords(
        SelectRecord::name('Admin')->value(1),
        SelectRecord::name('Manager')->value(2),
        SelectRecord::name('Seller')->value(3),
        SelectRecord::name('Guest')->value(4),
    )
...
```
</td>
<td>

</td>
</tr>
</table>

## Searching

Determines possibility to make full-text search in source table.

Example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListSearch;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->search(
            ListSearch::searchFields(['name', 'email'])
                ->placeholder('Search users')
        )
)

...
```

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**searchFields**
</td>
<td>

An array of fields which involved in the search process. The more fields, the slower but more accurate the search. 
Every field must be existed in main database table

```php
searchFields(['name', 'email'])
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**placeholder**
</td>
<td>

A placeholder for **Search** `<input/>`
```php
placeholder('Search users')
```
</td>
<td>

**Optional**
</td>
</tr>
</table>

## Filters

Add filters to the top of the module, near to search input. Provides the ability to filter the list by specific fields.

There are several types of filters available:
- [ListFilter](#listfilter) - `AlexKudrya\Adminix\Modules\List\ListFilter`
- [ListDateFilter](#listdatefilter) - `AlexKudrya\Adminix\Modules\List\ListDateFilter`
- [ListDateRangeFilter](#listdaterangefilter) - `AlexKudrya\Adminix\Modules\List\ListDateRangeFilter`

### `ListFilter`

Basic filter - `<select/>` input with defined options

Example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListFilter;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->addFilters(
            ListFilter::field('role_id')
                ->name('Role')
                ->src(
                    InputSelectSrc::nameField('name')
                        ->valueField('id')
                        ->dataSource(\App\Models\Role::class)
                ),
        )
)

...
```

Or

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListFilter;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->addFilters(
            ListFilter::field('role_id')
                ->name('Role')
                ->addFilterRecords(
                    ListFilterRecord::name('Client')->value(1),
                    ListFilterRecord::name('Guest')->value(2),
                    ListFilterRecord::name('Manager')->value(3),
                    ListFilterRecord::name('Admin')->value(4),
                )
        )
)

...
```

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**name**
</td>
<td>

Title of the filter

```php
name('Role')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**field**
</td>
<td>

Name of the field used for filtering. Field must be existed in base database table

```php
field('role_id')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**src**
</td>
<td>

Required if **filter_records** is empty. Has a format similar to `select` field src directive:

```php
src(
    InputSelectSrc::nameField('name')
        ->valueField('id')
        ->dataSource(\App\Models\Role::class)
),
```

| setting    | Description                                                                                                                                  |
|------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| dataSource | Source of data for filed items, can be an `Eloquent Model` - `\App\Models\Role::class` or name of table in database `roles`or `public.roles` |
| nameField  | Name of the field witch used as a title of `<option/>`                                                                                       |
| valueField | Name of the field witch used as a value of `<option/>` (usually it is "id")                                                                  |

</td>
<td>

**Required** if no **filterRecords()**
</td>
</tr>
<tr>
<td>

**filterRecords**
</td>
<td>

Required if **src** is empty. It is an `array` of options, where `name` is a title of `<option/>`, and `value`is a value of `<option/>`

```php
addFilterRecords(
    ListFilterRecord::name('Client')->value(1),
    ListFilterRecord::name('Guest')->value(2),
    ListFilterRecord::name('Manager')->value(3),
    ListFilterRecord::name('Admin')->value(4),
)
```
</td>
<td>

**Required** if no **src**
</td>
</tr>
</table>

### `ListDateFilter`

Date filter - allows you to select records within a selected date.

Example:

```PHP
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListDateFilter;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->addFilters(
            ListDateFilter::field('created_at')
                ->name('Registration date')
        )
)

...

```
<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>field</td>
<td>

Name of the field used for filtering. Field must have `timestamp`, `date` or `datetime` format.

```php
field('created_at')
```

</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>
name
</td>
<td>

Title of the filter

```php
name('Registration date')
```
</td>
<td>

**Required**
</td>
</tr>
</table>

### `ListDateRangeFilter`

Date range filter - allows you to select records within a date range.

Example:

```PHP
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\List\ListDateRangeFilter;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->addFilters(
            ListDateRangeFilter::field('created_at')
                ->name('Registration date')
                ->withTime()
        )
)

...

```
<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>field</td>
<td>

Name of the field used for filtering. Field must have `timestamp`, `date` or `datetime` format.

```php
field('created_at')
```

</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>
name
</td>
<td>

Title of the filter

```php
name('Registration date')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>
withTime
</td>
<td>

Determines whether the filter will filter by time or only by dates.

By default, filter only by date, without time.

```php
withTime()
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>
filterFrom
</td>
<td>

Define "From" input. If set `false`, "From" `datepicker` will not be displayed.

By default, it is enabled.

```php
filterFrom(false)
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>
filterTo
</td>
<td>

Define "To" input. If set `false`, "To" `datepicker` will not be displayed.

By default, it is enabled.

```php
filterTo(false)
```
</td>
<td>

**Optional**
</td>
</tr>
</table>

## Actions

"Actions" is an array of control buttons for every single record, to do some actions, such as "open", "edit", "delete",
or wherever else you need.

Example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Link\AdminixLinkModule;
use AlexKudrya\Adminix\Modules\Link\LinkModule;
use AlexKudrya\Adminix\Enums\HttpMethodsEnum;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->addActions(
            AdminixLinkModule::name('edit')
                ->title('EDIT')
                ->icon('bi bi-pencil')
                ->uri(UserPage::URI)
                ->params(['record:id']),
        
            LinkModule::name('delete')
                ->title('BAN')
                ->icon('bi bi-person-fill-slash')
                ->uri('ban_user')
                ->method(HttpMethodsEnum::POST)
                ->params(['record:id'])
                ->criteria([
                    ['deleted_at', '==', null]
                ]),
        
            LinkModule::name('restore')
                ->title('UNBAN')
                ->icon('bi bi-person-fill-up')
                ->uri('unban_user')
                ->method(HttpMethodsEnum::POST)
                ->params(['record:id'])
                ->criteria([
                    ['deleted_at', '!=', null]
                ]),
        )
)
...
```

To add **Action** buttons to **List** module you need paste `AlexKudrya\Adminix\Modules\Link\AdminixLinkModule` or
`AlexKudrya\Adminix\Modules\Link\LinkModule` class instances as an argument to `addAction` or `addActions` method of `AdminixPage` object.

**Action** buttons syntax is similar to [Link module](Link.md)

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>name</td>
<td>

Title which will be displayed on the button

```php
name('EDIT')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**uri**
</td>
<td>

In case **`AdminixLinkModule`** uri means the `uri` directive of page which defined in Adminix config file.
In this case better to use `URI` constant from target page provider class.

```php
uri(UserPage::URI)
```

In case **`LinkModule`** uri means the name of any route defined in router of your Laravel application.
Just copy route name from router.

```php
uri('edit_user')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**icon**
</td>
<td>

Icon class name from [Bootsrap icons](https://icons.getbootstrap.com/)

Example: to generate in the Link an icon `<i class="bi bi-people-fill"></i>` you need paste here `bi bi-people-fill` class name

```php
 icon('bi bi-people-fill')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**method**
</td>
<td>

Type of HTTP request, can be provided only by `AlexKudrya\Adminix\Enums\HttpMethodsEnum` Enum class

In case **`AdminixLinkModule`** method can be only **GET** so it is redundant.

In case **`LinkModule`** method must be match to the route defined in **uri** directive.

By default, it is equal to `HttpMethodsEnum::GET`

```php
method(HttpMethodsEnum::POST)
```
</td>
<td>

**Required** if not **GET**
</td>
</tr>
<tr>
<td>

**params**
</td>
<td>

Technically it is optional directive, but `action` without **params** makes it pointless, so it is make it required.

It is list of parameters which you added to your action (usually it is "id"). When `record:id`,
means that **id** of current record will be the parameter. By the way, for this purpose, 
if you not display **id** field i your list, you need add it to **fields** directive,
if you do not want to display it you may add this field with `HIDDEN` **type**.

Parameters adds to your link like this: **{URL}/**`parameter 1`**/**`parameter 2`...

Example:

```php
 params(['record:id'])
```

For record with **id** = 123 action link will be **`https://example/uri/123`**
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**criteria**
</td>
<td>

Determines display or not this action button for current record. For example if you need display action button only for users without **admin** status, you can do it like this:

```php
criteria([
    ['is_admin', '==', false]
])
```
or 
```php
criteria([
    ['deleted_at', '!=', null]
])
```

**Attention!** This directive support only `==` and `!=` operators, `>`, `<`, `>=`, `<=` is not working!
</td>
<td>

**Optional**
</td>
</tr>
</table>

## New item link

Usually, in the admin panels, lists have the ability to add new records, and the [New resource](New-resource.md) module is used for this. 
In order to display a link to a page with such a module in the correct place in the "list" module, there is this directive

Format is similar to [Link module](Link.md) or [Actions](#actions)

Example:

```php
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Link\AdminixLinkModule;

$page = new AdminixPage();

...

$page->addModule(
    ListModule...
        ->newItemLink(
            AdminixLinkModule::name('new_user_btn')
                ->title('NEW USER')
                ->icon('bi bi-person-fill-add')
                ->uri(NewUserPage::URI)
        )
)

...
```

## Appearance examples

Here is example of List without editable mode:

![image.png](65c1df6e777abac64110ebcd4c5290b6.png)

Here is example of List with editable mode:

![image.png](33008cfa923db4ec0ce603fcda1d8e45.png)