# Configuration

Panel configuration is carried out in a file `confing/adminix.php`

```php

   return [

       'route_prefix' => [URL prefix for adminix],

       'admin_user_model' => [User model class],

       'pages' => [Pages configuration],

       'menu' => [Menu configuration],
       
       'app_title' => 'Adminix Panel',

       'no_auth_access' => false
   ]

```

## Configuration elements

**`route_prefix`** - prefix for Adminix pages URL. Example - if prefix equal `adminix`, for `users` page in Adminix panel full URL will be *`https://domain.com/adminix/users`*.

**`admin_user_model`** - Eloquent model for auth users. By default it is `\App\Models\User::class`

**`pages`** - Configuration for Adminix pages, detailed in [*Pages configuration*](Pages.md)

**`menu`** - Configuration for Adminix menu, detailed in [*Menu configuration*](Menu.md)

**`app_title`** - Title for `<title>` tag in `<head>` of Adminix pages

**`no_auth_access`** - Provide access to admin panel without authorization (if `TRUE`)

[**Pages configuration**](Pages.md)

[**Menu configuration**](Menu.md)