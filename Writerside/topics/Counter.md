# Counter

![image.png](66362208541e0232088ef1dea48b32d9.png)

![image.png](906500f85ebdaea18ec168694037bfd3.png)

## Description

This module displays count of some entity in database, selected according to the necessary criteria. 
For example, you can display total number of registered **users** or, today **orders**, or something else.

## Configuration

```php
use App\Models\User;
use AlexKudrya\Adminix\AdminixPage;
use AlexKudrya\Adminix\Modules\Counter\CounterModule;
use Carbon\Carbon;

    ...

    $page = new AdminixPage();
    
    ...

    $page->addModules(
        CounterModule::title('TOTAL USERS')
            ->name('total_users')
            ->icon('bi bi-people')
            ->dataSource(\App\Models\User::class)
            ->criteria([
                ['is_admin', false],
            ]),
            
        CounterModule::title('TODAY REGISTRATIONS')
            ->name('today_registrations')
            ->icon('bi bi-person-add')
            ->dataSource(\App\Models\User::class)
            ->criteria([
                ['is_admin', false],
                ['created_at', '>=', Carbon::today()->startOfDay()->toDateTimeString()]
            ])
    );
    ...
```
To add to the page **Counter** module you need paste `AlexKudrya\Adminix\Modules\Counter\CounterModule` class
instance as an argument to `addModule` or `addModules` method of `AdminixPage` object.

## `CounterModule` configuration

<table>
<tr>
<th>Method</th>
<th>Description</th>
<th>

</th>
</tr>
<tr>
<td>

**title**
</td>
<td>

Title on top of the **counter**

```php
 title('TOTAL USERS')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**name**
</td>
<td>

Name of module, must be unique in current page.

```php
name('total_users')
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**icon**
</td>
<td>

Icon class name from [Bootsrap icons](https://icons.getbootstrap.com/)

Example: to generate in right side of the Counter an icon `<i class="bi bi-people"></i>` you need paste here `bi bi-people` class name

```php
 icon('bi bi-people')
```
</td>
<td>

**Optional**
</td>
</tr>
<tr>
<td>

**dataSource**
</td>
<td>

Source of data for Counter, can be an `Eloquent Model` - `\App\Models\User::class` or name of table in database `users` or `public.users`

```php
dataSource(\App\Models\User::class)
```
</td>
<td>

**Required**
</td>
</tr>
<tr>
<td>

**criteria**
</td>
<td>

Database `Builder` criteria to render records. For example to not show administrator you need paste this:

```php
criteria([
    ['is_admin', '=', false]
])
```

Or you can paste parameter from route:

```php
criteria([
    ['user_id', '=', 'param:1']
])
```

For example, for `https://site.com/adminix/comments/21` it wiil be mean that you need all comments where `user_id == 21`.
</td>
<td>

**Optional**
</td>
</tr>
</table>

